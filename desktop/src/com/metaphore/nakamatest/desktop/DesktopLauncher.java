package com.metaphore.nakamatest.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.metaphore.nakamatest.ActionResolver;
import com.metaphore.nakamatest.App;
import com.metaphore.nakamatest.AppConstants;
import com.metaphore.nakamatest.AppParams;

import java.util.UUID;

public class DesktopLauncher implements ActionResolver {
	private static final String PREF_NAME = "desktop.xml";
	private static final String PREF_KEY_DEVICE_UUID = "device_uuid";

	private String deviceUuid = null;

	public static void main (String[] arg) {
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
		config.setTitle("Nakama Test");
		config.setPreferencesConfig(AppConstants.APP_DATA_DIR, Files.FileType.External);
		config.setWindowedMode(480, 800);

		AppParams params = new AppParams();
		params.debug = true;

		DesktopLauncher launcher = new DesktopLauncher();

		App app = new App(params, launcher);
		new Lwjgl3Application(app, config);
	}

	@Override
	public String getDeviceUuid() {
		if (deviceUuid != null) {
			return deviceUuid;
		}

		Preferences prefs = Gdx.app.getPreferences(PREF_NAME);
		deviceUuid = prefs.getString(PREF_KEY_DEVICE_UUID, null);
		if (deviceUuid != null) {
			return deviceUuid;
		}

		deviceUuid = UUID.randomUUID().toString();
		prefs.putString(PREF_KEY_DEVICE_UUID, deviceUuid).flush();
		return deviceUuid;
	}
}
