package android.os;

/**
 * This class is a okhttp3 issue workaround.
 * Read more: https://github.com/MobiVM/robovm/issues/325
 */
public class Build {
    public static final class VERSION {
        public static final int SDK_INT = 16; // Android 4.1
    }
}
