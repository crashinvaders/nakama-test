package com.crashinvaders.common;

import android.os.Handler;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

/**
 * Pool on android hangs sometimes when it idles, so here is explanation and solution:
 * <br/>
 * http://stackoverflow.com/a/7473733/3802890
 */
public class AudioLagFixer implements Runnable {
    private final Sound sound;
    private final Handler handler;

    public AudioLagFixer(Handler handler) {
        this.handler = handler;
        sound = Gdx.audio.newSound(Gdx.files.internal("dummy_sound.wav"));
        postPlaySound();
    }

    public void dispose() {
        if (sound != null) {
            sound.dispose();
        }
    }

    @Override
    public void run() {
        // One small step for a man is a small step for a small man
        if (sound.loop(0f) < 0) {
            postPlaySound();
        }
    }

    private void postPlaySound() {
        handler.postDelayed(this, 500);
    }
}
