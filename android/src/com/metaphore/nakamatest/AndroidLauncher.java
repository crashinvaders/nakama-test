package com.metaphore.nakamatest;

import android.os.Bundle;
import android.provider.Settings;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.crashinvaders.common.AudioLagFixer;
import com.crashinvaders.common.PatchedAndroidApplication;

public class AndroidLauncher extends PatchedAndroidApplication implements ActionResolver {

	private AudioLagFixer audioLagFixer;

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		config.useImmersiveMode = true;

		AppParams params = new AppParams();
		params.debug = BuildConfig.DEBUG;

		initialize(new App(params, this), config);

		audioLagFixer = new AudioLagFixer(getHandler());
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		audioLagFixer.dispose();
	}

	@Override
	public String getDeviceUuid() {
		return Settings.Secure.getString(
				getContext().getContentResolver(),
				Settings.Secure.ANDROID_ID);
	}
}
