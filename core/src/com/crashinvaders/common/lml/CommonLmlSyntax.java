package com.crashinvaders.common.lml;

import com.crashinvaders.common.lml.attributes.*;
import com.crashinvaders.common.lml.tags.GroupLmlTag;
import com.crashinvaders.common.lml.tags.ParticleEffectLmlTag;
import com.crashinvaders.common.lml.tags.ShrinkContainerLmlTag;
import com.crashinvaders.common.lml.tags.TransformScalableWrapperLmlTag;
import com.crashinvaders.common.spine.lml.attributes.*;
import com.crashinvaders.common.spine.lml.tags.SkeletonGroupLmlTag;
import com.crashinvaders.common.spine.lml.tags.macro.SkeletonAddAnimationLmlMacroTag;
import com.crashinvaders.common.spine.lml.tags.macro.SkeletonAnimBBClickHandlerLmlMacroTag;
import com.crashinvaders.common.spine.lml.tags.macro.SkeletonAnimationMixLmlMacroTag;
import com.crashinvaders.common.spine.lml.tags.macro.SkeletonBBClickHandlerLmlMacroTag;
import com.github.czyzby.lml.parser.impl.DefaultLmlSyntax;
import com.github.czyzby.lml.parser.impl.attribute.container.ContainerAlignLmlAttribute;
import com.github.czyzby.lml.parser.impl.attribute.container.ContainerFillLmlAttribute;
import com.github.czyzby.lml.parser.impl.attribute.container.ContainerFillXLmlAttribute;
import com.github.czyzby.lml.parser.impl.attribute.container.ContainerFillYLmlAttribute;
import com.github.czyzby.lml.vis.parser.impl.attribute.flow.HorizontalSpacingLmlAttribute;
import com.github.czyzby.lml.vis.parser.impl.attribute.flow.VerticalSpacingLmlAttribute;
import com.github.czyzby.lml.vis.parser.impl.attribute.grid.GridSpacingLmlAttribute;
import com.github.czyzby.lml.vis.parser.impl.attribute.grid.ItemHeightLmlAttribute;
import com.github.czyzby.lml.vis.parser.impl.attribute.grid.ItemSizeLmlAttribute;
import com.github.czyzby.lml.vis.parser.impl.attribute.grid.ItemWidthLmlAttribute;
import com.github.czyzby.lml.vis.parser.impl.tag.provider.GridGroupLmlTagProvider;
import com.github.czyzby.lml.vis.parser.impl.tag.provider.HorizontalFlowGroupLmlTagProvider;
import com.github.czyzby.lml.vis.parser.impl.tag.provider.VerticalFlowGroupLmlTagProvider;

/** Extension to {@link DefaultLmlSyntax}. Adds some new/improved elements and tags.*/
public class CommonLmlSyntax extends DefaultLmlSyntax {

    @Override
    protected void registerActorTags() {
        super.registerActorTags();

        addTagProvider(new GroupLmlTag.TagProvider(), "group");
        addTagProvider(new ShrinkContainerLmlTag.TagProvider(), "shrinkContainer");
        addTagProvider(new TransformScalableWrapperLmlTag.TagProvider(), "scaleWrapper");
        addTagProvider(new SkeletonGroupLmlTag.TagProvider(), "skeleton");
        addTagProvider(new ParticleEffectLmlTag.TagProvider(), "particles");

        // Vis unique actors:
        addTagProvider(new GridGroupLmlTagProvider(), "gridGroup", "grid");
        addTagProvider(new HorizontalFlowGroupLmlTagProvider(), "horizontalFlow", "hFlow", "horizontalFlowGroup");
        addTagProvider(new VerticalFlowGroupLmlTagProvider(), "verticalFlow", "vFlow", "verticalFlowGroup");
    }

    @Override
    protected void registerMacroTags() {
        super.registerMacroTags();

        addMacroTagProvider(new SkeletonBBClickHandlerLmlMacroTag.TagProvider(), "bbClick");
        addMacroTagProvider(new SkeletonAnimBBClickHandlerLmlMacroTag.TagProvider(), "bbAnimClick");
        addMacroTagProvider(new SkeletonAddAnimationLmlMacroTag.TagProvider(), "addAnimation");
        addMacroTagProvider(new SkeletonAnimationMixLmlMacroTag.TagProvider(), "animationMix");
    }

    @Override
    protected void registerAttributes() {
        super.registerAttributes();

        registerCheckBoxAttributes();
        registerSkeletonAttributes();
        registerFlowGroupsAttributes();
        registerGridGroupAttributes();
        registerSliderAttributes();
    }

    @Override
    protected void registerCommonAttributes() {
        super.registerCommonAttributes();

        addAttributeProcessor(new OnEnterPressedLmlAttribute(), "enter", "onEnter");
        addAttributeProcessor(new OnBackPressedLmlAttribute(), "back", "onBack", "onEscape");
        addAttributeProcessor(new AdvancedColorLmlAttribute(), "color");
        addAttributeProcessor(new OriginLmlAttribute(), "origin");
        addAttributeProcessor(new ZIndexLmlAttribute(), "zIndex");
        addAttributeProcessor(new PositionLmlAttribute(), "position");
        addAttributeProcessor(new BoundsLmlAttribute(), "bounds");
        addAttributeProcessor(new PatchedOnClickLmlAttribute(), "onClick", "click");
        addAttributeProcessor(new DelegateInputEventsLmlAttribute(), "delegateInput");

        // SkeletonGroupLmlTag's specific attribute names that should be reserved.
        addBuildingAttributeProcessor(new DummyBuildingLmlAttribute(), "slot", "attachment");
    }

    @Override
    protected void registerImageAttributes() {
        super.registerImageAttributes();

        addAttributeProcessor(new ImageDrawableLmlAttribute(), "drawable");
    }

    @Override
    protected void registerContainerAttributes() {
        super.registerContainerAttributes();

        addAttributeProcessor(new ContainerPadLmlAttribute(), "pad", "containerPad");
        addAttributeProcessor(new ContainerPadLeftLmlAttribute(), "padLeft", "containerPadLeft");
        addAttributeProcessor(new ContainerPadRightLmlAttribute(), "padRight", "containerPadRight");
        addAttributeProcessor(new ContainerPadTopLmlAttribute(), "padTop", "containerPadTop");
        addAttributeProcessor(new ContainerPadBottomLmlAttribute(), "padBottom", "containerPadBottom");
        addAttributeProcessor(new ContainerFillLmlAttribute(), "containerFill");
        addAttributeProcessor(new ContainerFillXLmlAttribute(), "containerFillX");
        addAttributeProcessor(new ContainerFillYLmlAttribute(), "containerFillY");
        addAttributeProcessor(new ContainerAlignLmlAttribute(), "align", "containerAlign");
    }

    @Override
    protected void registerLabelAttributes() {
        super.registerLabelAttributes();

        addAttributeProcessor(new LabelFontScaleLmlAttribute(), "fontScale");
    }

    @Override
    protected void registerButtonAttributes() {
        super.registerButtonAttributes();

        addAttributeProcessor(new TextButtonFontScaleLmlAttribute(), "fontScale");
        addAttributeProcessor(new ButtonCheckedImageLmlAttribute(), "checkedImage");
    }

    @Override
    protected void registerHorizontalGroupAttributes() {
        super.registerHorizontalGroupAttributes();

        addAttributeProcessor(new HorizontalGroupExpandLmlAttribute(), "expand", "groupExpand");
        addAttributeProcessor(new HorizontalGroupGrowLmlAttribute(), "grow", "groupGrow");
        addAttributeProcessor(new HorizontalGroupWrapLmlAttribute(), "wrap", "groupWrap");
    }

    @Override
    protected void registerVerticalGroupAttributes() {
        super.registerVerticalGroupAttributes();

        addAttributeProcessor(new VerticalGroupExpandLmlAttribute(), "expand", "groupExpand");
        addAttributeProcessor(new VerticalGroupGrowLmlAttribute(), "grow", "groupGrow");
        addAttributeProcessor(new VerticalGroupWrapLmlAttribute(), "wrap", "groupWrap");
    }

    protected void registerCheckBoxAttributes() {
        addAttributeProcessor(new CheckBoxTextSpaceLmlAttribute(), "textSpace");
    }

    protected void registerSkeletonAttributes() {
        addAttributeProcessor(new SkeletonAnimationLmlAttribute(), "animation");
        addAttributeProcessor(new SkeletonDefaultMixLmlAttribute(), "defaultMix", "mix");
        addAttributeProcessor(new SkeletonScalingLmlAttribute(), "scaling", "skelScaling");
        addAttributeProcessor(new SkeletonScaleLmlAttribute(), "skeletonScale", "skelScale");
        addAttributeProcessor(new SkeletonSkinLmlAttribute(), "skeletonSkin", "skelSkin");
    }

    /** Flow groups attributes. */
    protected void registerFlowGroupsAttributes() {
        // HorizontalFlowGroup:
        addAttributeProcessor(new HorizontalSpacingLmlAttribute(), "spacing");
        // VerticalFlowGroup:
        addAttributeProcessor(new VerticalSpacingLmlAttribute(), "spacing");
    }

    /** GridGroup attributes. */
    protected void registerGridGroupAttributes() {
        addAttributeProcessor(new GridSpacingLmlAttribute(), "spacing");
        addAttributeProcessor(new ItemHeightLmlAttribute(), "itemHeight");
        addAttributeProcessor(new ItemSizeLmlAttribute(), "itemSize");
        addAttributeProcessor(new ItemWidthLmlAttribute(), "itemWidth");
    }

    protected void registerSliderAttributes() {
        addAttributeProcessor(new VerticalScrollSliderLmlAttribute(), "scrollerVertical", "scrollerV");
    }
}