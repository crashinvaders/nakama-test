package com.crashinvaders.common.lml.tags;

import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.crashinvaders.common.graphics.ParticleEffectX;
import com.crashinvaders.common.scene2d.ParticleEffectActor;
import com.github.czyzby.lml.parser.LmlParser;
import com.github.czyzby.lml.parser.impl.tag.AbstractNonParentalActorLmlTag;
import com.github.czyzby.lml.parser.tag.LmlActorBuilder;
import com.github.czyzby.lml.parser.tag.LmlTag;
import com.github.czyzby.lml.parser.tag.LmlTagProvider;

public class ParticleEffectLmlTag extends AbstractNonParentalActorLmlTag {

    public ParticleEffectLmlTag(LmlParser parser, LmlTag parentTag, StringBuilder rawTagData) {
        super(parser, parentTag, rawTagData);
    }

    @Override
    protected Actor getNewInstanceOfActor(LmlActorBuilder builder) {
        String styleName = builder.getStyleName();
        ParticleEffect protoEffect = getSkin(builder).get(styleName, ParticleEffect.class);
        return new ParticleEffectActor(new ParticleEffectX(protoEffect));
    }

    public static class TagProvider implements LmlTagProvider {
        @Override
        public LmlTag create(final LmlParser parser, final LmlTag parentTag, final StringBuilder rawTagData) {
            return new ParticleEffectLmlTag(parser, parentTag, rawTagData);
        }
    }
}
