package com.crashinvaders.common.lml;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.github.czyzby.lml.parser.LmlView;

public class SimpleLmlView implements LmlView {

    @Override
    public Stage getStage() {
        // Provide no stage to LmlParser in order to add actors manually.
        return null;
    }

    @Override
    public String getViewId() {
        // All LmlViews are accessible from the LML markup by their class names by default.
        return this.getClass().getSimpleName();
    }
}
