package com.crashinvaders.common.assets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.utils.*;

import java.lang.StringBuilder;

public class TempAssetDependencies implements Disposable {
    private static final String TAG = TempAssetDependencies.class.getSimpleName();
    private static final Array<String> tmpStringArray = new Array<>();

    private final Pool<AssetDependency> pool = new AssetDependencyPool();
    /** Asset path to word dependency */
    private final ArrayMap<String, AssetDependency> assetDependencies = new ArrayMap<>();
    private final AssetManager assetManager;

    public TempAssetDependencies(AssetManager assetManager) {
        this.assetManager = assetManager;
    }

    public AssetManager getManager() {
        return assetManager;
    }

    public <T> T load(String ownerKey, String fileName, Class<T> type) {
        if (!assetManager.isLoaded(fileName, type)) {
            assetManager.load(fileName, type);
            assetManager.finishLoadingAsset(fileName);
            Gdx.app.debug(TAG, "Asset loaded: " + fileName);
        }
        registerDependency(ownerKey, fileName);
        return assetManager.get(fileName, type);
    }

    public <T> T load(String ownerKey, String fileName, Class<T> type, AssetLoaderParameters<T> parameter) {
        if (!assetManager.isLoaded(fileName, type)) {
            assetManager.load(fileName, type, parameter);
            assetManager.finishLoadingAsset(fileName);
            Gdx.app.debug(TAG, "Asset loaded: " + fileName);
        }
        registerDependency(ownerKey, fileName);
        return assetManager.get(fileName, type);
    }

    public <T> T load(String ownerKey, AssetDescriptor<T> desc) {
        if (!assetManager.isLoaded(desc.fileName, desc.type)) {
            assetManager.load(desc);
            assetManager.finishLoadingAsset(desc.fileName);
            Gdx.app.debug(TAG, "Asset loaded: " + desc.fileName);
        }
        registerDependency(ownerKey, desc.fileName);
        return assetManager.get(desc);
    }

    public void unload(String ownerKey, String fileName) {
        boolean disposeAsset = unregisterDependency(ownerKey, fileName);
        if (disposeAsset) {
            assetManager.unload(fileName);
            Gdx.app.debug(TAG, "Asset unloaded: " + fileName);
        }
    }

    public void unloadAll(String ownerKey) {
        Array<String> filesToUnregister = tmpStringArray;
        // Look for word dependencies that contain ownerKey
        for (int i = 0; i < assetDependencies.size; i++) {
            AssetDependency dependency = assetDependencies.getValueAt(i);
            if (dependency.containsOwner(ownerKey)) {
                filesToUnregister.add(dependency.getFileName());
            }
        }
        // Unload dependencies for that contain ownerKey
        for (String fileName : filesToUnregister) {
            unload(ownerKey, fileName);
        }
        filesToUnregister.clear();
    }

    private void registerDependency(String ownerKey, String assetPath) {
        AssetDependency dependency = assetDependencies.get(assetPath);
        if (dependency == null) {
            dependency = pool.obtain();
            dependency.init(assetPath);
            assetDependencies.put(assetPath, dependency);
        }
        dependency.addOwner(ownerKey);
    }

    /** @return true if word no longer has owners and should be disposed */
    private boolean unregisterDependency(String ownerKey, String assetPath) {
        AssetDependency dependency = assetDependencies.get(assetPath);
        if (dependency == null) {
            Gdx.app.error(TAG, "Unregister failed, dependency for word \""+assetPath+"\" is not registered for owner: " + ownerKey);
            return false;
        }
        dependency.removeOwner(ownerKey);

        if (!dependency.hasOwners()) {
            pool.free(assetDependencies.removeKey(assetPath));
            return true;
        } else {
            return false;
        }
    }

    /** Release rest of the still held dependencies (if present). */
    @Override
    public void dispose() {
        if (assetDependencies.size == 0) return;

        ObjectSet<String> activeOwners = new ObjectSet<>();
        for (int i = 0; i < assetDependencies.size; i++) {
            AssetDependency dependency = assetDependencies.getValueAt(i);
            assetManager.unload(dependency.fileName);
            Gdx.app.debug(TAG, "Asset unloaded: " + dependency.fileName);
            activeOwners.addAll(dependency.owners);
        }
        if (activeOwners.size > 0) {
            StringBuilder sb = new StringBuilder();
            sb.append("Next owners didn't release their temp dependencies by them selves: ");
            sb.append(activeOwners.toString(", "));
            Gdx.app.debug(TAG, sb.toString());
        }
    }

    private static class AssetDependency implements Pool.Poolable {

        private final ObjectSet<String> owners = new ObjectSet<>();
        private String fileName;

        public void init(String fileName) {
            this.fileName = fileName;
        }

        public void addOwner(String key) {
            boolean result = owners.add(key);
            if (!result) {
                Gdx.app.error(TAG, "Dependency for word \"" + fileName +"\" registered twice for owner: " + key);
            }
        }

        public void removeOwner(String key) {
            boolean result = owners.remove(key);
            if (!result) {
                Gdx.app.error(TAG, "Removal failed, word \"" + fileName +"\" doesn't contain registered owner: " + key);
            }
        }

        public boolean hasOwners() {
            return owners.size > 0;
        }

        public boolean containsOwner(String key) {
            return owners.contains(key);
        }

        public String getFileName() {
            return fileName;
        }

        @Override
        public void reset() {
            fileName = null;
            owners.clear();
        }
    }

    private static class AssetDependencyPool extends Pool<AssetDependency> {
        @Override
        protected AssetDependency newObject() {
            return new AssetDependency();
        }
    }
}
