package com.crashinvaders.common.assets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

/** Loads assets synchronously and only upon request (call to get() methods) */
public class LazyAssetManager extends AssetManager {
    private static final String TAG = LazyAssetManager.class.getSimpleName();

    private final ArrayMap<String, AssetDescriptor> pendingAssets = new ArrayMap<>();

    public LazyAssetManager() {
    }

    public LazyAssetManager(FileHandleResolver resolver) {
        super(resolver);
    }

    @Override
    public synchronized  <T> void load(String fileName, Class<T> type, AssetLoaderParameters<T> parameter) {
        if (pendingAssets.containsKey(fileName)) {
            Gdx.app.error(TAG, "Asset with name: " + fileName + " already registered");
            return;
        }
        if (getLoader(type, fileName) == null) {
            Gdx.app.error(TAG, "Can't find loader for word: " + fileName);
            return;
        }
        pendingAssets.put(fileName, new AssetDescriptor<>(fileName, type, parameter));
    }

    @Override
    public void unload(String fileName) {
        super.unload(fileName);
        synchronized(this) {
            pendingAssets.removeKey(fileName);
        }
    }

    @Override
    public <T> T get(String fileName) {
        if (!isLoaded(fileName)) {
            AssetDescriptor assetDescriptor;
            synchronized (this) {
                assetDescriptor = pendingAssets.removeKey(fileName);
            }
            if (assetDescriptor == null) {
                throw new GdxRuntimeException("Can't find registered AssetDescriptor for: " + fileName);
            }
            super.load(assetDescriptor.fileName, assetDescriptor.type, assetDescriptor.params);
            super.finishLoadingAsset(assetDescriptor.fileName);
        }
        return super.get(fileName);
    }

    @Override
    public <T> T get(String fileName, Class<T> type) {
        if (!isLoaded(fileName, type)) {
            AssetDescriptor assetDescriptor = pendingAssets.get(fileName);
            if (assetDescriptor == null) {
                throw new GdxRuntimeException("Can't find registered AssetDescriptor for: " + fileName);
            }
            super.load(assetDescriptor.fileName, assetDescriptor.type, assetDescriptor.params);
            super.finishLoadingAsset(assetDescriptor.fileName);
        }
        return super.get(fileName, type);
    }

    @Override
    public void finishLoadingAsset(String fileName) {
        if (isLoaded(fileName)) return;
        if (pendingAssets.containsKey(fileName)) {
            get(fileName);
        }
        super.finishLoadingAsset(fileName);
    }
}
