package com.crashinvaders.common;

public class MutableFloat {
    private float value = 0f;

    public MutableFloat() {
    }

    public MutableFloat(float value) {
        this.value = value;
    }

    public void set(float value) {
        this.value = value;
    }

    public float get() {
        return value;
    }
}

