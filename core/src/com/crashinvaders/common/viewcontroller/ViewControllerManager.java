package com.crashinvaders.common.viewcontroller;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.Disposable;

public class ViewControllerManager implements Disposable {

    private final ArrayMap<Class<? extends ViewController>, ViewController> viewControllers = new ArrayMap<>();
    private final ArrayMap<Class<? extends ViewController>, ViewController> pendingInit = new ArrayMap<>();
    private final Stage stage;

    private boolean viewCreated = false;
    private Group sceneRoot = null;

    public ViewControllerManager(Stage stage) {
        this.stage = stage;
    }

    @Override
    public void dispose() {
        pendingInit.clear();

        for (int i = 0; i < viewControllers.size; i++) {
            viewControllers.getValueAt(i).dispose();
        }
        viewControllers.clear();
    }

    public void update(float delta) {
        if (pendingInit.size > 0) {
            for (int i = 0; i < pendingInit.size; i++) {
                pendingInit.getValueAt(i).onViewCreated(sceneRoot);
            }
            pendingInit.clear();
        }

        for (int i = 0; i < viewControllers.size; i++) {
            viewControllers.getValueAt(i).update(delta);
        }
    }

    public void add(ViewController viewController) {
        viewControllers.put(viewController.getClass(), viewController);

        if (viewCreated) {
            // The controller will be initialized next frame.
            pendingInit.put(viewController.getClass(), viewController);
        }
    }

    public boolean remove(ViewController viewController) {
        ViewController deletedViewController = viewControllers.removeKey(viewController.getClass());

        // Cleanup pending queue as well.
        pendingInit.removeKey(viewController.getClass());

        if (deletedViewController != null && viewCreated) {
            deletedViewController.dispose();
        }

        return deletedViewController != null;
    }

    public <T extends ViewController> boolean remove(Class<T> viewControllerType) {
        ViewController viewController = get(viewControllerType);
        if (viewController == null) return false;

        return remove(viewController);
    }

    public <T extends ViewController> T get(Class<T> viewControllerType) {
        return (T)viewControllers.get(viewControllerType);
    }

    public void onViewCreated(Group sceneRoot) {
        if (viewCreated) {
            throw new IllegalStateException("View controller already has been initialized.");
        }
        this.viewCreated = true;
        this.sceneRoot = sceneRoot;
        for (int i = 0; i < viewControllers.size; i++) {
            viewControllers.getValueAt(i).onViewCreated(sceneRoot);
        }
    }

    public Stage getStage() {
        return stage;
    }

    public Group getSceneRoot() {
        return sceneRoot;
    }
}
