package com.crashinvaders.common.assetloaders;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.BitmapFontLoader;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

/**
 * Extended {@link BitmapFont} loader.
 * Turn on color markup by default. You can disable this by specifying "_nm" suffix in file name
 * Extra vertical spacing can be added to font by adding "_spacing" suffix
 */
public class BitmapFontLoaderX extends BitmapFontLoader {

    public BitmapFontLoaderX(FileHandleResolver resolver) {
        super(resolver);
    }

    @Override
    public BitmapFont loadSync(AssetManager manager, String fileName, FileHandle file, BitmapFontParameter parameter) {
        BitmapFont font = super.loadSync(manager, fileName, file, parameter);

        // Turn on color markup
        boolean markupDisabled = fileName.contains("_nm");
        font.getData().markupEnabled = !markupDisabled;
        font.setUseIntegerPositions(false);

        // Add line spacing to font
        boolean extraSpacing = fileName.contains("_spacing");
        if (extraSpacing) {
            for (BitmapFont.Glyph[] subGlyphs : font.getData().glyphs) {
                if (subGlyphs == null) continue;
                for (BitmapFont.Glyph glyph : subGlyphs) {
                    if (glyph == null) continue;
                    glyph.xadvance *= 1.2f;
                }
            }
        }

        return font;
    }
}
