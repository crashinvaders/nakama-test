package com.crashinvaders.common.statequeue;

public interface StateQueueNode {
    void beginState(StateQueueManager stateManager);
    void endState(StateQueueManager stateManager);
}
