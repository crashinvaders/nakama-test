package com.crashinvaders.common.statequeue;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Pool;
import com.crashinvaders.common.PrioritizedArray;

@SuppressWarnings({"WeakerAccess", "unused"})
public class StateQueueManager<T extends StateQueueNode> implements Pool.Poolable {
    public static final int EMPTY_INDEX = -1;
    private static final String TAG = StateQueueManager.class.getSimpleName();

    private final PrioritizedArray<T> stateNodes = new PrioritizedArray<>();
    private int currentStateIdx = EMPTY_INDEX;

    private Listener<T> listener;

    @Override
    public void reset() {
        stateNodes.clear();
        currentStateIdx = EMPTY_INDEX;
        listener = null;
    }

    public void setListener(Listener<T> listener) {
        this.listener = listener;
    }

    /** Adds the node to the queue with 0 priority. */
    public void addStateNode(T state) {
        this.addStateNode(state, 0);
    }

    /** Adds the node to the queue. */
    public void addStateNode(T state, int priority) {
        // Memorize current state instance
        T currentState = null;
        if (currentStateIdx != EMPTY_INDEX) {
            currentState = stateNodes.get(currentStateIdx);
        }

        stateNodes.add(state, priority);

        // Update current state index in case it's got shifted.
        if (currentState != null) {
            currentStateIdx = stateNodes.indexOf(currentState);
        }
    }

    public T getState(int stateIdx) {
        return stateNodes.get(stateIdx);
    }

    public T getCurrentState() {
        if (currentStateIdx == EMPTY_INDEX) return null;
        return stateNodes.get(currentStateIdx);
    }

    public void start() {
        if (currentStateIdx != EMPTY_INDEX) {
            throw new IllegalStateException("The queue is already running.");
        }
        switchToNextState();
    }

    public void restart() {
        int idxCurrent = this.currentStateIdx;
        if (idxCurrent != EMPTY_INDEX) {
            finishNodeInternal(getState(this.currentStateIdx));
            this.currentStateIdx = EMPTY_INDEX;

            if (listener != null) {
                listener.onStateChanged(this, true, idxCurrent, EMPTY_INDEX);
            }
        }
        switchToNextState();
    }

    public void finishCurrentState(T actionNode) {
        if (getCurrentState() != actionNode) {
            Gdx.app.error(TAG, actionNode.getClass().getSimpleName() + " is not a current action node", new IllegalStateException());
            return;
        }
        switchToNextState();
    }

    protected void switchToNextState() {
        int idxCurrent = this.currentStateIdx;
        int idxNext = idxCurrent+1;
        if (idxNext >= stateNodes.size()) {
            idxNext = EMPTY_INDEX;
        }

        if (idxCurrent != EMPTY_INDEX) {
            finishNodeInternal(getState(idxCurrent));
            this.currentStateIdx = EMPTY_INDEX;
        }

        if (idxNext != EMPTY_INDEX) {
            this.currentStateIdx = idxNext;
            beginNodeInternal(getState(idxNext));
        }

        if (listener != null) {
            listener.onStateChanged(this, false, idxCurrent, idxNext);
        }
    }

    protected void finishNodeInternal(T node) {
        T currentNode = getCurrentState();
        if (node != currentNode) {
            throw new IllegalStateException("The node is not a current node.");
        }
        currentNode.endState(this);
    }

    protected void beginNodeInternal(T node) {
        T currentNode = getCurrentState();
        if (node != currentNode) {
            throw new IllegalStateException("The node is not a current node.");
        }
        currentNode.beginState(this);
    }

    public interface Listener<T extends StateQueueNode> {
        void onStateChanged(StateQueueManager<T> stateManager,
                            boolean restarting,
                            int prevStateIdx, int currStateIdx);
    }
}
