package com.crashinvaders.common.eventmanager;

/**
 * Base class for any event info that can be fired through {@link EventManager}
 */
public interface EventInfo {
}
