package com.crashinvaders.common.eventmanager;

/**
 * Utility class that can be used to define event handler for particular type by just creating anonymous inline class.
 * <p/>
 * Code example:
 * <pre>
 * eventManager().addHandler(new TypedEventHandler<ConcreteEvent>() {
 *     protected void handleEvent(ConcreteEvent event) {
 *         // ...
 *     }
 * }, ConcreteEvent.class);
 * </pre>
 */
public abstract class TypedEventHandler<TEvent extends EventInfo> implements EventHandler<EventParams> {

    @Override
    public final void handle(EventInfo event, EventParams params) {
        //noinspection unchecked
        handleEvent((TEvent)event);
    }

    protected abstract void handleEvent(TEvent event);
}
