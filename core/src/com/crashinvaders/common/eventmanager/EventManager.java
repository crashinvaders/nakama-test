package com.crashinvaders.common.eventmanager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.SnapshotArray;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public abstract class EventManager<TEventParams extends EventParams> {

    protected final Map<Class<? extends EventInfo>, SnapshotArray<EventHandler<TEventParams>>> handlers;

    public EventManager() {
        handlers = new HashMap<>();
    }

    public void dispatchEvent(EventInfo event) {
        Class infoClass = event.getClass();
        dispatchToHandlers(event, infoClass);
    }

    protected void dispatchToHandlers(EventInfo event, Class<? extends EventInfo> eventClass) {
        if (handlers.containsKey(eventClass)) {
            TEventParams eventParams = obtainEventParams(event);
            SnapshotArray<EventHandler<TEventParams>> handlers = this.handlers.get(eventClass);
            EventHandler[] handlersArray = handlers.begin();
            for (int i = 0, n = handlers.size; i < n; i++) {
                handlersArray[i].handle(event, eventParams);
            }
            handlers.end();
            freeEventParams(eventParams);
        }
    }

    protected abstract TEventParams obtainEventParams(EventInfo event);
    protected abstract void freeEventParams(TEventParams params);

    // Optimize by avoiding of array creation here (create few reloaded methods with multiple event params).
    public void addHandler(EventHandler<TEventParams> handler, Class<? extends EventInfo>... classes) {
        for (int i = 0; i < classes.length; i++) {
            Class<? extends EventInfo> eventClass = classes[i];
            if (!handlers.containsKey(eventClass)) {
                handlers.put(eventClass, new SnapshotArray<EventHandler<TEventParams>>(EventHandler.class));
            }

            SnapshotArray<EventHandler<TEventParams>> handlers = this.handlers.get(eventClass);
            if (handlers.contains(handler, true)) {
                Gdx.app.error("EventManager", "Duplicated handler spotted for event: " + eventClass.getSimpleName() + ", handler: " + handler);
            } else {
                handlers.add(handler);
            }
        }
    }

    public void removeHandler(EventHandler<TEventParams> handler) {
        Iterator<Map.Entry<Class<? extends EventInfo>, SnapshotArray<EventHandler<TEventParams>>>> iterator = handlers.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<Class<? extends EventInfo>, SnapshotArray<EventHandler<TEventParams>>> entry = iterator.next();
            SnapshotArray<EventHandler<TEventParams>> classEventHandlers = entry.getValue();
            classEventHandlers.removeValue(handler, true);
            if (classEventHandlers.size == 0) {
                iterator.remove();
            }
        }
    }

    public void removeHandler(EventHandler<TEventParams> handler, Class<? extends EventInfo>... classes) {
        for (int i = 0; i < classes.length; i++) {
            Class<? extends EventInfo> eventClass = classes[i];
            if (!handlers.containsKey(eventClass)) {
                throw new RuntimeException("Handlers don't exist for class " + eventClass);
            }
            SnapshotArray<EventHandler<TEventParams>> classEventHandlers = handlers.get(eventClass);
            if (!classEventHandlers.removeValue(handler, true)) {
                throw new RuntimeException("Handlers don't contain " + handler);
            }
            if (classEventHandlers.size == 0) {
                handlers.remove(eventClass);
            }
        }
    }

    public void clear() {
        handlers.clear();
    }
}
