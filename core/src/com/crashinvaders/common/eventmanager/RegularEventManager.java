package com.crashinvaders.common.eventmanager;

import com.badlogic.gdx.utils.Pool;

public class RegularEventManager extends EventManager<RegularEventManager.EmptyEventParams> {

    private final Pool<EmptyEventParams> paramsPool = new Pool<EmptyEventParams>() {
        @Override
        protected EmptyEventParams newObject() {
            return new EmptyEventParams();
        }
    };

    @Override
    protected EmptyEventParams obtainEventParams(EventInfo event) {
        return paramsPool.obtain();
    }

    @Override
    protected void freeEventParams(EmptyEventParams params) {
        paramsPool.free(params);
    }


    public static class EmptyEventParams implements EventParams {

    }
}
