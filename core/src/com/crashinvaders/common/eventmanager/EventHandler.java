package com.crashinvaders.common.eventmanager;

public interface EventHandler<T extends EventParams> {
    void handle(EventInfo event, T params);
}
