package com.crashinvaders.common;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class MathUtilsExt {

    public static float maxAbs(float x, float y) {
        return Math.abs(x) >= Math.abs(y) ? x : y;
    }

    public static float minAbs(float x, float y) {
        return Math.abs(x) <= Math.abs(y) ? x : y;
    }

    public static float rate(float value, float minValue, float maxValue) {
        return (value - minValue) / (maxValue - minValue);
    }

    public static float clampAndRate(float value, float minValue, float maxValue) {
        value = MathUtils.clamp(value, minValue, maxValue);
        return (value - minValue) / (maxValue - minValue);
    }

    public static float lerpCut(float progress, float progressLowCut, float progressHighCut, float fromValue, float toValue) {
        progress = MathUtils.clamp(progress, progressLowCut, progressHighCut);
        float a = (progress - progressLowCut) / (progressHighCut - progressLowCut);
        return MathUtils.lerp(fromValue, toValue, a);
    }

    public static float lerpCut(float progress, float progressLowCut, float progressHighCut, float fromValue, float toValue, Interpolation interpolation) {
        progress = MathUtils.clamp(progress, progressLowCut, progressHighCut);
        float a = rate(progress, progressLowCut, progressHighCut);
        return MathUtils.lerp(fromValue, toValue, interpolation.apply(a));
    }

    /**
     * Evaluate distance between rectangle and point (x,y) into target
     */
    public static void evalDist(Rectangle rect, float x, float y, Vector2 target) {
        float xDist = 0;
        float yDist = 0;
        if (x < rect.x) {
            xDist = x - rect.x;
        } else if (x > rect.x + rect.width) {
            xDist = x - (rect.x + rect.width);
        }
        if (y < rect.y) {
            yDist = y - rect.y;
        } else if (y > rect.y + rect.height) {
            yDist = y - (rect.y + rect.height);
        }
        target.set(xDist, yDist);
    }

    public static boolean inRange(float value, float rangeMin, float rangeMax) {
        return (value >= rangeMin) && (value <= rangeMax);
    }

    /**
     * Computes number of digits in the number
     * https://stackoverflow.com/a/1306751/3802890
     */
    public static int length(int value) {
        if (value == 0) return 1;
        if (value < 0) value = -value;
        return (int)(Math.log10(value)+1);
    }
}
