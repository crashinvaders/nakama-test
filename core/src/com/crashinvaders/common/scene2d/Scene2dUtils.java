package com.crashinvaders.common.scene2d;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.SnapshotArray;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.badlogic.gdx.utils.reflect.Field;
import com.badlogic.gdx.utils.reflect.ReflectionException;
import com.crashinvaders.common.CommonUtils;

import static com.badlogic.gdx.utils.Align.*;

public class Scene2dUtils {

    public static final String TAG_INJECT_FIELDS = "InjectActorFields";
    public static final Vector2 tmpVec2 = new Vector2();
    public static final Rectangle tmpRect0 = new Rectangle();
    public static final Rectangle tmpRect1 = new Rectangle();

    /**
     * Cancel touch focus for actor itself and for whole children hierarchy if it's a group
     */
    public static void cancelTouchFocus(Stage stage, Actor actor) {
        stage.cancelTouchFocus(actor);

        if (actor instanceof Group) {
            SnapshotArray<Actor> children = ((Group)actor).getChildren();
            if (children.size > 0) {
                Object[] snapshot = children.begin();
                for (int i = 0; i < children.size; i++) {
                    cancelTouchFocus(stage, (Actor)snapshot[i]);
                }
                children.end();
            }
        }
    }

    //FIXME this is rather dirty method, listener.clicked(null, 0, 0) may produce null pointer
    public static void simulateClick(Actor actor) {
        for (EventListener eventListener : actor.getListeners()) {
            if (eventListener instanceof ClickListener) {
                ClickListener listener = (ClickListener) eventListener;
                listener.clicked(null, 0, 0);
            }
        }
    }

    /** Injects actors from group into target's fields annotated with {@link InjectActor} using reflection. */
    public static void injectActorFields(Object target, Group group) {
        Class<?> handledClass = target.getClass();
        while (handledClass != null && !handledClass.equals(Object.class)) {
            for (final Field field : ClassReflection.getDeclaredFields(handledClass)) {
                if (field != null && field.isAnnotationPresent(InjectActor.class)) {
                    try {
                        InjectActor annotation = field.getDeclaredAnnotation(InjectActor.class).getAnnotation(InjectActor.class);
                        String actorName = annotation.value();
                        if (actorName.length() == 0) {
                            actorName = field.getName();
                        }
                        Actor actor = group.findActor(actorName);
                        if (actor == null && actorName.equals(group.getName())) {
                            actor = group;
                        }
                        if (actor == null) {
                            Gdx.app.error(TAG_INJECT_FIELDS, "Can't find actor with name: " + actorName + " in group: " + group + " to inject into: " + target);
                        } else {
                            field.setAccessible(true);
                            field.set(target, actor);
                        }
                    } catch (final ReflectionException exception) {
                        Gdx.app.error(TAG_INJECT_FIELDS, "Unable to set value into field: " + field + " of object: " + target, exception);
                    }
                }
            }
            handledClass = handledClass.getSuperclass();
        }
    }

    public static void setColorRecursively(Group group, Color color) {
        for (Actor actor : group.getChildren()) {
            setColorRecursively(actor, color);
        }
    }
    private static void setColorRecursively(Actor actor, Color color) {
        if (actor instanceof Group) {
            Group group = (Group) actor;
            for (Actor child : group.getChildren()) {
                setColorRecursively(child, color);
            }
        }
        actor.setColor(color);
    }

    public static Vector2 setPositionRelative(Actor srcActor, int srcAlign, Actor dstActor, int dstAlign, float dstX, float dstY, boolean round) {
        Vector2 pos = tmpVec2.set(srcActor.getX(srcAlign), srcActor.getY(srcAlign));

        if ((dstAlign & right) != 0)
            pos.x -= dstActor.getWidth();
        else if ((dstAlign & left) == 0)
            pos.x -= dstActor.getWidth() / 2;

        if ((dstAlign & top) != 0)
            pos.y -= dstActor.getHeight();
        else if ((dstAlign & bottom) == 0)
            pos.y -= dstActor.getHeight() / 2;

        pos.add(dstX, dstY);

        if (round) {
            pos.set(pos.x, pos.y);
        }
        dstActor.setPosition(pos.x, pos.y);
        return pos;
    }

    public static Vector2 getPositionUnified(Actor source, Actor target) {
        return getPositionUnified(source, target, Align.bottomLeft, Align.bottomLeft);
    }

    public static Vector2 getPositionUnified(Actor source, Actor target, int sourceAlign) {
        return getPositionUnified(source, target, sourceAlign, Align.bottomLeft);
    }

    public static Vector2 getPositionUnified(Actor source, Actor target, int sourceAlign, int targetAlign) {
        float sourceShiftX = source.getWidth() * CommonUtils.getAlignFactorX(sourceAlign);
        float sourceShiftY = source.getHeight() * CommonUtils.getAlignFactorY(sourceAlign);
        float targetShiftX = target.getWidth() * CommonUtils.getAlignFactorX(targetAlign);
        float targetShiftY = target.getHeight() * CommonUtils.getAlignFactorY(targetAlign);

        if (source.getParent() == target.getParent()) {
            return tmpVec2.set(source.getX(sourceAlign), source.getY(sourceAlign))
                    .add(targetShiftX, targetShiftY);
        }

        Vector2 stageCoord = source.localToStageCoordinates(tmpVec2.set(sourceShiftX, sourceShiftY));
        Vector2 parentCoord = target.getParent().stageToLocalCoordinates(stageCoord);
        return tmpVec2.set(parentCoord.x, parentCoord.y)
                .add(targetShiftX, targetShiftY);
    }

    public static boolean checkIntersection(Actor actor0, Actor actor1) {
        actor0.localToStageCoordinates(tmpVec2.set(0f, 0f));
        float x0 = tmpVec2.x;
        float y0 = tmpVec2.y;
        float width0 = actor0.getWidth();
        float height0 = actor1.getHeight();
        actor1.localToStageCoordinates(tmpVec2.set(0f, 0f));
        float x1 = tmpVec2.x;
        float y1 = tmpVec2.y;
        float width1 = actor0.getWidth();
        float height1 = actor1.getHeight();
        return tmpRect0.set(x0, y0, width0, height0).overlaps(tmpRect1.set(x1, y1, width1, height1));
    }

    public static int readIntValue(TextField textField) {
        return readIntValue(textField, 0);
    }

    public static int readIntValue(TextField textField, int defaultValue) {
        String text = textField.getText();
        try {
            return Integer.valueOf(text);
        } catch (NumberFormatException ignore) {
            return defaultValue;
        }
    }
}
