package com.crashinvaders.common.scene2d.skin;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.AsynchronousAssetLoader;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.*;

/** Custom loader for extender skin class {@link SkinX}. {@link Params} is required. */
public class SkinXLoader extends AsynchronousAssetLoader<SkinX, SkinXLoader.Params> {
    private static final String TAG = SkinXLoader.class.getSimpleName();

    public SkinXLoader (FileHandleResolver resolver) {
        super(resolver);
    }

    @Override
    public Array<AssetDescriptor> getDependencies(String fileName, FileHandle file, Params parameter) {
        Array<AssetDescriptor> deps = new Array();
        if (parameter != null) {
            // Add resources as dependencies.
            for (int i = 0; i < parameter.resources.size; i++) {
                AssetDescriptor descriptor = parameter.resources.getValueAt(i);
                deps.add(descriptor);
            }
            // Add manual dependencies.
            for (int i = 0; i < parameter.dependencies.size; i++) {
                AssetDescriptor descriptor = parameter.dependencies.get(i);
                deps.add(descriptor);
            }
        }
        // Add automatic dependencies.
        Array<AssetDescriptor> automaticDependencies = resolveDependencies(file);
        for (int i = 0; i < automaticDependencies.size; i++) {
            AssetDescriptor assetDescriptor = automaticDependencies.get(i);
            Gdx.app.log(TAG, file.nameWithoutExtension() + ": Automatic dependency resolved: " + assetDescriptor.fileName);
            AssetDescriptor descriptor = assetDescriptor;
            deps.add(descriptor);
        }
        return deps;
    }

    @Override
    public void loadAsync (AssetManager manager, String fileName, FileHandle file, Params parameter) {
    }

    @Override
    public SkinX loadSync (AssetManager manager, String fileName, FileHandle file, Params parameter) {
        SkinX skin = new SkinX(manager);
        if (parameter != null) {
            for (ObjectMap.Entry<String, AssetDescriptor> resourceEntry : parameter.resources) {
                AssetDescriptor assetDescriptor = resourceEntry.value;
                Object loadedResource = manager.get(assetDescriptor.fileName, assetDescriptor.type);
                skin.add(resourceEntry.key, loadedResource, assetDescriptor.type);
            }
        }
        skin.load(file);
        return skin;
    }

    /** Reads markup and extracts required dependencies. */
    private Array<AssetDescriptor> resolveDependencies(FileHandle fontFile) {
        JsonValue root = new JsonReader().parse(fontFile);
        Array<AssetDescriptor> dependencies = new Array<>();

        // Fonts that should be loaded from asset manager.
        JsonValue fontFtValues = root.get("com.crashinvaders.common.scene2d.skin.SkinX$AssetFont");
        if (fontFtValues != null) {
            for (JsonValue fontEntry = fontFtValues.child; fontEntry != null; fontEntry = fontEntry.next) {
                String fontPath = fontEntry.getString("path");
                dependencies.add(new AssetDescriptor<>(fontPath, BitmapFont.class));
            }
        }

        // Texture atlases.
        JsonValue atlasValues = root.get("com.crashinvaders.common.scene2d.skin.SkinX$AssetAtlas");
        if (atlasValues == null) {
            throw new IllegalStateException("No atlases are defined for skin: " + fontFile);
        }
        for (JsonValue atlasEntry = atlasValues.child; atlasEntry != null; atlasEntry = atlasEntry.next) {
            String atlasPath = atlasEntry.getString("path");
            dependencies.add(new AssetDescriptor<>(atlasPath, TextureAtlas.class));
        }

        return dependencies;
    }

    static public class Params extends AssetLoaderParameters<SkinX> {
        /** Assets that will be added as resources on before skin load. */
        private final ArrayMap<String, AssetDescriptor> resources = new ArrayMap<>();
        /** Manual defined dependencies. Required for some asset based resources that are automatically resolved by SkinX,
         * such as {@link com.crashinvaders.common.scene2d.skin.SkinX.AssetFont}
         * or {@link com.crashinvaders.common.scene2d.skin.SkinX.AssetAtlas}*/
        private final Array<AssetDescriptor> dependencies = new Array<>();

        public void addResource(String name, String resourcePath, Class resourceType) {
            addResource(name, new AssetDescriptor<>(resourcePath, resourceType));
        }

        public void addResource(AssetDescriptor descriptor) {
            addResource(descriptor.fileName, descriptor);
        }

        public void addResource(String name, AssetDescriptor descriptor) {
            resources.put(name, descriptor);
        }

        public void addResource(String assetPath, Class assetType) {
            addDependency(new AssetDescriptor<>(assetPath, assetType));
        }

        public void addDependency(AssetDescriptor descriptor) {
            dependencies.add(descriptor);
        }
    }
}
