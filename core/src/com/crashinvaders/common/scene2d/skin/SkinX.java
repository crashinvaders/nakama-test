package com.crashinvaders.common.scene2d.skin;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.*;
import com.badlogic.gdx.utils.*;

/**
 * Skin that depends on {@link AssetManager} and may extract resources directly from it.
 * <br/><br/>
 * Unlike regular {@link Skin}, it doesn't contain single dedicated atlas, but any atlases should be defined
 * inside the skin markup file using "com.crashinvaders.common.scene2d.skin.SkinX$AssetAtlas" values.
 * <br/><br/>
 * Supports font loading from {@link AssetManager}, define them with "com.crashinvaders.common.scene2d.skin.SkinX$AssetFont".
 * <br/><br/>
 * By default {@link SkinX} doesn't dispose its internal resources, because they are managed by {@link AssetManager}. See {@link #setDisposable(boolean)} for more details.
 */
public class SkinX extends Skin {
    private static final String TAG = SkinX.class.getSimpleName();

    /** We will keep track of these drawables in order to check if related textures were unloaded from {@link AssetManager}. */
    private final ObjectSet<Drawable> assetManagerDrawables = new ObjectSet<>();

    private final AssetManager assets;

    private boolean disposable = false;

    public SkinX(AssetManager assets) {
        this.assets = assets;
    }

    public SkinX(AssetManager assets, TextureAtlas atlas) {
        super(atlas);
        this.assets = assets;
    }

    public AssetManager getAssets() {
        return assets;
    }

    /**
     * By default {@link SkinX} doesn't dispose its internal resources, because they are managed by {@link AssetManager},
     * but in case you need to enable it you can use this method.
     */
    public void setDisposable(boolean disposable) {
        this.disposable = disposable;
    }

    @Override
    public void add(String name, Object resource, Class type) {
        // This is important for AssetFont, because it is parsed from another class
        if (resource instanceof BitmapFont) {
            type = BitmapFont.class;
        }
        // This is important for AssetAtlas, because it is parsed from another class
        if (resource instanceof TextureAtlas) {
            type = TextureAtlas.class;
        }
        super.add(name, resource, type);
    }

    @Override
    public <T> T get(String name, Class<T> type) {
        try {
            return super.get(name, type);
        } catch (Exception ignored) {
        }
        // If skin doesn't contain particular resource, look into asset manager
        return assets.get(name, type);
    }

    @Override
    public void addRegions(TextureAtlas atlas) {
        addRegions(atlas, null);
    }

    /**
     * @param atlasName all regions will be available by the pattern: "[atlasName]/[regionName]"
     * @see #addRegions(TextureAtlas)
     */
    public void addRegions(TextureAtlas atlas, String atlasName) {
        Array<TextureAtlas.AtlasRegion> regions = atlas.getRegions();
        for (int i = 0, n = regions.size; i < n; i++) {
            TextureAtlas.AtlasRegion region = regions.get(i);
            String name = region.name;
            if (atlasName != null) {
                name = atlasName + "/" + name;
            }
            if (region.index != -1) {
                name += "_" + region.index;
            }
            add(name, region, TextureRegion.class);
        }
    }

    @Override
    public Drawable getDrawable(String name) {
        Drawable drawable = optional(name, Drawable.class);

        // Check if found drawable one of the AssetManager's textures
        if (drawable != null && assetManagerDrawables.contains(drawable)) {
            Texture texture = ((TextureRegionDrawable) drawable).getRegion().getTexture();
            if (!assets.isLoaded(name, Texture.class) || assets.get(name, Texture.class) != texture) {
                // Texture was disposed, so we should remove such drawable from SkinX cache
                remove(name, Drawable.class);
                assetManagerDrawables.remove(drawable);
                drawable = null;
            }
        }

        if (drawable != null) return drawable;
        drawable = (Drawable) optional(name, TiledDrawable.class);
        if (drawable != null) return drawable;

        // Use texture or texture region. If it has splits, use ninepatch. If it has rotation or whitespace stripping, use sprite.
        try {
            TextureRegion textureRegion = getRegion(name);
            if (textureRegion instanceof TextureAtlas.AtlasRegion) {
                TextureAtlas.AtlasRegion region = (TextureAtlas.AtlasRegion) textureRegion;
                if (region.splits != null)
                    drawable = new NinePatchDrawable(getPatch(name));
                else if (region.rotate || region.packedWidth != region.originalWidth || region.packedHeight != region.originalHeight)
                    drawable = new SpriteDrawable(getSprite(name));
            }
            if (drawable == null) drawable = new TextureRegionDrawable(textureRegion);
        } catch (GdxRuntimeException ignored) {
        }

        // Check for explicit registration of ninepatch, sprite, or tiled drawable.
        if (drawable == null) {
            NinePatch patch = optional(name, NinePatch.class);
            if (patch != null)
                drawable = new NinePatchDrawable(patch);
            else {
                Sprite sprite = optional(name, Sprite.class);
                if (sprite != null)
                    drawable = new SpriteDrawable(sprite);
//                else
//                    throw new GdxRuntimeException(
//                            "No Drawable, NinePatch, TextureRegion, Texture, or Sprite registered with name: " + name);
            }
        }

        // Look through word textures for drawable
        if (drawable == null) {
            try {
                Texture texture = assets.get(name, Texture.class);
                drawable = new TextureRegionDrawable(new TextureRegion(texture));
                assetManagerDrawables.add(drawable);
            } catch (GdxRuntimeException ignored) {
            }
        }

        if (drawable != null && drawable instanceof BaseDrawable) ((BaseDrawable) drawable).setName(name);

        if (drawable != null) {
            add(name, drawable, Drawable.class);
        }
        if (drawable == null) {
            Gdx.app.error(TAG, "Can't find drawable with name: " + name);
        }
        return drawable;
    }

    @Override
    protected Json getJsonLoader(FileHandle skinFile) {
        Json json = super.getJsonLoader(skinFile);

        json.setSerializer(TiledDrawable.class, new Json.ReadOnlySerializer() {
            public Object read(Json json, JsonValue jsonData, Class type) {
                String name = jsonData.getString("name");
                Color color = null;
                if (jsonData.has("color")) {
                    color = json.readValue("color", Color.class, jsonData);
                }
                Drawable drawable = newDrawable(name);
                if (drawable instanceof TextureRegionDrawable) {
                    com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable tiledDrawable =
                            new com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable(((TextureRegionDrawable) drawable));
                    tiledDrawable.setName(jsonData.name);
                    if (color != null) {
                        tiledDrawable.getColor().set(color);
                    }
                    drawable = tiledDrawable;
                } else {
                    Gdx.app.error(TAG, "Can't create tiled drawable from \"" + name + "\". Only texture regions supported.");
                }
                return drawable;
            }
        });

        json.setSerializer(AssetFont.class, new Json.ReadOnlySerializer() {
            @Override
            public Object read(Json json, JsonValue jsonData, Class type) {
                String freeTypeName = jsonData.getString("path");
                return assets.get(freeTypeName, BitmapFont.class);
            }
        });

        json.setSerializer(AssetAtlas.class, new Json.ReadOnlySerializer() {
            @Override
            public Object read(Json json, JsonValue jsonData, Class type) {
                String atlasPath = jsonData.getString("path");
                boolean useName = jsonData.getBoolean("useName", false);
                TextureAtlas atlas = assets.get(atlasPath, TextureAtlas.class);
                addRegions(atlas, useName ? jsonData.name : null);
                return atlas;
            }
        });

        return json;
    }

    public static class TiledDrawable {
        public String name;
        public Color color;
    }

    public static class AssetFont {
        /** Free type generated font name in asset manager. */
        public String path;
    }

    public static class AssetAtlas {
        /** Path to atlas to load regions from. Cannot be null. */
        public String path;
        /** If enabled, regions from that atlas will be accessible by the pattern: "{atlasName}/{regionName}" */
        public boolean useName = false;
    }

    @Override
    public void dispose() {
        if (disposable) {
            super.dispose();
        }
    }
}
