package com.crashinvaders.common.scene2d;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.crashinvaders.common.graphics.ParticleEffectX;

public class ParticleEffectActor extends Actor {
    private static final Vector2 TMP_VEC2 = new Vector2();

    private final ParticleEffectX effect;
    private boolean effectEnabled = true;
    private boolean alphaModulation = false;

    public ParticleEffectActor(ParticleEffectX effect) {
        this.effect = effect;
    }

    public ParticleEffectX getEffect() {
        return effect;
    }

    public boolean isEffectEnabled() {
        return effectEnabled;
    }

    public void setEffectEnabled(boolean effectEnabled) {
        this.effectEnabled = effectEnabled;
    }

    public boolean isAlphaModulation() {
        return alphaModulation;
    }

    public void setAlphaModulation(boolean alphaModulation) {
        this.alphaModulation = alphaModulation;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (effectEnabled) {
            effect.update(delta);
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (effectEnabled) {
            Color color = getColor();
            batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
//            effect.setPosition(getX() + getWidth() * 0.5f, getY() + getHeight() * 0.5f);

            if (alphaModulation) {
                effect.drawWithAlphaModulation(batch, parentAlpha * color.a);
            } else {
                effect.draw(batch);
            }
        }
    }

    @Override
    protected void positionChanged() {
        super.positionChanged();
        effect.setPosition(getX() + getWidth() * 0.5f, getY() + getHeight() * 0.5f);
        effect.update(0f);
    }

    @Override
    protected void rotationChanged() {
        super.rotationChanged();
        effect.setRotation(getRotation());
    }
}
