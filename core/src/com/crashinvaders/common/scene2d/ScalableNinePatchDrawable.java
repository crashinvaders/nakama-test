/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package com.crashinvaders.common.scene2d;

import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;

/** Same {@link NinePatchDrawable}, but has extra scale property. */
public class ScalableNinePatchDrawable extends NinePatchDrawable {

	private float scale;

	public float getScale() {
		return scale;
	}

	public void setScale(float scale) {
		this.scale = scale;
	}

	@Override
	public void setPatch (NinePatch patch) {
		super.setPatch(patch);
		updateDimensions();
	}

	private void updateDimensions() {
		NinePatch patch = this.getPatch();
		if (patch == null) return;

		setMinWidth(patch.getTotalWidth() * scale);
		setMinHeight(patch.getTotalHeight() * scale);
		setTopHeight(patch.getPadTop() * scale);
		setRightWidth(patch.getPadRight() * scale);
		setBottomHeight(patch.getPadBottom() * scale);
		setLeftWidth(patch.getPadLeft() * scale);
	}
}
