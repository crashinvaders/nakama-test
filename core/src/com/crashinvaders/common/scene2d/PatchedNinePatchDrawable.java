package com.crashinvaders.common.scene2d;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;

public class PatchedNinePatchDrawable extends NinePatchDrawable {

    private float minWidth;
    private float minHeight;

    public PatchedNinePatchDrawable() {
    }

    public PatchedNinePatchDrawable(NinePatch patch) {
        super(patch);
    }

    public PatchedNinePatchDrawable(NinePatchDrawable drawable) {
        super(drawable);
    }

    @Override
    public void draw(Batch batch, float x, float y, float width, float height) {
        if (width >= minWidth && height >= minHeight) {
            super.draw(batch, x, y, width, height);
        }
    }

    @Override
    public void draw(Batch batch, float x, float y,
                     float originX, float originY,
                     float width, float height,
                     float scaleX, float scaleY,
                     float rotation) {
        if (width >= minWidth && height >= minHeight) {
            super.draw(batch, x, y, originX, originY, width, height, scaleX, scaleY, rotation);
        }
    }

    @Override
    public void setPatch(NinePatch patch) {
        super.setPatch(patch);
        this.minWidth = patch.getLeftWidth() + patch.getMiddleWidth() + patch.getRightWidth();
        this.minHeight = patch.getBottomHeight() + patch.getMiddleHeight() + patch.getTopHeight();
    }
}
