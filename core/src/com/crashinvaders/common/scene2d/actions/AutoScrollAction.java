package com.crashinvaders.common.scene2d.actions;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.crashinvaders.common.Timer;

public class AutoScrollAction extends Action implements Timer.Listener {
    private static final float DELAY = 10f;
    private static final float SPEED_MAX = 20f;
    private static final float SPEED_INCR = 20f;

    private final ScrollPane scrollPane;
    private final InputListener inputListener;
    private final Timer timer;

    private float speed = 0f;
    private boolean active = false;
    private boolean finished = false;

    public AutoScrollAction(final ScrollPane scrollPane) {
        this.scrollPane = scrollPane;

        scrollPane.addListener(inputListener = new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                stopScroll();
                return false;
            }
        });

        timer = new Timer();
        timer.start(DELAY, this);
    }

    @Override
    public void onTimeUp() {
        active = true;
    }

    @Override
    public boolean act(float delta) {
        timer.update(delta);

        if (active) {
            doAutoScroll(delta);
        }

        if (scrollPane.getScrollPercentY() >= 1f) {
            finishAction();
        }

        return finished;
    }

    private void stopScroll() {
        if (finished || !active) return;

        active = false;
        speed = 0f;
        timer.restart();
    }

    private void finishAction() {
        finished = true;
        scrollPane.removeListener(inputListener);
    }

    private void doAutoScroll(float delta) {
        speed = Math.min(SPEED_MAX, speed + SPEED_INCR * delta);

        float distance = speed * delta;
        scrollPane.setScrollY(scrollPane.getScrollY() + distance);
    }
}
