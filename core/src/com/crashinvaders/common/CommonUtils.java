package com.crashinvaders.common;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.CharArray;

import java.util.ArrayList;
import java.util.List;

import static com.badlogic.gdx.utils.Align.*;

public class CommonUtils {
    private static final Color tmpColor = new Color();

    public static <T> T random(T[] array) {
        int idx = MathUtils.random(array.length - 1);
        return array[idx];
    }

    /**
     * <p>Null safe comparison of Comparables.</p>
     *
     * @param <T> type of the values processed by this method
     * @param c1  the first comparable, may be null
     * @param c2  the second comparable, may be null
     * @param nullGreater if true {@code null} is considered greater
     *  than a non-{@code null} value or if false {@code null} is
     *  considered less than a Non-{@code null} value
     * @return a negative value if c1 &lt; c2, zero if c1 = c2
     *  and a positive value if c1 &gt; c2
     * @see java.util.Comparator#compare(Object, Object)
     */
    public static <T extends Comparable<? super T>> int compare(final T c1, final T c2, final boolean nullGreater) {
        if (c1 == c2) {
            return 0;
        } else if (c1 == null) {
            return nullGreater ? 1 : -1;
        } else if (c2 == null) {
            return nullGreater ? -1 : 1;
        }
        return c1.compareTo(c2);
    }

    /** This is a replication of Integer.compare() to support Android API less than 19 */
    public static int compare(int x, int y) {
        return (x < y) ? -1 : ((x == y) ? 0 : 1);
    }

    /** This is a replication of Float.compare() to support Android API less than 19 */
    public static int compare(float x, float y) {
        return (x < y) ? -1 : ((x == y) ? 0 : 1);
    }

    /** This is a replication of Boolean.compare() to support Android API less than 19 */
    public static int compare(boolean x, boolean y) {
        return (x == y) ? 0 : (x ? 1 : -1);
    }

    /** @return 0..1 depending on align value. */
    public static float getAlignFactorX(int align) {
        if ((align & left) != 0) return 0f;
        if ((align & right) != 0) return 1f;
        return 0.5f;
    }
    /** @return 0..1 depending on align value. */
    public static float getAlignFactorY(int align) {
        if ((align & bottom) != 0) return 0f;
        if ((align & top) != 0) return 1f;
        return 0.5f;
    }

    public static boolean stringEquals(String s0, String s1) {
        if (s0 == null && s1 == null) return true;
        return s0 != null && s0.equals(s1);
    }

    /** @param time in seconds
     *  @return String of "H:MM" format. */
    public static String formatToTimeString(int time) {
        String minutes = time / 60 + "";
        String seconds = time % 60 + "";
        if (seconds.length() == 1) {
            seconds = "0" + seconds;
        }
        return minutes + ":" + seconds;
    }

    public static Color parseHexColor(String hexCode) {
        switch (hexCode.length()) {
            case 3:
                return parseHexColor3(hexCode);
            case 4:
                return parseHexColor4(hexCode);
            case 6:
                return parseHexColor6(hexCode);
            case 8:
                return parseHexColor8(hexCode);
            default:
                throw new IllegalArgumentException("Wrong format, HEX string value should be either 3, 4, 6 or 8 letters long.");
        }
    }

    public static Color parseHexColor3(String hex) {
        if (hex.length() != 3) throw new IllegalArgumentException("HEX string value should be exact 3 letters long.");
        int r = Integer.valueOf(hex.substring(0, 1), 16);
        int g = Integer.valueOf(hex.substring(1, 2), 16);
        int b = Integer.valueOf(hex.substring(2, 3), 16);
        return tmpColor.set(r / 15f, g / 15f, b / 15f, 1f);
    }

    public static Color parseHexColor4(String hex) {
        if (hex.length() != 4) throw new IllegalArgumentException("HEX string value should be exact 4 letters long.");
        int r = Integer.valueOf(hex.substring(0, 1), 16);
        int g = Integer.valueOf(hex.substring(1, 2), 16);
        int b = Integer.valueOf(hex.substring(2, 3), 16);
        int a = Integer.valueOf(hex.substring(3, 4), 16);
        return tmpColor.set(r / 15f, g / 15f, b / 15f, a / 15f);
    }

    public static Color parseHexColor6(String hex) {
        if (hex.length() != 6) throw new IllegalArgumentException("HEX string value should be exact 6 letters long.");
        int r = Integer.valueOf(hex.substring(0, 2), 16);
        int g = Integer.valueOf(hex.substring(2, 4), 16);
        int b = Integer.valueOf(hex.substring(4, 6), 16);
        return tmpColor.set(r / 255f, g / 255f, b / 255f, 1f);
    }

    public static Color parseHexColor8(String hex) {
        if (hex.length() != 8) throw new IllegalArgumentException("HEX string value should be exact 8 letters long.");
        int r = Integer.valueOf(hex.substring(0, 2), 16);
        int g = Integer.valueOf(hex.substring(2, 4), 16);
        int b = Integer.valueOf(hex.substring(4, 6), 16);
        int a = Integer.valueOf(hex.substring(6, 8), 16);
        return tmpColor.set(r / 255f, g / 255f, b / 255f, a / 255f);
    }

    /** @return Screen diagonal in inches. */
    public static float getScreenSize() {
        float density = 160f * Gdx.graphics.getDensity();
        float w = Gdx.graphics.getWidth();
        float h = Gdx.graphics.getHeight();
        return (float)Math.sqrt(w*w + h*h) / density;
    }

    public static <T> Array<T> toArray(List<T> list) {
        return toArray(list, new Array<T>(list.size()));
    }

    public static <T> Array<T> toArray(List<T> list, Array<T> out) {
        for (T item : list) {
            out.add(item);
        }
        return out;
    }

    /** Null safe variation of {@link #toArray(List)}.
     * @return empty array if list is null.*/
    public static <T> Array<T> toArraySafe(List<T> list) {
        return toArraySafe(list, new Array<T>(list != null ? list.size() : 4));
    }

    /** Null safe variation of {@link #toArray(List)}.
     * @return empty array if list is null.*/
    public static <T> Array<T> toArraySafe(List<T> list, Array<T> out) {
        if (list == null) return out;
        return toArray(list, out);
    }

    public static <T> List<T> toList(Array<T> array) {
        return toList(array, new ArrayList<T>());
    }

    public static <T> List<T> toList(Array<T> array, List<T> out) {
        for (int i = 0; i < array.size; i++) {
            out.add(array.get(i));
        }
        return out;
    }
}
