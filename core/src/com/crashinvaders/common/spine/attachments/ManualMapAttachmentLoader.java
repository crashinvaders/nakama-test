package com.crashinvaders.common.spine.attachments;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.ObjectMap;
import com.esotericsoftware.spine.Skin;
import com.esotericsoftware.spine.attachments.*;

/** Use <code>#add()</code> methods to register all texture regions manually. */
public class ManualMapAttachmentLoader implements AttachmentLoader {

	private final ObjectMap<String, TextureRegion> regions = new ObjectMap<>();

	public ManualMapAttachmentLoader() {
	}

	public ManualMapAttachmentLoader(ObjectMap<String, TextureRegion> regions) {
		this.regions.putAll(regions);
	}

	public ManualMapAttachmentLoader add(String name, TextureRegion region) {
		regions.put(name, region);
		return this;
	}

	public ManualMapAttachmentLoader add(String name, Texture texture) {
		regions.put(name, new TextureRegion(texture));
		return this;
	}

	public ManualMapAttachmentLoader add(TextureAtlas atlas) {
		for (AtlasRegion region : atlas.getRegions()) {
			regions.put(region.name, region);
		}
		return this;
	}

	public RegionAttachment newRegionAttachment (Skin skin, String name, String path) {
		TextureRegion region = regions.get(path);
		if (region == null) throw new RuntimeException("Region not found (region attachment: " + name + ")");
		RegionAttachment attachment = new RegionAttachment(name);
		attachment.setRegion(region);
		return attachment;
	}

	public MeshAttachment newMeshAttachment (Skin skin, String name, String path) {
		TextureRegion region = regions.get(path);
		if (region == null) throw new RuntimeException("Region not found in atlas: " + path + " (mesh attachment: " + name + ")");
		MeshAttachment attachment = new MeshAttachment(name);
		attachment.setRegion(region);
		return attachment;
	}

	public BoundingBoxAttachment newBoundingBoxAttachment (Skin skin, String name) {
		return new BoundingBoxAttachment(name);
	}

	public ClippingAttachment newClippingAttachment (Skin skin, String name) {
		return new ClippingAttachment(name);
	}

	public PathAttachment newPathAttachment (Skin skin, String name) {
		return new PathAttachment(name);
	}

	public PointAttachment newPointAttachment (Skin skin, String name) {
		return new PointAttachment(name);
	}
}
