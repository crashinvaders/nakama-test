package com.crashinvaders.common.spine.attachments;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.esotericsoftware.spine.Skin;
import com.esotericsoftware.spine.attachments.*;

public class SkinAttachmentLoader implements AttachmentLoader {

	private final com.badlogic.gdx.scenes.scene2d.ui.Skin sceneSkin;

	private TextureRegion emptyRegion;

	public SkinAttachmentLoader(com.badlogic.gdx.scenes.scene2d.ui.Skin sceneSkin) {
		if (sceneSkin == null) throw new IllegalArgumentException("sceneSkin cannot be null.");
		this.sceneSkin = sceneSkin;
	}

	public void setEmptyRegion(String drawableName) {
		this.emptyRegion = sceneSkin.getRegion(drawableName);
	}

	public void setEmptyRegion(TextureRegion emptyTextureRegion) {
		this.emptyRegion = emptyTextureRegion;
	}

	public RegionAttachment newRegionAttachment (Skin skin, String name, String path) {
		TextureRegion region = resolveRegion(path);
		if (region == null) throw new RuntimeException("Region not found in atlas: " + path + " (region attachment: " + name + ")");
		RegionAttachment attachment = new RegionAttachment(name);
		attachment.setRegion(region);
		return attachment;
	}

	public MeshAttachment newMeshAttachment (Skin skin, String name, String path) {
		TextureRegion region = resolveRegion(path);
		if (region == null) throw new RuntimeException("Region not found in atlas: " + path + " (mesh attachment: " + name + ")");
		MeshAttachment attachment = new MeshAttachment(name);
		attachment.setRegion(region);
		return attachment;
	}

	public BoundingBoxAttachment newBoundingBoxAttachment (Skin skin, String name) {
		return new BoundingBoxAttachment(name);
	}

	public ClippingAttachment newClippingAttachment (Skin skin, String name) {
		return new ClippingAttachment(name);
	}

	public PathAttachment newPathAttachment (Skin skin, String name) {
		return new PathAttachment(name);
	}

	public PointAttachment newPointAttachment (Skin skin, String name) {
		return new PointAttachment(name);
	}

	private TextureRegion resolveRegion(String path) {
		TextureRegion region = sceneSkin.getRegion(path);
		if (region == null && emptyRegion != null) {
			region = emptyRegion;
		}
		return region;
	}
}
