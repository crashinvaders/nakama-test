package com.crashinvaders.common.spine.attachments;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.esotericsoftware.spine.Skin;
import com.esotericsoftware.spine.attachments.*;

public class MultiAtlasAttachmentLoader implements AttachmentLoader {

	private final TextureAtlas mainAtlas;
	private final Array<AltAtlasNode> altAtlases = new Array<>(true, 8);
	private final ObjectMap<String, TextureRegion> textures = new ObjectMap<>(8);

	private TextureRegion emptyRegion = null;

	public MultiAtlasAttachmentLoader(TextureAtlas atlas) {
		if (atlas == null) throw new IllegalArgumentException("atlas cannot be null.");
		mainAtlas = atlas;
	}

	public void addAltAtlas(String pathPrefix, TextureAtlas atlas) {
		AltAtlasNode atlasNode = new AltAtlasNode(pathPrefix, atlas);
		altAtlases.add(atlasNode);
	}

	public void addTexture(String path, Texture texture) {
		textures.put(path, new TextureRegion(texture));
	}

	public void setEmptyRegion(TextureRegion emptyTextureRegion) {
		this.emptyRegion = emptyTextureRegion;
	}

	public void setEmptyRegion(String regionName) {
		this.emptyRegion = resolveRegion(regionName);
	}

	public RegionAttachment newRegionAttachment (Skin skin, String name, String path) {
		TextureRegion region = resolveRegion(path);
		if (region == null) throw new RuntimeException("Region not found: " + path + " (region attachment: " + name + ")");
		RegionAttachment attachment = new RegionAttachment(name);
		attachment.setRegion(region);
		return attachment;
	}

	public MeshAttachment newMeshAttachment (Skin skin, String name, String path) {
		TextureRegion region = resolveRegion(path);
		if (region == null) throw new RuntimeException("Region not found: " + path + " (mesh attachment: " + name + ")");
		MeshAttachment attachment = new MeshAttachment(name);
		attachment.setRegion(region);
		return attachment;
	}

	public BoundingBoxAttachment newBoundingBoxAttachment (Skin skin, String name) {
		return new BoundingBoxAttachment(name);
	}

	public ClippingAttachment newClippingAttachment (Skin skin, String name) {
		return new ClippingAttachment(name);
	}

	public PathAttachment newPathAttachment (Skin skin, String name) {
		return new PathAttachment(name);
	}

	public PointAttachment newPointAttachment (Skin skin, String name) {
		return new PointAttachment(name);
	}

	private TextureRegion resolveRegion(String path) {
		// Check main atlas first.
		AtlasRegion mainAtlasRegion = mainAtlas.findRegion(path);
		if (mainAtlasRegion != null) return mainAtlasRegion;

		// Check individual textures.
		TextureRegion texture = textures.get(path);
		if (texture != null) {
			return texture;
		}

		// Check alternative atlases.
		for (int i = 0; i < altAtlases.size; i++) {
			AltAtlasNode atlasNode = altAtlases.get(i);
			if (path.startsWith(atlasNode.pathPrefix)) {
				String localPath = path.replace(atlasNode.pathPrefix, "");
				AtlasRegion region = atlasNode.atlas.findRegion(localPath);
				if (region != null) {
					return region;
				}
			}
		}

		// If none found, use empty region (if present).
		if (emptyRegion != null) {
			return emptyRegion;
		}
		return null;
	}

	private static class AltAtlasNode {
		final String pathPrefix;
		final TextureAtlas atlas;

		public AltAtlasNode(String pathPrefix, TextureAtlas atlas) {
			this.pathPrefix = pathPrefix;
			this.atlas = atlas;
		}
	}
}
