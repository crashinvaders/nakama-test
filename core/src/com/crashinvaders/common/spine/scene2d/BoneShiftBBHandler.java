package com.crashinvaders.common.spine.scene2d;

import com.esotericsoftware.spine.Bone;
import com.esotericsoftware.spine.Skeleton;

public abstract class BoneShiftBBHandler implements BBAttachHandler {

    private final Bone bone;
    private final float originalY;

    private float yPressShift = -2f;

    private boolean focused = false;
    private boolean enabled = true;

    public BoneShiftBBHandler(Skeleton skeleton, String boneName) {
        bone = skeleton.findBone(boneName);
        originalY = bone.getY();
    }

    @Override
    public void onTouchDown(float x, float y) {
        focused = true;
        bone.setY(originalY + yPressShift);
    }

    @Override
    public void onEnter(float x, float y) {
        if (!focused) return;
        bone.setY(originalY + yPressShift);
    }

    @Override
    public void onExit(float x, float y) {
        if (!focused) return;
        bone.setY(originalY);
    }

    @Override
    public void onTouchUp(float x, float y, boolean over) {
        focused = false;
        bone.setY(originalY);

        if (over) {
            onClick();
        }
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public BoneShiftBBHandler pressShift(float shift) {
        this.yPressShift = shift;
        return this;
    }

    protected abstract void onClick();
}
