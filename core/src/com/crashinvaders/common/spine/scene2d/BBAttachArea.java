package com.crashinvaders.common.spine.scene2d;
/* Metaphore 04-Jun-14 21:09 */

import com.badlogic.gdx.math.Vector2;
import com.esotericsoftware.spine.Bone;
import com.esotericsoftware.spine.Skeleton;
import com.esotericsoftware.spine.Slot;
import com.esotericsoftware.spine.attachments.BoundingBoxAttachment;

public class BBAttachArea {
    private final BoundingBox BOUNDING_BOX_TMP = new BoundingBox();

    private final Bone bone;
    private final Slot slot;
    private final BoundingBoxAttachment attachment;

    private final Vector2 tmpVec2 = new Vector2();
    private final float[] tmpVertices;

    public BBAttachArea(String attachName, String slotName, Skeleton skeleton) {
        slot = skeleton.findSlot(slotName);
        if (slot == null) {
            throw new IllegalArgumentException("Cant find BB slot '"+slotName+"' of skeleton '"+skeleton.getData().getName()+"'");
        }

        attachment = (BoundingBoxAttachment) skeleton.getAttachment(slotName, attachName);
        if (attachment == null) {
            throw new IllegalArgumentException("Cant find BB attachment '"+attachName+"' for slot '"+slotName+"' of skeleton '"+skeleton.getData().getName()+"'");
        }

        bone = slot.getBone();

        tmpVertices = new float[attachment.getVertices().length];
    }

    public BBAttachArea(Slot slot, BoundingBoxAttachment attachment) {
        this.slot = slot;
        this.bone = slot.getBone();
        this.attachment = attachment;

        tmpVertices = new float[attachment.getVertices().length];
    }

    public Vector2 getAreaCenter() {
        Vector2 result = tmpVec2;
        float[] vertices = tmpVertices;

        computeWorldVertices(vertices);

        float x = 0;
        float y = 0;
        int verticesAmount = vertices.length / 2;
        for (int i = 0; i < verticesAmount; i++) {
            x += vertices[i*2];
            y += vertices[i*2+1];
        }
        x /= verticesAmount;
        y /= verticesAmount;
        result.set(x, y);
        return result;
    }

    /** Checks for inclusive by BB rule */
    public boolean contains(float x, float y) {
        if (slot.getAttachment() != attachment) return false;

        BoundingBox bb = getBB();
        return bb.contains(x, y);
    }

    /**
     * Beware, BoundingBox returned instance is reusing internally
     */
    public BoundingBox getBB() {
        float[] vertices = tmpVertices;
        computeWorldVertices(vertices);

        float maxX = -Float.MAX_VALUE;
        float minX = Float.MAX_VALUE;
        float maxY = -Float.MAX_VALUE;
        float minY = Float.MAX_VALUE;

        int verticesAmount = vertices.length / 2;
        for (int i = 0; i < verticesAmount; i++) {
            float xV = vertices[i*2];
            float yV = vertices[i*2+1];
            if (xV > maxX) maxX = xV;
            if (xV < minX) minX = xV;
            if (yV > maxY) maxY = yV;
            if (yV < minY) minY = yV;
        }

        return BOUNDING_BOX_TMP.set(minX, maxX, minY, maxY);
    }

    public float[] getVertices() {
        float[] vertices = tmpVertices;
        computeWorldVertices(vertices);
        return vertices;
    }

    private void computeWorldVertices(float[] vertices) {
        attachment.computeWorldVertices(slot, 0, attachment.getWorldVerticesLength(), vertices, 0, 2);
    }

    public static class BoundingBox {
        private float x0, x1, y0, y1;

        private BoundingBox() {
        }

        private BoundingBox set(float x0, float x1, float y0, float y1) {
            //TODO check for min-max order
            this.x0 = x0;
            this.x1 = x1;
            this.y0 = y0;
            this.y1 = y1;
            return this;
        }

        public float getX0() {
            return x0;
        }

        public float getX1() {
            return x1;
        }

        public float getY0() {
            return y0;
        }

        public float getY1() {
            return y1;
        }

        public float getWidth() {
            return Math.abs(x0 - x1);
        }

        public float getHeight() {
            return Math.abs(y0 - y1);
        }

        public boolean contains(float x, float y) {
            return x > x0 && x < x1 && y > y0 && y < y1;
        }

        @Override
        public String toString() {
            return  "x0=" + x0 +
                    ", targetX=" + x1 +
                    ", y0=" + y0 +
                    ", targetY=" + y1;
        }
    }
}
