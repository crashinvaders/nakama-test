package com.crashinvaders.common.spine.scene2d;

public abstract class ClickBBAttachHandler implements BBAttachHandler {

    private boolean enabled = true;
    private boolean pressed = true;
    private boolean withinBounds = false;

    @Override
    public void onTouchDown(float x, float y) {
        pressed = true;
        withinBounds = true;
    }

    @Override
    public void onTouchUp(float x, float y, boolean over) {
        if (isVisuallyPressed() && over) {
            onClick(x, y);
        }
        pressed = false;
        withinBounds = false;
    }

    @Override
    public void onEnter(float x, float y) {
        if (pressed) {
            withinBounds = true;
        }
    }

    @Override
    public void onExit(float x, float y) {
        if (pressed) {
            withinBounds = false;
        }
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isPressed() {
        return pressed;
    }

    public boolean isVisuallyPressed() {
        return pressed & withinBounds;
    }

    protected abstract void onClick(float x, float y);
}
