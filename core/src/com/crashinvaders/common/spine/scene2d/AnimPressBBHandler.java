package com.crashinvaders.common.spine.scene2d;

import com.esotericsoftware.spine.AnimationState;

public abstract class AnimPressBBHandler extends VisualBBHandler {
    private final AnimationState animState;
    private final String downAnimation;
    private final String upAnimation;
    private final int track;

    public AnimPressBBHandler(AnimationState animState, int track, String downAnimation, String upAnimation) {
        this.animState = animState;
        this.downAnimation = downAnimation;
        this.upAnimation = upAnimation;
        this.track = track;
    }

    @Override
    protected void visualizePress() {
        super.visualizePress();
//        animState.clearTrack(track);
        animState.setAnimation(track, downAnimation, false);
    }

    @Override
    protected void visualizeRelease() {
        super.visualizeRelease();
//        animState.clearTrack(track);
        animState.setAnimation(track, upAnimation, false);
    }
}
