package com.crashinvaders.common.spine.scene2d;

public interface BBAttachHandler {
    void onTouchDown(float x, float y);
    void onEnter(float x, float y);
    void onExit(float x, float y);
    /** @param over Is over related skeleton actor (not BB itself) */
    void onTouchUp(float x, float y, boolean over);

    boolean isEnabled();
}
