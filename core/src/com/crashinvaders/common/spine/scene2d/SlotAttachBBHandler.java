package com.crashinvaders.common.spine.scene2d;

import com.esotericsoftware.spine.Skeleton;
import com.esotericsoftware.spine.Slot;
import com.esotericsoftware.spine.attachments.Attachment;

public abstract class SlotAttachBBHandler extends VisualBBHandler {

    protected final Slot slot;
    protected final Skeleton skeleton;

    private final Attachment origAttach;
    private Attachment attachUp;
    private Attachment attachDown;

    public SlotAttachBBHandler(Skeleton skeleton, String slotName) {
        this.skeleton = skeleton;
        slot = skeleton.findSlot(slotName);
        origAttach = slot.getAttachment();
    }

    public SlotAttachBBHandler attachUp(String attachName) {
        attachUp = findAttach(attachName);
        return this;
    }

    public SlotAttachBBHandler attachDown(String attachName) {
        attachDown = findAttach(attachName);
        return this;
    }

    @Override
    protected void visualizePress() {
        super.visualizePress();
        slot.setAttachment(attachDown != null ? attachDown : origAttach);
    }

    @Override
    protected void visualizeRelease() {
        super.visualizeRelease();
        slot.setAttachment(attachUp != null ? attachUp : origAttach);
    }

    private Attachment findAttach(String attachName) {
        return skeleton.getAttachment(slot.getData().getName(), attachName);
    }
}
