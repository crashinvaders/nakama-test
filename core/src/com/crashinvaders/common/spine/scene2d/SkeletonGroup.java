package com.crashinvaders.common.spine.scene2d;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.*;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.scenes.scene2d.utils.Layout;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.badlogic.gdx.utils.reflect.Field;
import com.badlogic.gdx.utils.reflect.ReflectionException;
import com.crashinvaders.common.MatrixUtils;
import com.crashinvaders.common.spine.SpineUtils;
import com.esotericsoftware.spine.*;
import com.esotericsoftware.spine.attachments.Attachment;
import com.esotericsoftware.spine.attachments.BoundingBoxAttachment;
import com.github.czyzby.kiwi.util.gdx.reflection.Reflection;

public class SkeletonGroup extends Group {
    public static final String DEFAULT_GENERAL_BB = "bb-general";
    private static final Color tmpColor = new Color();
    private static final Vector2 tmpVec2 = new Vector2();

    private final ArrayMap<Actor, ChildContainer> childrenNodes = new ArrayMap<>();

    private final Skeleton skeleton;
    private final SkeletonGroupRenderer skeletonRenderer;
    private final AnimationState animationState;

    private final Array<BBAttachNode> bbAttachNodes;

    private Scaling scaling = Scaling.NONE;
    private float skelWidth = 0f;
    private float skelHeight = 0f;
    private float skelScale = 1f;

    public SkeletonGroup(SkeletonData skeletonData) {
        this(skeletonData, new AnimationStateData(skeletonData));
    }
    public SkeletonGroup(SkeletonData skeletonData, AnimationStateData animationStateData) {
        if (skeletonData == null) {
            throw new NullPointerException("skeletonData cannot be null");
        }

        this.setTransform(false);

        bbAttachNodes = new Array<>();

        skeleton = new Skeleton(skeletonData);
        skeletonRenderer = new SkeletonGroupRenderer();
        skeletonRenderer.setPremultipliedAlpha(false);
        animationState = new AnimationState(animationStateData);

        setTouchable(Touchable.enabled);

        addListener(new BBAttachInputListener());

        trySizeByGeneralBB();
    }

    /** @deprecated Use {@link #addActor(Actor, String, String)} instead. */
    @Override
    public void addActor(Actor actor) {
        throw new IllegalArgumentException("Use #addActor(Actor, String, String) instead." );
    }

    public void addActor(Actor actor, String slotName, String attachName) {
        Slot slot = skeleton.findSlot(slotName);
        BoundingBoxAttachment attachment = (BoundingBoxAttachment)skeleton.getAttachment(slotName, attachName);
        if (attachment == null) {
            throw new IllegalArgumentException("Can't find attachment " + attachName + " for slot: " + slotName);
        }
        ChildContainer childContainer = new ChildContainer(actor, slot, attachment);
        childrenNodes.put(actor, childContainer);
        super.addActor(childContainer);
    }

    @Override
    public boolean removeActor(Actor actor, boolean unfocus) {
        ChildContainer childContainer = childrenNodes.removeKey(actor);
        if (childContainer != null) {
            super.removeActor(childContainer, unfocus);
            return true;
        }
        return false;
    }

    /** Works only for scaling set to Scaling.NONE */
    public void setSkelScale(float skelScale) {
        this.skelScale = skelScale;

        skeleton.getRootBone().setRotation(0f);
        skeleton.getRootBone().setScale(this.skelScale * getScaleX());
        skeleton.updateWorldTransform();
        trySizeByGeneralBB();

        Group parent = getParent();
        if (parent instanceof Layout) {
            ((Layout) parent).invalidateHierarchy();
        }
    }

    public void setScaling(Scaling scaling) {
        this.scaling = scaling;
        recomputeSkeletonScale();
    }

    /** Try set size from general bb, if exists */
    private void trySizeByGeneralBB() {
        Slot slot = skeleton.findSlot(DEFAULT_GENERAL_BB);
        if (slot == null) return;

        Attachment attachment = slot.getAttachment();
        if (attachment == null) return;

        if (attachment.getName().equals(DEFAULT_GENERAL_BB)) {
//            Gdx.app.debug("SkeletonActor", "general bb found and used as default size from skeleton: " + skeleton.getData().getName());

            BBAttachArea attachArea = new BBAttachArea(slot, (BoundingBoxAttachment) attachment);
            setSizeFromBB(attachArea);
        }
    }

    public AnimationState getAnimState() {
        return animationState;
    }

    public Skeleton getSkeleton() {
        return skeleton;
    }

    public void updateSkeletonTransform() {
        skeleton.setPosition(getX(), getY());
        skeleton.getRootBone().setRotation(getRotation());
        skeleton.getRootBone().setScale(this.skelScale * getScaleX(), this.skelScale * getScaleY());
        animationState.apply(skeleton);
        skeleton.updateWorldTransform();
    }

    @Override
    protected void sizeChanged() {
        super.sizeChanged();
        recomputeSkeletonScale();
    }

    @Override
    protected void rotationChanged() {
        super.rotationChanged();
        skeleton.getRootBone().setRotation(getRotation());
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        animationState.update(delta);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        updateSkeletonTransform();
        skeleton.getColor().set(tmpColor.set(getColor()).mul(1f, 1f, 1f, parentAlpha));

        // Store current batch blend functions
        int srcFunc = batch.getBlendSrcFunc();
        int dstFunc = batch.getBlendDstFunc();
        int srcFuncAlpha = batch.getBlendSrcFuncAlpha();
        int dstFuncAlpha = batch.getBlendDstFuncAlpha();

        skeletonRenderer.draw((PolygonSpriteBatch)batch, skeleton, childrenNodes, parentAlpha);

        // Restore batch blending functions
        if (batch.getBlendSrcFunc() != srcFunc || batch.getBlendDstFunc() != dstFunc ||
                batch.getBlendSrcFuncAlpha() != srcFuncAlpha || batch.getBlendDstFuncAlpha() != dstFuncAlpha) {
            batch.setBlendFunctionSeparate(srcFunc, dstFunc, srcFuncAlpha, dstFuncAlpha);
        }
    }

    @Override
    protected void drawDebugBounds(ShapeRenderer shapes) {
        super.drawDebugBounds(shapes);
        if (!getDebug()) return;

        shapes.set(ShapeRenderer.ShapeType.Line);

        shapes.setColor(Color.ORANGE);
        if (scaling == Scaling.NONE) {
            shapes.rect(getX(), getY(), skelWidth, skelHeight);
        } else {
            shapes.rect(getX(), getY(), skelWidth * skelScale, skelHeight * skelScale);
        }

        shapes.setColor(Color.PURPLE);
        for (BBAttachNode node : bbAttachNodes) {
            BBAttachArea.BoundingBox bb = node.bbAttachArea.getBB();
            shapes.rect(bb.getX0(), bb.getY0(), bb.getWidth(), bb.getHeight());
        }
    }

    @Override
    protected void drawDebugChildren(ShapeRenderer shapes) {
        for (int i = 0, n = childrenNodes.size; i < n; i++) {
            ChildContainer node = childrenNodes.getValueAt(i);
            node.drawDebug(shapes);
        }
    }

    private void recomputeSkeletonScale() {
        switch (scaling) {
            case FIT_X:
                if (skelWidth > 0) {
                    skelScale = getWidth() / skelWidth;
                }
                break;
            case FIT_Y:
                if (skelHeight > 0) {
                    skelScale = getHeight() / skelHeight;
                }
                break;
            case FIT:
                if (skelHeight > 0 && skelWidth > 0) {
                    float wFactor = getWidth() / skelWidth;
                    float hFactor = getHeight() / skelHeight;
                    skelScale = Math.min(wFactor, hFactor);
                }
                break;
            case FILL:
                if (skelHeight > 0 && skelWidth > 0) {
                    float wFactor = getWidth() / skelWidth;
                    float hFactor = getHeight() / skelHeight;
                    skelScale = Math.max(wFactor, hFactor);
                }
                break;
        }
    }

    public void registerBBAttach(String slotName, String attachName, BBAttachHandler handler) {
        bbAttachNodes.add(new BBAttachNode(attachName, slotName, handler));
    }

    public void setSizeFromBB(String attachName, String slotName) {
        BBAttachArea attachArea = new BBAttachArea(attachName, slotName, skeleton);
        setSizeFromBB(attachArea);
    }

    public void setSizeFromBB(BBAttachArea attachArea) {
        skeleton.updateWorldTransform();
        BBAttachArea.BoundingBox bb = attachArea.getBB();
        skelWidth = bb.getWidth();
        skelHeight = bb.getHeight();
        setSize(bb.getWidth(), bb.getHeight());

        setOrigin(Align.center);
    }

    public Vector2 getBoneLocalPosition(String boneName) {
        Bone bone = skeleton.findBone(boneName);
        return tmpVec2.set(bone.getWorldX(), bone.getWorldY()).sub(skeleton.getX(), skeleton.getY());
    }

    public Vector2 getBoneStagePosition(String boneName) {
        Vector2 localPosition = getBoneLocalPosition(boneName);
        return localToStageCoordinates(localPosition);
    }

    private class BBAttachInputListener extends InputListener {
        /** Focused node */
        private BBAttachNode node;

        @Override
        public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            // Skeleton position is not zero, so we have to apply adjustments to touch coordinates
            x += skeleton.getX();
            y += skeleton.getY();
            for (BBAttachNode node : bbAttachNodes) {
                if (node.handler.isEnabled() && node.bbAttachArea.contains(x, y)) {
                    this.node = node;
                    node.focused = true;
                    node.handler.onTouchDown(x, y);
                    return true;
                }
            }
            return super.touchDown(event, x, y, pointer, button);
        }

        @Override
        public void touchDragged(InputEvent event, float x, float y, int pointer) {
            // Skeleton position is not zero, so we have to apply adjustments to touch coordinates
            x += skeleton.getX();
            y += skeleton.getY();
            boolean contains = node.bbAttachArea.contains(x, y);
            if (node.focused != contains) {
                node.focused = contains;

                if (contains) {
                    node.handler.onEnter(x, y);
                } else {
                    node.handler.onExit(x, y);
                }
            }
        }

        @Override
        public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            boolean over = isOver(event.getListenerActor(), x, y);

            // Skeleton position is not zero, so we have to apply adjustments to touch coordinates
            x += skeleton.getX();
            y += skeleton.getY();

            node.handler.onTouchUp(x, y, over);
            node.focused = false;
            node = null;
        }

        /** Returns true if the specified position is over the specified actor or within the tap square. */
        public boolean isOver (Actor actor, float x, float y) {
            Actor hit = actor.hit(x, y, true);
            return hit != null;
        }
    }

    public enum Scaling {
        NONE,
        FIT_X,
        FIT_Y,
        FIT,
        FILL,
    }

    private class BBAttachNode {
        final BBAttachArea bbAttachArea;
        final BBAttachHandler handler;

        boolean focused;

        BBAttachNode(String attachName, String slotName, BBAttachHandler handler) {
            bbAttachArea = new BBAttachArea(attachName, slotName, skeleton);
            this.handler = handler;
        }
    }

    //FIXME There is a problem with child position when some actor on stage has debug enabled.
    static class ChildContainer extends Group {
        private static final Matrix3 tmpMatrix3 = new Matrix3();
        private static final Matrix4 tmpMatrix4 = new Matrix4();
        private static final Affine2 tmpAffine2 = new Affine2();
        private static final Vector2 tmpVec2 = new Vector2();

        private static final Field groupTransformField;
        static {
            try {
                groupTransformField = ClassReflection.getDeclaredField(Group.class, "worldTransform");
                groupTransformField.setAccessible(true);
            } catch (ReflectionException e) {
                throw new RuntimeException(e);
            }
        }

        private final Actor actor;
        private final Slot slot;
        private final BoundingBoxAttachment attach;
        private final Affine2 worldTransform;
        private final Matrix4 computedTransform = new Matrix4();
        private final float baseX, baseY;

        public ChildContainer(Actor actor, Slot slot, BoundingBoxAttachment attach) {
            this.actor = actor;
            this.slot = slot;
            this.attach = attach;

            Rectangle bounds = SpineUtils.computeBBLocal(attach);
            this.baseX = bounds.x;
            this.baseY = bounds.y;

            this.addActor(actor);
            this.setBounds(0f, 0f, bounds.width, bounds.height);
            actor.setBounds(0f, 0f, bounds.width, bounds.height);

            this.setTouchable(Touchable.childrenOnly);

            // Obtain transform field reference
            try {
                worldTransform = Reflection.getFieldValue(groupTransformField, this, Affine2.class);
            } catch (ReflectionException e) {
                throw new RuntimeException(e);
            }
        }

        public Actor getActor() {
            return actor;
        }

        public Slot getSlot() {
            return slot;
        }

        public BoundingBoxAttachment getAttach() {
            return attach;
        }

        @Override
        protected Matrix4 computeTransform() {
            Bone bone = slot.getBone();
            bone.getWorldTransform(tmpMatrix3);
            Matrix4 boneTransform = tmpMatrix4;
            boneTransform.set(tmpMatrix3);

            Affine2 worldTransform = this.worldTransform.idt();
//            float originX = getOriginX(), originY = getOriginY();
//            worldTransform.setToTrnRotScl(getX() + originX, getY() + originY, getRotation(), getScaleX(), getScaleY());
//            if (originX != 0 || originY != 0) worldTransform.translate(-originX, -originY);

            // Find the first parent that transforms.
            Group parentGroup = getParent();
            while (parentGroup != null) {
                if (parentGroup.isTransform()) break;
                parentGroup = parentGroup.getParent();
            }
            if (parentGroup != null) {
                try {
                    Affine2 parentTransform = Reflection.getFieldValue(groupTransformField, parentGroup, Affine2.class);
                    worldTransform.preMul(parentTransform);
                } catch (ReflectionException e) {
                    throw new RuntimeException(e);
                }
            }

            worldTransform.mul(tmpAffine2.set(boneTransform));
            worldTransform.translate(baseX, baseY);

            computedTransform.set(worldTransform);
            return computedTransform;
        }

        @Override
        public Actor hit(float x, float y, boolean touchable) {
            if (touchable && getTouchable() == Touchable.disabled) return null;
            if (!isVisibleInternal()) return null;

            if (actor.isVisible()) {
                Actor hit = actor.hit(x, y, touchable);
                if (hit != null) return hit;
            }

            if (touchable && this.getTouchable() != Touchable.enabled) return null;
            return x >= 0 && x < getWidth() && y >= 0 && y < getHeight() ? this : null;
        }

        @Override
        public Vector2 parentToLocalCoordinates(Vector2 coords) {
            Bone bone = slot.getBone();
            Skeleton skeleton = bone.getSkeleton();
            bone.updateWorldTransform();

            Matrix3 boneTransform = bone.getWorldTransform(tmpMatrix3);

            coords.add(skeleton.getX(), skeleton.getY())
                    .mul(MatrixUtils.inv(boneTransform))
                    .sub(baseX, baseY);

            return coords;
        }

        @Override
        public Vector2 localToParentCoordinates(Vector2 coords) {
            Bone bone = slot.getBone();
            Skeleton skeleton = bone.getSkeleton();
            bone.updateWorldTransform();

            Matrix3 boneTransform = bone.getWorldTransform(tmpMatrix3);

            coords.sub(skeleton.getX(), skeleton.getY())
                    .mul(boneTransform)
                    .add(baseX, baseY);

            return coords;
        }

        @Override
        public void draw(Batch batch, float parentAlpha) {
            // #drawInternal() will be called directly from SkeletonGroupRenderer
        }

        @Override
        public void drawDebug(ShapeRenderer shapes) {
            if (!isVisibleInternal() || !actor.isVisible()) return;
            if (!actor.getDebug() && !(actor instanceof Group)) return;

            computeTransform();
            applyTransform(shapes, computedTransform);
            actor.drawDebug(shapes);
            resetTransform(shapes);
            shapes.flush();
        }

        void drawInternal(Batch batch, float parentAlpha) {
            if (!isVisibleInternal() || !actor.isVisible()) return;

            computeTransform();
            applyTransform(batch, computedTransform);
            Color slotColor = slot.getColor();
            actor.setColor(slotColor);
            actor.draw(batch, getColor().a * parentAlpha);
            resetTransform(batch);
        }

        boolean isVisibleInternal() {
            return super.isVisible() && slot.getAttachment() == attach;
        }
    }
}
