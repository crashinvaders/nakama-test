package com.crashinvaders.common.spine.scene2d;

/** Base class for any visual behavior implementation */
public abstract class VisualBBHandler implements BBAttachHandler {

    private boolean focused = false;
    private boolean pressed = false;
    private boolean enabled = true;

    @Override
    public void onTouchDown(float x, float y) {
        focused = true;
        pressed = true;
        visualizePress();
    }

    @Override
    public void onEnter(float x, float y) {
        if (!focused) return;
        pressed = true;
        visualizePress();
    }

    @Override
    public void onExit(float x, float y) {
        if (!focused) return;
        pressed = false;
        visualizeRelease();
    }

    @Override
    public void onTouchUp(float x, float y, boolean over) {
        if (pressed) {
            visualizeRelease();
        }

        if (this.pressed && over) {
            onClick(x, y);
        }

        focused = false;
        this.pressed = false;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isFocused() {
        return focused;
    }

    public boolean isPressed() {
        return pressed;
    }

    protected abstract void onClick(float x, float y);

    protected void visualizePress() { }

    protected void visualizeRelease() { }
}
