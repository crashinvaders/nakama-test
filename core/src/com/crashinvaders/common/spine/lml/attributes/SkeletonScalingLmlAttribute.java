
package com.crashinvaders.common.spine.lml.attributes;

import com.crashinvaders.common.spine.scene2d.SkeletonGroup;
import com.github.czyzby.lml.parser.LmlParser;
import com.github.czyzby.lml.parser.tag.LmlAttribute;
import com.github.czyzby.lml.parser.tag.LmlTag;

public class SkeletonScalingLmlAttribute implements LmlAttribute<SkeletonGroup> {
    @Override
    public Class<SkeletonGroup> getHandledType() {
        return SkeletonGroup.class;
    }

    @Override
    public void process(final LmlParser parser, final LmlTag tag, final SkeletonGroup actor, final String rawAttributeData) {
        String scalingString = parser.parseString(rawAttributeData, actor);
        SkeletonGroup.Scaling scaling = SkeletonGroup.Scaling.NONE;
        switch (scalingString) {
            case "fill": scaling = SkeletonGroup.Scaling.FILL; break;
            case "fit": scaling = SkeletonGroup.Scaling.FIT; break;
            case "fitx": scaling = SkeletonGroup.Scaling.FIT_X; break;
            case "fity": scaling = SkeletonGroup.Scaling.FIT_Y; break;
        }
        actor.setScaling(scaling);
    }
}
