
package com.crashinvaders.common.spine.lml.attributes;

import com.crashinvaders.common.spine.scene2d.SkeletonGroup;
import com.github.czyzby.lml.parser.LmlParser;
import com.github.czyzby.lml.parser.tag.LmlAttribute;
import com.github.czyzby.lml.parser.tag.LmlTag;

public class SkeletonScaleLmlAttribute implements LmlAttribute<SkeletonGroup> {
    @Override
    public Class<SkeletonGroup> getHandledType() {
        return SkeletonGroup.class;
    }

    @Override
    public void process(final LmlParser parser, final LmlTag tag, final SkeletonGroup actor, final String rawAttributeData) {
        float scale = parser.parseFloat(rawAttributeData, actor);
        actor.setSkelScale(scale);
    }
}
