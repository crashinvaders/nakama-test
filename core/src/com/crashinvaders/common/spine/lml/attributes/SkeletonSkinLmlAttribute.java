
package com.crashinvaders.common.spine.lml.attributes;

import com.crashinvaders.common.spine.scene2d.SkeletonGroup;
import com.esotericsoftware.spine.Skeleton;
import com.esotericsoftware.spine.Skin;
import com.github.czyzby.lml.parser.LmlParser;
import com.github.czyzby.lml.parser.tag.LmlAttribute;
import com.github.czyzby.lml.parser.tag.LmlTag;

public class SkeletonSkinLmlAttribute implements LmlAttribute<SkeletonGroup> {
    @Override
    public Class<SkeletonGroup> getHandledType() {
        return SkeletonGroup.class;
    }

    @Override
    public void process(final LmlParser parser, final LmlTag tag, final SkeletonGroup actor, final String rawAttributeData) {
        String skinName = parser.parseString(rawAttributeData, actor);
        Skeleton skeleton = actor.getSkeleton();
        Skin skin = skeleton.getData().findSkin(skinName);
        if (skin == null) {
            parser.throwError("Cannot find skin: \"" + skinName + "\" for \"" + skeleton.getData().getName() + "\" skeleton.");
        }
        skeleton.setSkin(skinName);
    }
}
