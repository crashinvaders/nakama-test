package com.crashinvaders.common.spine.lml.tags.macro;

import com.crashinvaders.common.spine.scene2d.ClickBBAttachHandler;
import com.crashinvaders.common.spine.scene2d.SkeletonGroup;
import com.github.czyzby.lml.parser.LmlParser;
import com.github.czyzby.lml.parser.action.ActorConsumer;
import com.github.czyzby.lml.parser.impl.tag.AbstractMacroLmlTag;
import com.github.czyzby.lml.parser.tag.LmlTag;
import com.github.czyzby.lml.parser.tag.LmlTagProvider;

/**
 * Assigns {@link ActorConsumer} for a {@link SkeletonGroup}'s bounding box click.
 * {@link #ATTR_ATTACH} is optional and will use value from {@link #ATTR_SLOT} if not provided.
 */
public class SkeletonBBClickHandlerLmlMacroTag extends AbstractMacroLmlTag {
    protected static final String ATTR_SLOT = "slot";
    protected static final String ATTR_ATTACH = "attach";
    protected static final String ATTR_CLICK_HANDLER = "onclick";

    public SkeletonBBClickHandlerLmlMacroTag(final LmlParser parser, final LmlTag parentTag, final StringBuilder rawTagData) {
        super(parser, parentTag, rawTagData);
    }

    @Override
    public String[] getExpectedAttributes() {
        return new String[] { ATTR_SLOT, ATTR_ATTACH, ATTR_CLICK_HANDLER};
    }

    @Override
    public void closeTag() {
        super.closeTag();

        LmlParser parser = getParser();

        if (!(getParent().getActor() instanceof SkeletonGroup)) {
            parser.throwError("Parent is not a SkeletonGroup");
        }
        final SkeletonGroup skelGroup = (SkeletonGroup) getParent().getActor();

        final String slotName = parser.parseString(getAttribute(ATTR_SLOT), skelGroup);
        final String attachName;
        if (hasAttribute(ATTR_ATTACH)) {
            attachName = parser.parseString(getAttribute(ATTR_ATTACH), skelGroup);
        } else {
            attachName = slotName;
        }

        final ActorConsumer<?, Object> actionConsumer = parser.parseAction(getAttribute(ATTR_CLICK_HANDLER));

        skelGroup.registerBBAttach(slotName, attachName, new ClickBBAttachHandler() {
            @Override
            protected void onClick(float x, float y) {
                actionConsumer.consume(skelGroup);
            }
        });
    }

    @Override
    public void handleDataBetweenTags(CharSequence rawData) {

    }

    public static class TagProvider implements LmlTagProvider {
        @Override
        public LmlTag create(LmlParser parser, LmlTag parentTag, StringBuilder rawTagData) {
            return new SkeletonBBClickHandlerLmlMacroTag(parser, parentTag, rawTagData);
        }
    }
}
