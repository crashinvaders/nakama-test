package com.crashinvaders.common.spine.lml.tags.macro;

import com.crashinvaders.common.spine.scene2d.SkeletonGroup;
import com.esotericsoftware.spine.AnimationStateData;
import com.github.czyzby.lml.parser.LmlParser;
import com.github.czyzby.lml.parser.impl.tag.AbstractMacroLmlTag;
import com.github.czyzby.lml.parser.tag.LmlTag;
import com.github.czyzby.lml.parser.tag.LmlTagProvider;

public class SkeletonAnimationMixLmlMacroTag extends AbstractMacroLmlTag {
    protected static final String ATTR_DURATION = "duration";
    protected static final String ATTR_ANIMATIONS = "animations";

    public SkeletonAnimationMixLmlMacroTag(final LmlParser parser, final LmlTag parentTag, final StringBuilder rawTagData) {
        super(parser, parentTag, rawTagData);
    }

    @Override
    public String[] getExpectedAttributes() {
        return new String[] {ATTR_DURATION, ATTR_ANIMATIONS};
    }

    @Override
    public void closeTag() {
        super.closeTag();

        LmlParser parser = getParser();

        if (!(getParent().getActor() instanceof SkeletonGroup)) {
            parser.throwError("Parent is not a SkeletonGroup");
        }
        final SkeletonGroup skelGroup = (SkeletonGroup) getParent().getActor();

        final float duration = parser.parseFloat(getAttribute(ATTR_DURATION), skelGroup);
        final String[] animations = parser.parseArray(getAttribute(ATTR_ANIMATIONS), skelGroup);

        if (animations.length < 2) {
            parser.throwError("Should be at least 2 animations to setup mix between them.");
        }

        AnimationStateData animData = skelGroup.getAnimState().getData();
        for (int i = 0; i < animations.length-1; i++) {
            String animation0 = animations[i];
            String animation1;
            for (int j = i+1; j < animations.length; j++) {
                animation1 = animations[j];
                animData.setMix(animation0, animation1, duration);
                animData.setMix(animation1, animation0, duration);
            }
        }
    }

    @Override
    public void handleDataBetweenTags(CharSequence rawData) {

    }

    public static class TagProvider implements LmlTagProvider {
        @Override
        public LmlTag create(LmlParser parser, LmlTag parentTag, StringBuilder rawTagData) {
            return new SkeletonAnimationMixLmlMacroTag(parser, parentTag, rawTagData);
        }
    }
}
