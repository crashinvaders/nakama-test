package com.crashinvaders.common.spine.lml.tags;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.crashinvaders.common.spine.scene2d.SkeletonGroup;
import com.esotericsoftware.spine.SkeletonData;
import com.github.czyzby.lml.parser.LmlParser;
import com.github.czyzby.lml.parser.impl.tag.AbstractActorLmlTag;
import com.github.czyzby.lml.parser.tag.LmlActorBuilder;
import com.github.czyzby.lml.parser.tag.LmlTag;
import com.github.czyzby.lml.parser.tag.LmlTagProvider;

public class SkeletonGroupLmlTag extends AbstractActorLmlTag {

    public SkeletonGroupLmlTag(final LmlParser parser, final LmlTag parentTag, final StringBuilder rawTagData) {
        super(parser, parentTag, rawTagData);
    }

    @Override
    protected Actor getNewInstanceOfActor(final LmlActorBuilder builder) {
        String styleName = builder.getStyleName();
        SkeletonGroup skeletonGroup = new SkeletonGroup(getSkin(builder).get(styleName, SkeletonData.class));
        return skeletonGroup;
    }

    @Override
    protected void handlePlainTextLine(String plainTextLine) {
        getParser().throwErrorIfStrict("Received plain text data between tags that cannot be parsed: " + plainTextLine);
    }

    @Override
    protected void handleValidChild(LmlTag childTag) {
        String slot = childTag.getAttribute("slot");
        if (slot == null) {
            getParser().throwErrorIfStrict("slot cannot be null");
        }

        String attachment = childTag.getAttribute("attachment");
        if (attachment == null) {
            attachment = slot;
        }

        SkeletonGroup skeletonGroup = (SkeletonGroup) getActor();
        skeletonGroup.addActor(childTag.getActor(), slot, attachment);
    }

    public static class TagProvider implements LmlTagProvider {
        @Override
        public LmlTag create(final LmlParser parser, final LmlTag parentTag, final StringBuilder rawTagData) {
            return new SkeletonGroupLmlTag(parser, parentTag, rawTagData);
        }
    }
}
