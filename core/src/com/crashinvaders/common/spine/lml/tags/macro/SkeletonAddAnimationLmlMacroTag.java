package com.crashinvaders.common.spine.lml.tags.macro;

import com.crashinvaders.common.spine.scene2d.SkeletonGroup;
import com.github.czyzby.lml.parser.LmlParser;
import com.github.czyzby.lml.parser.impl.tag.AbstractMacroLmlTag;
import com.github.czyzby.lml.parser.tag.LmlTag;
import com.github.czyzby.lml.parser.tag.LmlTagProvider;

public class SkeletonAddAnimationLmlMacroTag extends AbstractMacroLmlTag {
    protected static final String ATTR_NAME = "name";
    protected static final String ATTR_TRACK = "track";
    protected static final String ATTR_LOOP = "loop";
    protected static final String ATTR_DELAY = "delay";

    public SkeletonAddAnimationLmlMacroTag(final LmlParser parser, final LmlTag parentTag, final StringBuilder rawTagData) {
        super(parser, parentTag, rawTagData);
    }

    @Override
    public String[] getExpectedAttributes() {
        return new String[] {ATTR_NAME, ATTR_TRACK, ATTR_LOOP, ATTR_DELAY};
    }

    @Override
    public void closeTag() {
        super.closeTag();

        LmlParser parser = getParser();

        if (!(getParent().getActor() instanceof SkeletonGroup)) {
            parser.throwError("Parent is not a SkeletonGroup");
        }
        final SkeletonGroup skelGroup = (SkeletonGroup) getParent().getActor();

        final String animationName = parser.parseString(getAttribute(ATTR_NAME), skelGroup);
        final int track;
        if (hasAttribute(ATTR_TRACK)) {
            track = parser.parseInt(getAttribute(ATTR_TRACK), skelGroup);
        } else {
            track = 0;
        }
        final boolean loop;
        if (hasAttribute(ATTR_LOOP)) {
            loop = parser.parseBoolean(getAttribute(ATTR_LOOP), skelGroup);
        } else {
            loop = false;
        }
        final float delay;
        if (hasAttribute(ATTR_DELAY)) {
            delay = parser.parseFloat(getAttribute(ATTR_DELAY), skelGroup);
        } else {
            delay = 0f;
        }

        skelGroup.getAnimState().addAnimation(track, animationName, loop, delay);
    }

    @Override
    public void handleDataBetweenTags(CharSequence rawData) {

    }

    public static class TagProvider implements LmlTagProvider {
        @Override
        public LmlTag create(LmlParser parser, LmlTag parentTag, StringBuilder rawTagData) {
            return new SkeletonAddAnimationLmlMacroTag(parser, parentTag, rawTagData);
        }
    }
}
