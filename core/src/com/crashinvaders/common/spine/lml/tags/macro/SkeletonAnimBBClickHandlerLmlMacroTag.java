package com.crashinvaders.common.spine.lml.tags.macro;

import com.crashinvaders.common.spine.scene2d.AnimPressBBHandler;
import com.crashinvaders.common.spine.scene2d.SkeletonGroup;
import com.github.czyzby.lml.parser.LmlParser;
import com.github.czyzby.lml.parser.action.ActorConsumer;
import com.github.czyzby.lml.parser.impl.tag.AbstractMacroLmlTag;
import com.github.czyzby.lml.parser.tag.LmlTag;
import com.github.czyzby.lml.parser.tag.LmlTagProvider;

import static com.crashinvaders.common.spine.lml.tags.macro.SkeletonBBClickHandlerLmlMacroTag.*;

public class SkeletonAnimBBClickHandlerLmlMacroTag extends AbstractMacroLmlTag {
    protected static final String ATTR_ANIM_TRACK = "animtrack";
    protected static final String ATTR_ANIM_TOUCH_DOWN = "animtouchdown";
    protected static final String ATTR_ANIM_TOUCH_UP = "animtouchup";

    private static int globalNextTrackIdx = 100000;

    public SkeletonAnimBBClickHandlerLmlMacroTag(LmlParser parser, LmlTag parentTag, StringBuilder rawTagData) {
        super(parser, parentTag, rawTagData);
    }

    @Override
    public String[] getExpectedAttributes() {
        return new String[] { ATTR_SLOT, ATTR_ATTACH, ATTR_CLICK_HANDLER, ATTR_ANIM_TRACK, ATTR_ANIM_TOUCH_DOWN, ATTR_ANIM_TOUCH_UP };
    }

    @Override
    public void closeTag() {
        super.closeTag();

        LmlParser parser = getParser();

        if (!(getParent().getActor() instanceof SkeletonGroup)) {
            parser.throwError("Parent is not a SkeletonGroup");
        }
        final SkeletonGroup skelGroup = (SkeletonGroup) getParent().getActor();

        final String slotName = parser.parseString(getAttribute(ATTR_SLOT), skelGroup);
        final String attachName;
        if (hasAttribute(ATTR_ATTACH)) {
            attachName = parser.parseString(getAttribute(ATTR_ATTACH), skelGroup);
        } else {
            attachName = slotName;
        }

        final int animTrack;
        if (hasAttribute(ATTR_ANIM_TRACK)) {
            animTrack = parser.parseInt(getAttribute(ATTR_ANIM_TRACK), skelGroup);
        } else {
            animTrack = globalNextTrackIdx++;
        }
        final String animTouchDown = parser.parseString(getAttribute(ATTR_ANIM_TOUCH_DOWN), skelGroup);
        final String animTouchUp = parser.parseString(getAttribute(ATTR_ANIM_TOUCH_UP), skelGroup);

        final ActorConsumer<?, Object> actionConsumer = parser.parseAction(getAttribute(ATTR_CLICK_HANDLER));

        skelGroup.registerBBAttach(slotName, attachName,
                new AnimPressBBHandler(skelGroup.getAnimState(), animTrack, animTouchDown, animTouchUp) {
                    @Override
                    protected void onClick(float x, float y) {
                        actionConsumer.consume(skelGroup);
                    }
                });
    }

    @Override
    public void handleDataBetweenTags(CharSequence rawData) {

    }

    public static class TagProvider implements LmlTagProvider {
        @Override
        public LmlTag create(LmlParser parser, LmlTag parentTag, StringBuilder rawTagData) {
            return new SkeletonAnimBBClickHandlerLmlMacroTag(parser, parentTag, rawTagData);
        }
    }
}
