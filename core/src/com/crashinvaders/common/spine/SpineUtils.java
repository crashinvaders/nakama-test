package com.crashinvaders.common.spine;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.esotericsoftware.spine.AnimationState;
import com.esotericsoftware.spine.Skeleton;
import com.esotericsoftware.spine.Skin;
import com.esotericsoftware.spine.Slot;
import com.esotericsoftware.spine.attachments.RegionAttachment;
import com.esotericsoftware.spine.attachments.VertexAttachment;

public class SpineUtils {
    private static final String TAG = SpineUtils.class.getSimpleName();
    private static final Array<Skin> tmpSkins = new Array<>();
    private static final Rectangle tmpRect = new Rectangle();
    private static final float[] tmpVertices = new float[128 * 2];

    public static void updateTrackTime(AnimationState animState, int track, float time) {
        AnimationState.TrackEntry trackEntry = animState.getCurrent(track);
        trackEntry.setTrackTime(trackEntry.getTrackTime() + time);
    }

    public static void randomizeSkin(Skeleton skeleton) {
        tmpSkins.addAll(skeleton.getData().getSkins());
        for (int i = 0; i < tmpSkins.size; i++) {
            if (tmpSkins.get(i).getName().equals("default")) {
                tmpSkins.removeIndex(i);
                break;
            }
        }
        skeleton.setSkin(tmpSkins.random());
        tmpSkins.clear();
    }

    public static RegionAttachment createRegionAttach(String attachName, TextureRegion region) {
        RegionAttachment attachment = new RegionAttachment(attachName);
        attachment.setRegion(region);
        attachment.setWidth(region.getRegionWidth());
        attachment.setHeight(region.getRegionHeight());
        attachment.updateOffset();
        return attachment;
    }

    /** @return Rectangle instance will be reused. */
    public static Rectangle computeBBLocal(VertexAttachment attach) {
        float[] vertices = attach.getVertices();
        int size = attach.getWorldVerticesLength();

        float maxX = -Float.MAX_VALUE;
        float minX = Float.MAX_VALUE;
        float maxY = -Float.MAX_VALUE;
        float minY = Float.MAX_VALUE;

        int verticesAmount = size / 2;
        for (int i = 0; i < verticesAmount; i++) {
            float xV = vertices[i*2];
            float yV = vertices[i*2+1];
            if (xV > maxX) maxX = xV;
            if (xV < minX) minX = xV;
            if (yV > maxY) maxY = yV;
            if (yV < minY) minY = yV;
        }

        return tmpRect.set(minX, minY, maxX - minX, maxY - minY);
    }

    /** @return Rectangle instance will be reused. */
    public static Rectangle computeBBWorld(Slot slot, VertexAttachment attach) {
        float[] vertices = tmpVertices;
        int size = attach.getWorldVerticesLength();

        attach.computeWorldVertices(slot, 0, attach.getWorldVerticesLength(), vertices, 0, 2);

        float maxX = -Float.MAX_VALUE;
        float minX = Float.MAX_VALUE;
        float maxY = -Float.MAX_VALUE;
        float minY = Float.MAX_VALUE;

        int verticesAmount = size / 2;
        for (int i = 0; i < verticesAmount; i++) {
            float xV = vertices[i*2];
            float yV = vertices[i*2+1];
            if (xV > maxX) maxX = xV;
            if (xV < minX) minX = xV;
            if (yV > maxY) maxY = yV;
            if (yV < minY) minY = yV;
        }

        return tmpRect.set(minX, minY, maxX - minX, maxY - minY);
    }

    /** Does exactly the same as {@link Skeleton#setAttachment(String, String)}, but doesn't throw any exceptions. */
    public static void setAttachSafe(Skeleton skeleton, String slotName, String attachmentName) {
        try {
            skeleton.setAttachment(slotName, attachmentName);
        } catch (IllegalArgumentException e) {
            Gdx.app.error(TAG, "Can't set attachment \"" + attachmentName + "\" for the slot \"" + slotName + "\"", e);
        }
    }
}
