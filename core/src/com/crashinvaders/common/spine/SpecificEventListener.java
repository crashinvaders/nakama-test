package com.crashinvaders.common.spine;

import com.badlogic.gdx.Gdx;
import com.esotericsoftware.spine.AnimationState;
import com.esotericsoftware.spine.AnimationState.TrackEntry;
import com.esotericsoftware.spine.Event;

public abstract class SpecificEventListener implements AnimationState.AnimationStateListener {
    protected final String eventName;
    private boolean scheduledForRemoval;

    public SpecificEventListener(String eventName) {
        this.eventName = eventName;
    }

    @Override
    public void start(TrackEntry entry) { }

    @Override
    public void interrupt(TrackEntry entry) { }

    @Override
    public void end(TrackEntry entry) { }

    @Override
    public void dispose(TrackEntry entry) { }

    @Override
    public void complete(TrackEntry entry) { }

    @Override
    public final void event(TrackEntry entry, Event event) {
        if (scheduledForRemoval) return;
        if (event.getData().getName().equals(eventName)) {
            onEvent(entry, event);
        }
    }

    protected abstract void onEvent(TrackEntry entry, Event event);

    public void postRemove(final AnimationState animState) {
        scheduledForRemoval = true;

        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                animState.removeListener(SpecificEventListener.this);
                onPostRemove();
            }
        });
    }

    protected void onPostRemove() { }
}
