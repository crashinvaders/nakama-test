package com.crashinvaders.common.spine;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.utils.Pool;
import com.esotericsoftware.spine.Skeleton;
import com.esotericsoftware.spine.SkeletonData;

import java.util.HashMap;
import java.util.Map;

public class SpineSkeletonCache {

    private final AssetManager assets;

    /** Holds references between skeleton data and it's pool */
    private final Map<SkeletonData, SkeletonPool> pools;

    int counter = 0;

    public SpineSkeletonCache(AssetManager assets) {
        this.assets = assets;
        this.pools = new HashMap<>();
    }

    public Skeleton obtain(String skeletonPath) {
//        System.out.println("ParticleEffectCache: Obtained " + ++counter);

        SkeletonPool pool = findPool(skeletonPath);
        return pool.obtain();
    }

    public void free(Skeleton skeleton) {
//        System.out.println("ParticleEffectCache: Freed " + --counter);

        SkeletonData data = skeleton.getData();
        SkeletonPool pool = findPool(data);
        pool.free(skeleton);

        skeleton.setToSetupPose();
    }

    private SkeletonPool findPool(String skeletonPath) {
        SkeletonData data = getDataFromAssets(skeletonPath);
        return findPool(data);
    }

    private SkeletonPool findPool(SkeletonData data) {
        SkeletonPool pool = pools.get(data);
        if (pool == null) {
            pool = createPool(data);
        }
        return pool;
    }

    private SkeletonPool createPool(SkeletonData data) {
        if (data == null) {
            throw new NullPointerException();
        }

        SkeletonPool pool = new SkeletonPool(data);
        pools.put(data, pool);
        return pool;
    }

    private SkeletonData getDataFromAssets(String skeletonPath) {
        SkeletonData data = assets.get(skeletonPath);
        if (data == null) {
            throw new IllegalArgumentException("Can't find skeleton data by path: " + skeletonPath);
        }
        return data;
    }

    private static class SkeletonPool extends Pool<Skeleton> {
        private final SkeletonData data;

        public SkeletonPool(SkeletonData data) {
            super(32);
            this.data = data;
        }

        @Override
        protected Skeleton newObject() {
            return new Skeleton(data);
        }
    }
}
