package com.crashinvaders.common.spine;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.SynchronousAssetLoader;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Array;
import com.crashinvaders.common.spine.attachments.MultiAtlasAttachmentLoader;
import com.esotericsoftware.spine.SkeletonData;
import com.esotericsoftware.spine.SkeletonJson;

/* MaryMur 10.02.14 15:13 */
public class SkeletonDataLoader extends SynchronousAssetLoader<SkeletonData, SkeletonDataLoader.Params> {

    public SkeletonDataLoader(FileHandleResolver resolver) {
        super(resolver);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Array<AssetDescriptor> getDependencies(String fileName, FileHandle file, Params parameter) {
        if (parameter == null || parameter.atlasPath == null) {
            throw new IllegalArgumentException("Atlas parameter can't be null");
        }

        Array<AssetDescriptor> dependencies = new Array<AssetDescriptor>();

        dependencies.add(new AssetDescriptor(parameter.atlasPath, TextureAtlas.class));

        for (int i = 0; i < parameter.altAtlases.size; i++) {
            Params.AltAtlas altAtlas = parameter.altAtlases.get(i);
            dependencies.add(new AssetDescriptor(altAtlas.atlasPath, TextureAtlas.class));
        }
        for (int i = 0; i < parameter.altTextures.size; i++) {
            Params.AltTexture altTexture = parameter.altTextures.get(i);
            dependencies.add(new AssetDescriptor(altTexture.texturePath, Texture.class));
        }

        return dependencies;
    }

    @Override
    public SkeletonData load(AssetManager assets, String fileName, FileHandle file, Params parameter) {
        if (parameter == null || parameter.atlasPath == null) {
            throw new IllegalArgumentException("Atlas parameter can't be null");
        }

        TextureAtlas mainAtlas = assets.get(parameter.atlasPath, TextureAtlas.class);
        MultiAtlasAttachmentLoader attachmentLoader = new MultiAtlasAttachmentLoader(mainAtlas);

        if (parameter.emptyRegionName != null) {
            TextureAtlas.AtlasRegion emptyRegion = mainAtlas.findRegion(parameter.emptyRegionName);
            attachmentLoader.setEmptyRegion(emptyRegion);
        }

        for (int i = 0; i < parameter.altAtlases.size; i++) {
            Params.AltAtlas altAtlas = parameter.altAtlases.get(i);
            attachmentLoader.addAltAtlas(
                    altAtlas.regionPrefix,
                    assets.get(altAtlas.atlasPath, TextureAtlas.class));
        }

        for (int i = 0; i < parameter.altTextures.size; i++) {
            Params.AltTexture altTexture = parameter.altTextures.get(i);
            Texture texture = assets.get(altTexture.texturePath, Texture.class);

            // Copy texture settings from the main atlas's texture.
            Texture mainAtlasTexture = mainAtlas.getTextures().first();
            texture.setFilter(mainAtlasTexture.getMinFilter(), mainAtlasTexture.getMagFilter());

            attachmentLoader.addTexture(
                    altTexture.name,
                    texture);
        }

        SkeletonJson json = new SkeletonJson(attachmentLoader);
        json.setScale(parameter.baseScale);

        SkeletonData skeletonData = json.readSkeletonData(file);
//        skeletonData.setAtlasTextureHash(atlas.getTextures().first().hashCode());
        return skeletonData;
    }

    public static class Params extends AssetLoaderParameters<SkeletonData> {

        final Array<AltAtlas> altAtlases = new Array<>(true, 8);
        final Array<AltTexture> altTextures = new Array<>(true, 8);
        final String atlasPath;
        final float baseScale;

        /** Name of an empty region that will be used in case attachment is not found. */
        String emptyRegionName = null;

        public Params(String atlasPath) {
            this.atlasPath = atlasPath;
            this.baseScale = 1f;
        }

        public Params(String atlasPath, float baseScale) {
            this.atlasPath = atlasPath;
            this.baseScale = baseScale;
        }

        public Params setEmptyRegionName(String emptyRegionName) {
            this.emptyRegionName = emptyRegionName;
            return this;
        }

        public Params addAtlas(String regionPrefix, String atlasPath) {
            altAtlases.add(new AltAtlas(regionPrefix, atlasPath));
            return this;
        }

        public Params addTexture(String name, String texturePath) {
            altTextures.add(new AltTexture(name, texturePath));
            return this;
        }

        public Params addTexture(String texturePath) {
            int lastSlash = texturePath.lastIndexOf('/');
            int lastDot = texturePath.lastIndexOf('.');
            int begin = lastSlash != -1 ? lastSlash + 1 : 0;
            int end = lastDot != -1 ? lastDot : texturePath.length();
            String name = texturePath.substring(begin, end);
            return addTexture(name, texturePath);
        }

        public static class AltAtlas {
            final String regionPrefix;
            final String atlasPath;

            public AltAtlas(String regionPrefix, String atlasPath) {
                this.regionPrefix = regionPrefix;
                this.atlasPath = atlasPath;
            }
        }

        public static class AltTexture {
            final String name;
            final String texturePath;

            public AltTexture(String name, String texturePath) {
                this.name = name;
                this.texturePath = texturePath;
            }
        }
    }
}
