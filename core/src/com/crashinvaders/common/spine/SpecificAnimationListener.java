package com.crashinvaders.common.spine;

import com.badlogic.gdx.Gdx;
import com.esotericsoftware.spine.AnimationState;
import com.esotericsoftware.spine.AnimationState.TrackEntry;
import com.esotericsoftware.spine.Event;

public abstract class SpecificAnimationListener implements AnimationState.AnimationStateListener {
    protected final String animName;
    private boolean scheduledForRemoval;

    public SpecificAnimationListener(String animName) {
        this.animName = animName;
    }

    @Override
    public final void start(TrackEntry entry) {
        if (scheduledForRemoval) return;
        if (entry.getAnimation().getName().equals(animName)) {
            onStart(entry);
        }
    }

    @Override
    public final void interrupt(TrackEntry entry) {
        if (scheduledForRemoval) return;
        if (entry.getAnimation().getName().equals(animName)) {
            onInterrupt(entry);
        }
    }

    @Override
    public final void end(TrackEntry entry) {
        if (scheduledForRemoval) return;
        if (entry.getAnimation().getName().equals(animName)) {
            onEnd(entry);
        }
    }

    @Override
    public final void dispose(TrackEntry entry) {
        if (scheduledForRemoval) return;
        if (entry.getAnimation().getName().equals(animName)) {
            onDispose(entry);
        }
    }

    @Override
    public final void complete(TrackEntry entry) {
        if (scheduledForRemoval) return;
        if (entry.getAnimation().getName().equals(animName)) {
            onComplete(entry);
        }
    }

    @Override
    public final void event(TrackEntry entry, Event event) {
        if (scheduledForRemoval) return;
        if (entry.getAnimation().getName().equals(animName)) {
            onEvent(entry, event);
        }
    }

    protected void onStart(TrackEntry entry) { }
    protected void onInterrupt(TrackEntry entry) { }
    protected void onEnd(TrackEntry entry) { }
    protected void onDispose(TrackEntry entry) { }
    protected void onComplete(TrackEntry entry) { }
    protected void onEvent(TrackEntry entry, Event event) { }

    public void postRemove(final AnimationState animState) {
        scheduledForRemoval = true;

        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                animState.removeListener(SpecificAnimationListener.this);
                onPostRemove();
            }
        });
    }

    protected void onPostRemove() { }
}
