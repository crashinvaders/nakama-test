package com.crashinvaders.common.graphics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.utils.Array;

public class ParticleUtils {
    private static final String TAG = ParticleUtils.class.getSimpleName();

    public static void setEmitterColor(ParticleEffect effect, String emitterName, Color color) {
        ParticleEmitter emitter = effect.findEmitter(emitterName);
        if (emitter == null) {
            Gdx.app.error(TAG, "Can't find emitter: " + emitterName + " for " + effect);
            return;
        }

        ParticleEmitter.GradientColorValue tintValue = emitter.getTint();
        for (int i = 0; i < tintValue.getTimeline().length; i++) {
            float[] colors = tintValue.getColors();
            colors[i*3 + 0] = color.r;
            colors[i*3 + 1] = color.g;
            colors[i*3 + 2] = color.b;
        }
    }

    public static void rotateEffect(ParticleEffect effect, float degrees) {
        Array<ParticleEmitter> emitters = effect.getEmitters();
        for (int i = 0; i < emitters.size; i++) {
            ParticleEmitter emitter = emitters.get(i);
            ParticleEmitter.ScaledNumericValue angleValue = emitter.getAngle();
            angleValue.setLow(angleValue.getLowMin() + degrees, angleValue.getLowMax() + degrees);
            angleValue.setHigh(angleValue.getHighMin() + degrees, angleValue.getHighMax() + degrees);
        }
    }
}
