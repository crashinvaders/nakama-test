package com.crashinvaders.common.graphics;

import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.badlogic.gdx.utils.reflect.Field;
import com.badlogic.gdx.utils.reflect.ReflectionException;

import java.io.BufferedReader;
import java.io.IOException;

/** {@link ParticleEffect} extension that features:
 * <div> 1. Persistent scale property. Use {@link #setScale(float)} and {@link #getScale()} to work with scale properly. </div>
 * <div> 2. Supports alpha modulation drawing through {@link #drawWithAlphaModulation(Batch, float)}. </div>*/
@SuppressWarnings({"unused", "WeakerAccess"})
public class ParticleEffectX extends ParticleEffect {

    private float scale = 1f;

    public ParticleEffectX() {
    }

    public ParticleEffectX(ParticleEffect effect) {
        super(effect);
    }

    /** Allows to apply effect global alpha modulation.
     * May perform slower than regular {@link #draw(Batch)}, use with caution. */
    @SuppressWarnings({"LibGDXFlushInsideLoop"})
    public void drawWithAlphaModulation (Batch spriteBatch, float alphaModulation) {
        Array<ParticleEmitter> emitters = getEmitters();
        for (int i = 0, n = emitters.size; i < n; i++)
            ((ParticleEmitterX) emitters.get(i)).
                    drawWithAlphaModulation(spriteBatch, alphaModulation);
    }

    public void setScale(float scale) {
        if (MathUtils.isEqual(this.scale, scale)) return;

        float scaleFactor = scale/this.scale;
        this.scaleEffect(scaleFactor);
        this.scale = scale;
    }

    public void setRotation(float rotation) {
        Array<ParticleEmitter> emitters = getEmitters();
        for (int i = 0; i < emitters.size; i++) {
            ((ParticleEmitterX)emitters.get(i)).setRotation(rotation);
        }
    }

    public float getScale() {
        return scale;
    }

    public ParticleEffectX cpy() {
        return new ParticleEffectX(this);
    }

    @Override
    protected ParticleEmitter newEmitter(ParticleEmitter emitter) {
        return new ParticleEmitterX(emitter);
    }

    @Override
    protected ParticleEmitter newEmitter(BufferedReader reader) throws IOException {
        return new ParticleEmitterX(reader);
    }

    public static class ParticleEmitterX extends ParticleEmitter {

        private boolean[] active;
        private ScaledNumericValue originRotation = null;
        private ScaledNumericValue originAngle = null;

        public ParticleEmitterX(BufferedReader reader) throws IOException {
            super(reader);
        }

        public ParticleEmitterX(ParticleEmitter emitter) {
            super(emitter);
        }

        @Override
        public void setMaxParticleCount(int maxParticleCount) {
            super.setMaxParticleCount(maxParticleCount);
            // Extract "active" private field.
            try {
                Field fieldActive = ClassReflection.getDeclaredField(ParticleEmitter.class, "active");
                fieldActive.setAccessible(true);
                active = (boolean[]) fieldActive.get(this);
            } catch (ReflectionException e) {
                throw new RuntimeException(e);
            }
        }

        public void setRotation(float rotation) {
            if (originRotation == null) {
                originRotation = new ScaledNumericValue();
                originRotation.set(getRotation());
            }
            if (originAngle == null) {
                originAngle = new ScaledNumericValue();
                originAngle.set(getAngle());
            }
            getRotation().setLow(originRotation.getLowMin() + rotation, originRotation.getLowMax() + rotation);
            getRotation().setHigh(originRotation.getHighMin() + rotation, originRotation.getHighMax() + rotation);
            getAngle().setLow(originAngle.getLowMin() + rotation, originAngle.getLowMax() + rotation);
            getAngle().setHigh(originAngle.getHighMin() + rotation, originAngle.getHighMax() + rotation);
        }

        public void drawWithAlphaModulation(Batch batch, float alphaModulation) {
            boolean premultipliedAlpha = isPremultipliedAlpha();
            boolean additive = isAdditive();

            if (premultipliedAlpha) {
                batch.setBlendFunction(GL20.GL_ONE, GL20.GL_ONE_MINUS_SRC_ALPHA);
            } else if (additive) {
                batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE);
            } else {
                batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
            }
            Particle[] particles = getParticles();
            boolean[] active = this.active;

            for (int i = 0, n = active.length; i < n; i++) {
                if (active[i]) particles[i].draw(batch, alphaModulation);
            }

            if (cleansUpBlendFunction() && (additive || premultipliedAlpha))
                batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

        }
    }
}
