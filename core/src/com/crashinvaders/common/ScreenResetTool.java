package com.crashinvaders.common;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.badlogic.gdx.utils.reflect.ReflectionException;

public class ScreenResetTool {
    private static final String TAG = ScreenResetTool.class.getSimpleName();

    // Closed CTOR
    private ScreenResetTool() { }

    public static Screen tryCreateScreen(Screen prototype) {
        // Check if the screen is able to reproduce own reset copy.
        if (prototype instanceof InstanceProvider) {
            Gdx.app.debug(TAG,
                    "Restarting " +
                    prototype.getClass().getSimpleName() +
                    " through new instance using InstanceProvider.");
            InstanceProvider instanceProvider = (InstanceProvider) prototype;
            return instanceProvider.createResetScreenInstance();
        }

        // Try to recreate screen instance using reflection.
        Class<? extends Screen> currentScreenClass = prototype.getClass();
        try {
            Gdx.app.debug(TAG,
                    "Restarting " +
                    prototype.getClass().getSimpleName() +
                    " through new instance using reflection.");
            return ClassReflection.newInstance(currentScreenClass);
        } catch (ReflectionException e) {
            Gdx.app.error("App", "Can't instantiate new " + currentScreenClass.getSimpleName() + " through reflection.", null);
            return null;
        }
    }

    /** May be implemented by {@link Screen} in order to help {@link ScreenResetTool#tryCreateScreen(Screen)}
     * to restart it by providing a proper instance recreation logic. */
    public static interface InstanceProvider<T extends Screen> {
        T createResetScreenInstance();
    }
}
