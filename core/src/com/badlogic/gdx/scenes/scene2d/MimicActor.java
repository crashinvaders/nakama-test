package com.badlogic.gdx.scenes.scene2d;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;

public class MimicActor extends Actor {
    private static final Vector2 tmpVec2 = new Vector2();

    private Actor actor;

    /** Has no actor to mimic. See {@link #setActor(Actor)}. */
    public MimicActor() {
    }

    /** @param actor will be mimicked. */
    public MimicActor(final Actor actor) {
        this.actor = actor;
    }

    @Override
    public boolean remove () {
        actor = null;
        return super.remove();
    }

    /** @return mimicked actor. */
    public Actor getActor () {
        return actor;
    }

    /** @param actor will be mimicked. */
    public void setActor (final Actor actor) {
        this.actor = actor;
    }

    @Override
    public float getWidth () {
        return actor == null ? 0f : actor.getWidth();
    }

    @Override
    public float getHeight () {
        return actor == null ? 0f : actor.getHeight();
    }

    @Override
    public void draw (final Batch batch, final float parentAlpha) {
        if (actor != null) {

            float x = getX();
            float y = getY();

            // Check if wrapped actor is a group with enabled transform and compute translated position.
            if (actor instanceof Group) {
                Group group = (Group) this.actor;
                if (group.isTransform()) {
                    this.getParent().localToStageCoordinates(tmpVec2.set(x, y));
                    group.getParent().stageToLocalCoordinates(tmpVec2);
                    x = tmpVec2.x;
                    y = tmpVec2.y;
                }
            }

            tmpVec2.set(actor.getX(), actor.getY());
            actor.setPosition(x, y);
            actor.draw(batch, getColor().a * parentAlpha);
            actor.setPosition(tmpVec2.x, tmpVec2.y);
        }
    }
}