package com.metaphore.nakamatest;

import com.badlogic.gdx.*;
import com.badlogic.gdx.assets.AssetManager;
import com.crashinvaders.common.PauseManager;
import com.crashinvaders.common.PrioritizedInputMultiplexer;
import com.crashinvaders.common.ScreenResetTool;
import com.crashinvaders.common.eventmanager.EventManager;
import com.crashinvaders.common.eventmanager.RegularEventManager;
import com.metaphore.nakamatest.communication.ServerConnectionManager;
import com.metaphore.nakamatest.screens.connection.ConnectionScreen;
import com.metaphore.nakamatest.screens.load.LoadScreen;
import com.metaphore.nakamatest.screens.proto.ProtoScreen;

public class App extends Game {
	private static final String TAG = App.class.getSimpleName();

	private static App instance;

	private final ActionResolver actionResolver;
	private final AppParams params;
	private final PrioritizedInputMultiplexer inputMultiplexer;
	private final RegularEventManager eventManager;
	private final PauseManager pauseManager;

	private ServerConnectionManager connectionManager;
	private AssetManager assets;

	public static App inst() {
		if (instance == null) {
			throw new IllegalStateException("App is not initialized yet!");
		}
		return instance;
	}

	public App(AppParams params, ActionResolver actionResolver) {
		this.actionResolver = actionResolver;
		this.params = params;

		inputMultiplexer = new PrioritizedInputMultiplexer();
		inputMultiplexer.setMaxPointers(Integer.MAX_VALUE);
		eventManager = new RegularEventManager();
		pauseManager = new PauseManager();
	}

	@Override
	public void create () {
		instance = this;

		Gdx.app.setLogLevel(params.debug ? Application.LOG_DEBUG : Application.LOG_INFO);
		if (params.debug) {
			Gdx.app.log(TAG, "Debug mode is ON.");
		}

		Gdx.input.setCatchBackKey(true);
		Gdx.input.setInputProcessor(inputMultiplexer);

		inputMultiplexer.addProcessor(new GlobalInputHandler(), -Integer.MAX_VALUE);
		if (params.debug) {
			inputMultiplexer.addProcessor(new GlobalDebugInputHandler(), Integer.MAX_VALUE);
		}

		connectionManager = new ServerConnectionManager(eventManager);

		setScreen(new LoadScreen(params, new LoadScreen.Listener() {
			@Override
			public void onLoadComplete(AssetManager assets) {
				App.this.assets = assets;

//				setScreen(new NakamaTestScreen());
//				setScreen(new ConnectionScreen());
				setScreen(new ProtoScreen());

			}
		}));
	}

	@Override
	public void dispose () {
		setScreen(null);

		inputMultiplexer.clear();
		Gdx.input.setInputProcessor(null);

		eventManager.clear();

		connectionManager.dispose();
		connectionManager = null;

		instance = null;
	}

	@Override
	public void resume() {
		super.resume();
		Gdx.input.setInputProcessor(inputMultiplexer);
	}

	@Override
	public void pause() {
		super.pause();
		Gdx.input.setInputProcessor(null);
	}

	@Override
	public void setScreen(Screen screen) {
		if (this.screen != null) {
			this.screen.hide();
			this.screen.dispose();
			this.screen = null;
		}
		this.screen = screen;
		if (this.screen != null) {
			this.screen.show();
			this.screen.resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		}
	}

	//region Accessors
	public AppParams getParams() {
		return params;
	}
	public ActionResolver getActionResolver() {
		return actionResolver;
	}
	public PrioritizedInputMultiplexer getInput() {
		return inputMultiplexer;
	}
	public EventManager getEvents() {
		return eventManager;
	}
	public PauseManager getPauseManager() {
		return pauseManager;
	}
	public AssetManager getAssets() {
		return assets;
	}
	public ServerConnectionManager getConnectionManager() {
		return connectionManager;
	}
	//endregion

	public void restartGame() {
		pauseManager.releaseAll();
		dispose();
		Gdx.app.debug(TAG, "App is restarting...\n");
		create();
	}

	public void restartCurrentScreen() {
		Screen currentScreen = getScreen();
		if (currentScreen == null) return;

		Screen newScreen = ScreenResetTool.tryCreateScreen(currentScreen);
		if (newScreen != null) {
			setScreen(newScreen);
		}
	}

	private class GlobalInputHandler extends InputAdapter {
		@Override
		public boolean keyDown(int keycode) {
			switch (keycode) {
				case Input.Keys.ESCAPE:
				case Input.Keys.BACK:
					Gdx.app.exit();
					return true;
				default:
					return super.keyDown(keycode);
			}
		}
	}

	private class GlobalDebugInputHandler extends InputAdapter {
		@Override
		public boolean keyDown(int keycode) {
			switch (keycode) {
				case Input.Keys.F8:
					// Restart entire game
					restartGame();
					return true;
				case Input.Keys.F9:
					// Restart current screen
					restartCurrentScreen();
					return true;
				default:
					return super.keyDown(keycode);
			}
		}
	}
}
