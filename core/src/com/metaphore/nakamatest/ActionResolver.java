package com.metaphore.nakamatest;

/** Native procedure call bridge. */
public interface ActionResolver {
    /** Unique device identifier. */
    String getDeviceUuid();
}
