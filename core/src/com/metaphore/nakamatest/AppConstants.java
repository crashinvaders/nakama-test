package com.metaphore.nakamatest;

import com.badlogic.gdx.graphics.Color;

public class AppConstants {
    public static final String APP_DATA_DIR = ".pref/nakama_test/";
    public static final String PREF_NAME_COMMON = "common.xml";

    public static final Color clearColor = new Color(0x202020ff);
}
