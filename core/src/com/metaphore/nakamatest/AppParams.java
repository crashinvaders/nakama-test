package com.metaphore.nakamatest;

public class AppParams {
    /** Turn on application debug features */
    public boolean debug = false;
    /** {@link LazyAssetManagerX} will be used */
    public boolean lazyLoad = false;
}
