package com.metaphore.nakamatest.screens.load;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.ObjectMap;
import com.crashinvaders.common.assets.LazyAssetManager;
import com.crashinvaders.common.scene2d.skin.SkinX;
import com.crashinvaders.common.scene2d.skin.SkinXLoader;
import com.crashinvaders.common.spine.SkeletonDataLoader;
import com.esotericsoftware.spine.SkeletonData;
import com.metaphore.nakamatest.AppParams;

public class LoadScreen extends ScreenAdapter {

    private final Color clearColor = new Color();

    private final Listener listener;
    private final AssetManager assetManager;
    private final LoadScene loadScene;
    private boolean loadComplete;

    public LoadScreen(AppParams appParams, Listener listener) {
        this.listener = listener;

        if (appParams.lazyLoad) {
            assetManager = new LazyAssetManager();
        } else {
            assetManager = new AssetManager();
        }
        initializeAssets(assetManager);

        loadScene = new LoadScene();
    }

    @Override
    public void show() {
        super.show();
        clearColor.set(Color.DARK_GRAY);
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        loadScene.resize(width, height);
    }

    @Override
    public void render(float delta) {
        Gdx.gl20.glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a);
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);
        loadScene.render(delta, assetManager.getProgress());

        if (assetManager.update() && !loadComplete) {
            loadComplete = true;
            onAssetsLoaded(assetManager);

            Gdx.app.postRunnable(new Runnable() {
                @Override
                public void run() {
                    listener.onLoadComplete(assetManager);
                }
            });
        }
    }

    private void initializeAssets(AssetManager assets) {
        final FileHandleResolver fileHandleResolver = assets.getFileHandleResolver();
        assets.setLoader(SkeletonData.class, new SkeletonDataLoader(fileHandleResolver));
        assets.setLoader(SkinX.class, new SkinXLoader(fileHandleResolver));

        // Atlases.
        {
            assets.load("atlases/screen-proto.atlas", TextureAtlas.class);
        }

        // Skins.
        {
            assets.load("skins/uiskin.json", SkinX.class);
        }
    }

    private void onAssetsLoaded(AssetManager assets) {
        // Enable color markup for all the skin fonts.
        SkinX skin = assets.get("skins/uiskin.json", SkinX.class);
        for (ObjectMap.Entry<String, BitmapFont> entry : skin.getAll(BitmapFont.class)) {
            entry.value.getData().markupEnabled = true;
        }
    }

    public interface Listener {
        void onLoadComplete(AssetManager assets);
    }
}
