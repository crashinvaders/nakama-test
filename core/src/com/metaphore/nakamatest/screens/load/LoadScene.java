package com.metaphore.nakamatest.screens.load;

import com.badlogic.gdx.utils.Disposable;

class LoadScene implements Disposable {

    @Override
    public void dispose() {

    }

    public void resize(float width, float height) {

    }

    public void render(float delta, float loadProgress) {

    }
}