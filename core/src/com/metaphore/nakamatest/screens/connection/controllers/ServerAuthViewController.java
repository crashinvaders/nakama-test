package com.metaphore.nakamatest.screens.connection.controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.utils.Array;
import com.crashinvaders.common.lml.CommonLmlParser;
import com.crashinvaders.common.viewcontroller.LmlViewController;
import com.crashinvaders.common.viewcontroller.ViewControllerManager;
import com.github.czyzby.lml.annotation.LmlAction;
import com.github.czyzby.lml.annotation.LmlActor;

public class ServerAuthViewController extends LmlViewController {

    @LmlActor Group groupServerAuth;
    @LmlActor TextField edtServerUrl;
    @LmlActor TextField edtDeviceUuid;

    private DataStorageController dataStorage;

    public ServerAuthViewController(ViewControllerManager viewControllers, CommonLmlParser lmlParser) {
        super(viewControllers, lmlParser);
    }

    @Override
    public void onViewCreated(Group sceneRoot) {
        super.onViewCreated(sceneRoot);
        processLmlFields(this);

        dataStorage = getController(DataStorageController.class);

        // Load the most recent used values by default.
        if (!dataStorage.getServerUrls().isEmpty()) {
            edtServerUrl.setText(dataStorage.getServerUrls().first());
        }
        if (!dataStorage.getDeviceUuids().isEmpty()) {
            edtDeviceUuid.setText(dataStorage.getDeviceUuids().first());
        } else {
            String deviceUuid = getController(DeviceUuidViewController.class).getDeviceUuid();
            edtDeviceUuid.setText(deviceUuid);
        }
    }

    public void show() {
        groupServerAuth.setVisible(true);
    }

    public void hide() {
        groupServerAuth.setVisible(false);
    }

    @LmlAction void showOldServerUrls() {
        Array<String> serverUrls = dataStorage.getServerUrls();
        if (serverUrls.size == 0) return;

        Gdx.input.setOnscreenKeyboardVisible(false);
        getController(ListItemSelectPopupController.class).show(
                serverUrls,
                new ListItemSelectPopupController.ResultListener<String>() {
                    @Override
                    public void onItemSelected(String selectedItem) {
                        edtServerUrl.setText(selectedItem);
                    }
                }
        );
    }

    @LmlAction void showOldDeviceUuid() {
        Array<String> deviceUuids = dataStorage.getDeviceUuids();
        if (deviceUuids.size == 0) return;

        Gdx.input.setOnscreenKeyboardVisible(false);
        getController(ListItemSelectPopupController.class).show(
                deviceUuids,
                new ListItemSelectPopupController.ResultListener<String>() {
                    @Override
                    public void onItemSelected(String selectedItem) {
                        edtDeviceUuid.setText(selectedItem);
                    }
                }
        );
    }

    @LmlAction void tryConnect() {
        String serverUrl = edtServerUrl.getText().trim();
        String deviceUuid = edtDeviceUuid.getText().trim();
        if (serverUrl.isEmpty() || deviceUuid.isEmpty()) return;

        dataStorage.storeServerUrl(serverUrl);
        dataStorage.storeDeviceUuid(deviceUuid);

        getController(ConnectionStateController.class).authenticateOnTheServer(serverUrl, deviceUuid);
    }
}
