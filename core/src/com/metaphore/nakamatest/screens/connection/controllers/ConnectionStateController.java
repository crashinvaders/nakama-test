package com.metaphore.nakamatest.screens.connection.controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonWriter;
import com.crashinvaders.common.eventmanager.EventHandler;
import com.crashinvaders.common.eventmanager.EventInfo;
import com.crashinvaders.common.eventmanager.EventParams;
import com.crashinvaders.common.viewcontroller.ViewController;
import com.crashinvaders.common.viewcontroller.ViewControllerManager;
import com.heroiclabs.nakama.UserPresence;
import com.metaphore.nakamatest.App;
import com.metaphore.nakamatest.communication.CommData;
import com.metaphore.nakamatest.communication.ServerConnectionManager;
import com.metaphore.nakamatest.communication.ServerConnectionManager.MatchSession;
import com.metaphore.nakamatest.communication.events.MatchDataEvent;
import com.metaphore.nakamatest.communication.events.MatchDisconnectedEvent;

public class ConnectionStateController implements ViewController, EventHandler {
    private static final String TAG = ConnectionStateController.class.getSimpleName();

    private final ViewControllerManager viewControllers;
    private final ServerConnectionManager connectionManager;

    private final Json jsonSerializer;

    private ConnectionState connectionState = ConnectionState.NOT_CONNECTED;
    private BusyIndicatorViewController busyIndicatorView;
    private ToastMessageViewController toastMessageView;
    private ServerAuthViewController serverAuthView;
    private MatchConnectionViewController matchConnectionView;
    private PreGameLobbyViewController preGameLobbyView;

    public ConnectionStateController(ViewControllerManager viewControllers) {
        this.viewControllers = viewControllers;
        connectionManager = App.inst().getConnectionManager();

        jsonSerializer = new Json(JsonWriter.OutputType.json);
    }

    @Override
    public void onViewCreated(Group sceneRoot) {
        busyIndicatorView = viewControllers.get(BusyIndicatorViewController.class);
        toastMessageView = viewControllers.get(ToastMessageViewController.class);
        serverAuthView = viewControllers.get(ServerAuthViewController.class);
        matchConnectionView = viewControllers.get(MatchConnectionViewController.class);
        preGameLobbyView = viewControllers.get(PreGameLobbyViewController.class);

        if (!connectionManager.hasConnection()) {
            connectionState = ConnectionState.NOT_CONNECTED;
            serverAuthView.show();
        } else {
            if (connectionManager.getMatch() == null) {
                connectionState = ConnectionState.AUTHENTICATED;
                matchConnectionView.show();
            } else {
                connectionState = ConnectionState.PRE_GAME_LOBBY;
                preGameLobbyView.show();
            }
        }

        App.inst().getEvents().addHandler(this,
                MatchDataEvent.class,
                MatchDisconnectedEvent.class);
    }

    @Override
    public void dispose() {
        App.inst().getEvents().removeHandler(this);
    }

    @Override
    public void update(float delta) {

    }

    @Override
    public void handle(EventInfo event, EventParams params) {
        if (event instanceof MatchDataEvent) {
            MatchDataEvent e = (MatchDataEvent) event;
            String jsonData = new String(e.getData());

            switch ((int) e.getOpCode()) {
                case CommData.OPC_GAME_STARTED: {
                    CommData.OnGameStarted data = jsonSerializer.fromJson(CommData.OnGameStarted.class, jsonData);
                    onCommGameStarted(data);
                    break;
                }
            }

            Gdx.app.debug(TAG, "Received message. OP code: " + e.getOpCode() + " Message: " + jsonData);

            //TODO Handle messages here.
        } else

        if (event instanceof MatchDisconnectedEvent) {
            if (connectionState.ordinal() >= ConnectionState.PRE_GAME_LOBBY.ordinal()) {
                connectionState = ConnectionState.AUTHENTICATED;

                serverAuthView.hide();
                matchConnectionView.show();
                preGameLobbyView.hide();

                showErrorMessage("Match connection lost");
            }
        }
    }

    public void authenticateOnTheServer(String serverUrl, String deviceUuid) {
        ensureExactConnectionState(ConnectionState.NOT_CONNECTED);

        busyIndicatorView.show();
        connectionState = ConnectionState.AUTHENTICATING;

//        String deviceUuid = App.inst().getActionResolver().getDeviceUuid();
        connectionManager.connect(serverUrl, deviceUuid, new ServerConnectionManager.ConnectionResultListener() {
            @Override
            public void onSuccess() {
                busyIndicatorView.hide();
                connectionState = ConnectionState.AUTHENTICATED;

                serverAuthView.hide();
                matchConnectionView.show();
            }

            @Override
            public void onError(Exception exception) {
                busyIndicatorView.hide();
                connectionState = ConnectionState.NOT_CONNECTED;

                showErrorMessage(exception.getMessage());
            }
        });
    }

    public void joinMatch(String matchId) {
        ensureExactConnectionState(ConnectionState.AUTHENTICATED);

        busyIndicatorView.show();
        connectionState = ConnectionState.JOINING_MATCH;

        connectionManager.joinMatch(matchId, new ServerConnectionManager.ConnectionResultListener() {
            @Override
            public void onSuccess() {
                busyIndicatorView.hide();
                connectionState = ConnectionState.PRE_GAME_LOBBY;

                matchConnectionView.hide();
                preGameLobbyView.show();
            }

            @Override
            public void onError(Exception exception) {
                busyIndicatorView.hide();
                connectionState = ConnectionState.AUTHENTICATED;

                showErrorMessage(exception.getMessage());
            }
        });
    }

    public void createMatch() {
        ensureExactConnectionState(ConnectionState.AUTHENTICATED);

        busyIndicatorView.show();
        connectionState = ConnectionState.JOINING_MATCH;

        connectionManager.createMatch(new ServerConnectionManager.ConnectionResultListener() {
            @Override
            public void onSuccess() {
                busyIndicatorView.hide();
                connectionState = ConnectionState.PRE_GAME_LOBBY;

                matchConnectionView.hide();
                preGameLobbyView.show();
            }

            @Override
            public void onError(Exception exception) {
                busyIndicatorView.hide();
                connectionState = ConnectionState.AUTHENTICATED;

                showErrorMessage(exception.getMessage());
            }
        });

    }

    public String getMatchId() {
        ensureExactConnectionState(ConnectionState.PRE_GAME_LOBBY);

        MatchSession match = connectionManager.getMatch();
        if (match == null) return null;

        return match.getMatchId();
    }

    public String getUsername() {
        ensureMinConnectionState(ConnectionState.AUTHENTICATED);

        return connectionManager.getUsername();
    }

    public Array<UserPresence> getMatchPlayers() {
        ensureExactConnectionState(ConnectionState.PRE_GAME_LOBBY);

        Array<UserPresence> result = new Array<>();

        MatchSession match = connectionManager.getMatch();
        if (match == null) return result;

        result.addAll(match.getPlayers());
        return result;
    }

    public void startGame() {
        ensureExactConnectionState(ConnectionState.PRE_GAME_LOBBY);

        CommData.StartGameRequest commData = new CommData.StartGameRequest();
        commData.random_seed = MathUtils.random(Long.MAX_VALUE - 1);

        Gdx.app.debug(TAG, "Request game start. Random seed: " + commData.random_seed);

//        kryo.writeObject(kryoOutput, commData);
//        byte[] data = kryoOutput.getBuffer();

        String jsonData = jsonSerializer.toJson(commData);
        Gdx.app.debug(TAG, "Message data: " + jsonData);

        connectionManager.sendMatchData(commData.getOpCode(), jsonData.getBytes());
    }

    private void showErrorMessage(String errorText) {
        toastMessageView.show("Error: " + errorText, 5f);
    }

    private void ensureExactConnectionState(ConnectionState requiredState) {
        if (connectionState != requiredState) {
            throw new IllegalStateException("Current connection state: " + connectionState +
                    " doesn't match the exact required one: " + requiredState);
        }
    }

    private void ensureMinConnectionState(ConnectionState requiredState) {
        if (connectionState.ordinal() < requiredState.ordinal()) {
            throw new IllegalStateException("Current connection state: " + connectionState +
                    " doesn't match the minimum required one: " + requiredState);
        }
    }

    //region Server comm message handlers.
    private void onCommGameStarted(CommData.OnGameStarted data) {


    }
    //endregion

    public enum ConnectionState {
        NOT_CONNECTED,
        AUTHENTICATING,
        AUTHENTICATED,
        JOINING_MATCH,
        PRE_GAME_LOBBY,
        GAME_STARTED
    }
}
