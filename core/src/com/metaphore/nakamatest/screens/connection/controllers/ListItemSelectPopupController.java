package com.metaphore.nakamatest.screens.connection.controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.crashinvaders.common.lml.CommonLmlParser;
import com.crashinvaders.common.lml.LmlUtils;
import com.crashinvaders.common.viewcontroller.LmlViewController;
import com.crashinvaders.common.viewcontroller.ViewControllerManager;
import com.github.czyzby.lml.annotation.LmlActor;
import com.github.czyzby.lml.parser.action.ActionContainer;

public class ListItemSelectPopupController extends LmlViewController {

    private Group popupHolder;

    private PopupViewController popup = null;

    public ListItemSelectPopupController(ViewControllerManager viewControllers, CommonLmlParser lmlParser) {
        super(viewControllers, lmlParser);
    }

    @Override
    public void onViewCreated(Group sceneRoot) {
        super.onViewCreated(sceneRoot);
        popupHolder = sceneRoot.findActor("popupHolder");
    }

    @Override
    public void dispose() {
        super.dispose();
        popupHolder = null;
    }

    public void show(Array items, ResultListener resultListener) {
        if (popup != null) return;

        popup = new PopupViewController(lmlParser, items, resultListener);
        popupHolder.addActor(popup.rootView);
    }

    public void hide() {
        if (popup == null) return;

        popupHolder.removeActor(popup.rootView);
        popup = null;
    }

    private class PopupViewController implements ActionContainer {

        @LmlActor List<String> listTextItems;

        private final Group rootView;

        public PopupViewController(CommonLmlParser lmlParser, Array items, final ResultListener resultListener) {

            rootView = LmlUtils.parseLmlTemplate(lmlParser, this, Gdx.files.internal("lml/connection/text-item-select-popup.lml"));

            listTextItems.clearItems();
            listTextItems.setItems(items);
            listTextItems.setSelectedIndex(-1);
            listTextItems.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    String selectedItem = listTextItems.getSelected();
                    ListItemSelectPopupController.this.hide();

                    if (resultListener != null) {
                        resultListener.onItemSelected(selectedItem);
                    }
                }
            });
        }
    }

    public interface ResultListener<T> {
        void onItemSelected(T selectedItem);
    }
}
