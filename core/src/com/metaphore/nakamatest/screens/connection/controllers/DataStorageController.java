package com.metaphore.nakamatest.screens.connection.controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;
import com.crashinvaders.common.viewcontroller.ViewController;

public class DataStorageController implements ViewController {
    private static final String PREF_NAME = "connection.xml";
    private static final String PREF_KEY_USERNAMES = "usernames";
    private static final String PREF_KEY_SERVER_URLS = "server_urls";
    private static final String PREF_KEY_DEVICE_UUIDS = "device_uuids";

    private final Array<String> usernames = new Array<>();
    private final Array<String> serverUrls = new Array<>();
    private final Array<String> deviceUuids = new Array<>();

    private final Preferences prefs;

    public DataStorageController() {
        prefs = Gdx.app.getPreferences(PREF_NAME);

        // Load data from storage.
        String serializedUsernames = prefs.getString(PREF_KEY_USERNAMES, null);
        if (serializedUsernames != null) {
            toArray(usernames, serializedUsernames);
        }
        String serializedServerUrls = prefs.getString(PREF_KEY_SERVER_URLS, null);
        if (serializedServerUrls != null) {
            toArray(serverUrls, serializedServerUrls);
        }
        String serializedDeviceUuids = prefs.getString(PREF_KEY_DEVICE_UUIDS, null);
        if (serializedDeviceUuids != null) {
            toArray(deviceUuids, serializedDeviceUuids);
        }
    }

    @Override
    public void onViewCreated(Group sceneRoot) {

    }

    @Override
    public void dispose() {

    }

    @Override
    public void update(float delta) {

    }

    public void storeUsername(String username) {
        usernames.removeValue(username, false);
        usernames.insert(0, username);
        String serialized = fromArray(usernames);
        prefs.putString(PREF_KEY_USERNAMES, serialized).flush();
    }

    public void storeServerUrl(String serverUrl) {
        serverUrls.removeValue(serverUrl, false);
        serverUrls.insert(0, serverUrl);
        String serialized = fromArray(serverUrls);
        prefs.putString(PREF_KEY_SERVER_URLS, serialized).flush();
    }

    public void storeDeviceUuid(String deviceUuid) {
        deviceUuids.removeValue(deviceUuid, false);
        deviceUuids.insert(0, deviceUuid);
        String serialized = fromArray(deviceUuids);
        prefs.putString(PREF_KEY_DEVICE_UUIDS, serialized).flush();
    }

    public Array<String> getUsernames() {
        return usernames;
    }

    public Array<String> getServerUrls() {
        return serverUrls;
    }

    public Array<String> getDeviceUuids() {
        return deviceUuids;
    }

    private static String fromArray(Array<String> values) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < values.size; i++) {
            sb.append(values.get(i));
            if (i < values.size - 1) {
                sb.append(';');
            }
        }
        return sb.toString();
    }

    private static void toArray(Array<String> array, String serializedValues) {
        String[] values = serializedValues.split(";");
        array.clear();
        array.addAll(values);
    }
}
