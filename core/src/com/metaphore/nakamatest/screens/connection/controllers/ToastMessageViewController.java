package com.metaphore.nakamatest.screens.connection.controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Align;
import com.crashinvaders.common.lml.CommonLmlParser;
import com.crashinvaders.common.lml.LmlUtils;
import com.crashinvaders.common.scene2d.actions.ActionsExt;
import com.crashinvaders.common.viewcontroller.LmlViewController;
import com.crashinvaders.common.viewcontroller.ViewControllerManager;
import com.github.czyzby.lml.annotation.LmlActor;
import com.github.czyzby.lml.parser.LmlParser;
import com.github.czyzby.lml.parser.action.ActionContainer;

public class ToastMessageViewController extends LmlViewController {

    @LmlActor Group popupHolder;

    public ToastMessageViewController(ViewControllerManager viewControllers, CommonLmlParser lmlParser) {
        super(viewControllers, lmlParser);
    }

    @Override
    public void onViewCreated(Group sceneRoot) {
        super.onViewCreated(sceneRoot);
        processLmlFields(this);
    }

    public void show(String text, float duration) {
        final ToastPopupView popup = new ToastPopupView(lmlParser, text);
        popup.show(popupHolder);
        popupHolder.addAction(Actions.delay(duration, Actions.run(new Runnable() {
            @Override
            public void run() {
                popup.hide(popupHolder);
            }
        })));
    }

    private class ToastPopupView implements ActionContainer {

        final Group viewRoot;

        public ToastPopupView(LmlParser lmlParser, String text) {

            lmlParser.getData().addArgument("toastText", text);

            viewRoot = LmlUtils.parseLmlTemplate(lmlParser, this,
                    Gdx.files.internal("lml/connection/text-toast-popup.lml"));
        }

        public void show(Group parent) {
            parent.addActor(viewRoot);
            viewRoot.setVisible(false);
            viewRoot.addAction(Actions.sequence(
                    ActionsExt.origin(Align.center),
                    Actions.alpha(0f),
                    Actions.scaleTo(1.1f, 1.1f),
                    Actions.moveBy(0f, -50f),
                    Actions.visible(true),
                    Actions.parallel(
                            Actions.fadeIn(0.25f),
                            Actions.scaleTo(1f, 1f, 0.25f, Interpolation.elasticOut),
                            Actions.moveBy(0f, +50f, 0.5f, Interpolation.exp10Out))
            ));
        }

        public void hide(Group parent) {
            viewRoot.clearActions();
            viewRoot.addAction(Actions.sequence(
                    Actions.parallel(
                            Actions.fadeOut(0.5f),
                            Actions.scaleTo(1.1f, 1.1f, 0.5f, Interpolation.exp5In),
                            Actions.moveBy(0f, -50f, 0.6f, Interpolation.exp5In)),
                    Actions.removeActor()
            ));
        }
    }
}
