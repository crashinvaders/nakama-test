package com.metaphore.nakamatest.screens.connection.controllers;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Align;
import com.crashinvaders.common.lml.CommonLmlParser;
import com.crashinvaders.common.scene2d.actions.ActionsExt;
import com.crashinvaders.common.viewcontroller.LmlViewController;
import com.crashinvaders.common.viewcontroller.ViewControllerManager;
import com.github.czyzby.lml.annotation.LmlActor;

public class BusyIndicatorViewController extends LmlViewController {

    @LmlActor Group groupBusyIndicator;

    public BusyIndicatorViewController(ViewControllerManager viewControllers, CommonLmlParser lmlParser) {
        super(viewControllers, lmlParser);
    }

    @Override
    public void onViewCreated(Group sceneRoot) {
        super.onViewCreated(sceneRoot);
        processLmlFields(this);
    }

    public void show() {
        groupBusyIndicator.clearActions();
        groupBusyIndicator.addAction(Actions.sequence(
                ActionsExt.origin(Align.center),
                Actions.alpha(0f),
                Actions.scaleTo(1.1f, 1.1f),
                Actions.visible(true),
                Actions.parallel(
                        Actions.fadeIn(0.5f),
                        Actions.scaleTo(1f, 1f, 0.5f, Interpolation.elasticOut))
        ));
    }

    public void hide() {
        groupBusyIndicator.clearActions();
        groupBusyIndicator.addAction(Actions.sequence(
                ActionsExt.origin(Align.center),
                Actions.visible(true),
                Actions.parallel(
                        Actions.fadeOut(0.5f),
                        Actions.scaleTo(1.1f, 1.1f, 0.5f, Interpolation.exp5In)),
                Actions.visible(false)
        ));
    }
}
