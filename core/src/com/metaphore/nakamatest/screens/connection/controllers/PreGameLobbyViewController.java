package com.metaphore.nakamatest.screens.connection.controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.ObjectSet;
import com.crashinvaders.common.eventmanager.EventHandler;
import com.crashinvaders.common.eventmanager.EventInfo;
import com.crashinvaders.common.eventmanager.EventParams;
import com.crashinvaders.common.lml.CommonLmlParser;
import com.crashinvaders.common.lml.LmlUtils;
import com.crashinvaders.common.viewcontroller.LmlViewController;
import com.crashinvaders.common.viewcontroller.ViewControllerManager;
import com.github.czyzby.lml.annotation.LmlAction;
import com.github.czyzby.lml.annotation.LmlActor;
import com.github.czyzby.lml.parser.LmlParser;
import com.github.czyzby.lml.parser.action.ActionContainer;
import com.heroiclabs.nakama.UserPresence;
import com.metaphore.nakamatest.App;
import com.metaphore.nakamatest.communication.events.MatchPlayersChangedEvent;

import java.util.Iterator;

public class PreGameLobbyViewController extends LmlViewController implements EventHandler {
    private static final String TAG = PreGameLobbyViewController.class.getSimpleName();

    @LmlActor Group groupPreGameLobby;
    @LmlActor Label lblLobbyMatchId;
    @LmlActor HorizontalGroup groupMatchPlayers;

    private final ArrayMap<String, PlayerListItemView> playerListItems = new ArrayMap<>();

    private ConnectionStateController connectionController;

    public PreGameLobbyViewController(ViewControllerManager viewControllers, CommonLmlParser lmlParser) {
        super(viewControllers, lmlParser);
    }

    @Override
    public void onViewCreated(Group sceneRoot) {
        super.onViewCreated(sceneRoot);
        processLmlFields(this);

        connectionController = getController(ConnectionStateController.class);
    }

    public void show() {
        groupPreGameLobby.setVisible(true);

        String matchId = connectionController.getMatchId();
        lblLobbyMatchId.setText(matchId);

        updatePlayerList();

        App.inst().getEvents().addHandler(this, MatchPlayersChangedEvent.class);
    }

    public void hide() {
        groupPreGameLobby.setVisible(false);

        App.inst().getEvents().removeHandler(this);
    }

    @Override
    public void handle(EventInfo event, EventParams params) {
        if (event instanceof MatchPlayersChangedEvent) {
            updatePlayerList();
        }
    }

    @LmlAction void startGame() {
        connectionController.startGame();
    }

    @LmlAction void copyMatchIdToClipboard() {
        String matchId = connectionController.getMatchId();
        Gdx.app.getClipboard().setContents(matchId);
        Gdx.app.log(TAG, "Match ID has been copied to clipboard.");
        getController(ToastMessageViewController.class).show("Match ID has been copied to clipboard.", 2f);
    }

    private void updatePlayerList() {
        Array<UserPresence> matchPlayers = connectionController.getMatchPlayers();

        ObjectSet<String> connectedUserIds = new ObjectSet<>(matchPlayers.size);

        for (int i = 0; i < matchPlayers.size; i++) {
            UserPresence userPresence = matchPlayers.get(i);
            connectedUserIds.add(userPresence.getUserId());

            // Create missing player item views.
            if (playerListItems.containsKey(userPresence.getUserId())) continue;

            PlayerListItemView playerView = new PlayerListItemView(
                    lmlParser,
                    userPresence.getUserId(),
                    userPresence.getUsername());

            playerListItems.put(playerView.userId, playerView);
        }

        // Remove all the user item views that are missing in the match.
        Iterator<ObjectMap.Entry<String, PlayerListItemView>> iterator = playerListItems.iterator();
        while (iterator.hasNext()) {
            String userId = iterator.next().key;
            if (!connectedUserIds.contains(userId)) {
                iterator.remove();
            }
        }

        // Repopulate player list.
        {
            groupMatchPlayers.clearChildren();
            for (int i = 0; i < playerListItems.size; i++) {
                PlayerListItemView playerView = playerListItems.getValueAt(i);
                groupMatchPlayers.addActor(playerView.viewRoot);
            }
        }
    }

    private static class PlayerListItemView implements ActionContainer {

        @LmlActor Label lblUsername;
        @LmlActor Label lblStatus;

        final String userId;
        final String username;

        final Group viewRoot;

        private boolean ready = false;

        public PlayerListItemView(LmlParser lmlParser, String userId, String username) {
            this.userId = userId;
            this.username = username;

            viewRoot = LmlUtils.parseLmlTemplate(lmlParser, this,
                    Gdx.files.internal("lml/connection/player-list-item.lml"));

            updateView();
        }

        public boolean isReady() {
            return ready;
        }

        public void setReady(boolean ready) {
            this.ready = ready;
            updateView();
        }

        private void updateView() {
            lblUsername.setText(username);

            if (ready) {
                lblStatus.setText("Ready");
                lblStatus.setColor(Color.GREEN);
            } else {
                lblStatus.setText("In Lobby");
                lblStatus.setColor(Color.GRAY);
            }
        }
    }
}
