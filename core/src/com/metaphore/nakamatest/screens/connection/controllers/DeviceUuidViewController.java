package com.metaphore.nakamatest.screens.connection.controllers;

import com.crashinvaders.common.lml.CommonLmlParser;
import com.crashinvaders.common.viewcontroller.LmlViewController;
import com.crashinvaders.common.viewcontroller.ViewControllerManager;
import com.metaphore.nakamatest.App;

public class DeviceUuidViewController extends LmlViewController {

    private final String deviceUuid;

    public DeviceUuidViewController(ViewControllerManager viewControllers, CommonLmlParser lmlParser) {
        super(viewControllers, lmlParser);

        deviceUuid = App.inst().getActionResolver().getDeviceUuid();
        lmlParser.getData().addArgument("deviceUuid", deviceUuid);
    }

    public String getDeviceUuid() {
        return deviceUuid;
    }
}
