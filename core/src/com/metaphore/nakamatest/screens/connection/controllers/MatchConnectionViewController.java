package com.metaphore.nakamatest.screens.connection.controllers;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.crashinvaders.common.lml.CommonLmlParser;
import com.crashinvaders.common.viewcontroller.LmlViewController;
import com.crashinvaders.common.viewcontroller.ViewControllerManager;
import com.github.czyzby.lml.annotation.LmlAction;
import com.github.czyzby.lml.annotation.LmlActor;

public class MatchConnectionViewController extends LmlViewController {

    @LmlActor Group groupMatchConnection;
    @LmlActor TextField edtMatchId;
    @LmlActor Label lblLocalUsername;

    private ConnectionStateController connectionController;

    public MatchConnectionViewController(ViewControllerManager viewControllers, CommonLmlParser lmlParser) {
        super(viewControllers, lmlParser);
    }

    @Override
    public void onViewCreated(Group sceneRoot) {
        super.onViewCreated(sceneRoot);
        processLmlFields(this);

        connectionController = getController(ConnectionStateController.class);
    }

    public void show() {
        groupMatchConnection.setVisible(true);

        String username = connectionController.getUsername();
        lblLocalUsername.setText(username);
    }

    public void hide() {
        groupMatchConnection.setVisible(false);
    }

    @LmlAction void tryJoinMatch() {
        String matchId = edtMatchId.getText().trim();
        if (matchId.isEmpty()) return;

        connectionController.joinMatch(matchId);
    }

    @LmlAction void createMatch() {
        connectionController.createMatch();
    }
}
