package com.metaphore.nakamatest.screens.connection;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.crashinvaders.common.ScreenResetTool;
import com.crashinvaders.common.lml.CommonLmlParser;
import com.crashinvaders.common.lml.CommonLmlParserBuilder;
import com.crashinvaders.common.lml.EmptyActorConsumer;
import com.crashinvaders.common.scene2d.skin.SkinX;
import com.crashinvaders.common.viewcontroller.ViewController;
import com.crashinvaders.common.viewcontroller.ViewControllerManager;
import com.github.czyzby.lml.annotation.LmlAction;
import com.github.czyzby.lml.parser.action.ActionContainer;
import com.metaphore.nakamatest.App;
import com.metaphore.nakamatest.AppConstants;
import com.metaphore.nakamatest.common.lml.ProjectLmlSyntax;
import com.metaphore.nakamatest.screens.connection.controllers.*;

public class ConnectionScreen extends ScreenAdapter implements ActionContainer, ScreenResetTool.InstanceProvider {

    private final PolygonSpriteBatch batch;
    private final Stage stage;
    private final SkinX skin;
    private final CommonLmlParser lmlParser;
    private final ViewControllerManager viewControllers;

    private Stack rootView;

    public ConnectionScreen() {
        batch = new PolygonSpriteBatch();
        stage = new Stage(new ExtendViewport(480f, 800f), batch);

        skin = App.inst().getAssets().get("skins/uiskin.json", SkinX.class);
        for (ObjectMap.Entry<String, BitmapFont> entry : skin.getAll(BitmapFont.class)) {
            entry.value.getData().markupEnabled = true;
        }

        lmlParser = (CommonLmlParser) new CommonLmlParserBuilder()
                .syntax(new ProjectLmlSyntax())
                .skin(skin)
                .actions("Global", this)
                .action(":empty", new EmptyActorConsumer())
                .build();

        viewControllers = new ViewControllerManager(stage);

        if (App.inst().getParams().debug) {
            stage.addListener(new DebugInputHandler());
        }
    }

    @Override
    public void show() {
        super.show();

        viewControllers.add(new DataStorageController());
        viewControllers.add(new DeviceUuidViewController(viewControllers, lmlParser));
        viewControllers.add(new BusyIndicatorViewController(viewControllers, lmlParser));
        viewControllers.add(new ToastMessageViewController(viewControllers, lmlParser));
        viewControllers.add(new ListItemSelectPopupController(viewControllers, lmlParser));
        viewControllers.add(new ServerAuthViewController(viewControllers, lmlParser));
        viewControllers.add(new MatchConnectionViewController(viewControllers, lmlParser));
        viewControllers.add(new PreGameLobbyViewController(viewControllers, lmlParser));

        viewControllers.add(new ConnectionStateController(viewControllers));

        Stack screenRootView = (Stack) lmlParser.createView(this,
                Gdx.files.internal("lml/connection/root.lml")).first();
        screenRootView.validate();

        rootView = screenRootView;
        stage.getRoot().addActor(rootView);
        viewControllers.onViewCreated(rootView);

        App.inst().getInput().addProcessor(stage);
    }

    @Override
    public void hide() {
        super.hide();
        App.inst().getInput().removeProcessor(stage);

        viewControllers.dispose();
        stage.clear();
        rootView = null;
    }

    @Override
    public void dispose() {
        super.dispose();
        stage.dispose();
        batch.dispose();
        skin.dispose();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        Color clearColor = AppConstants.clearColor;
        Gdx.gl.glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        viewControllers.update(delta);

        stage.act(delta);
        stage.draw();
    }

    @LmlAction
    public void restartScreen() {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                App.inst().restartCurrentScreen();
            }
        });
    }

    @Override
    public Screen createResetScreenInstance() {
        return new ConnectionScreen();
    }

    public CommonLmlParser getLmlParser() {
        return lmlParser;
    }

    public Stage getStage() {
        return stage;
    }

    public Stack getRootView() {
        return rootView;
    }

    /**
     * @see CommonLmlParser#processLmlFieldAnnotations(Object)
     */
    public <View> void processLmlFields(View view) {
        lmlParser.processLmlFieldAnnotations(view);
    }

    public <T extends ViewController> T getViewController(Class<T> viewControllerType) {
        return (T) viewControllers.get(viewControllerType);
    }

    public ViewControllerManager getViewControllers() {
        return viewControllers;
    }

    private class DebugInputHandler extends InputListener {

        boolean stageDebug = false;

        @Override
        public boolean keyDown(InputEvent event, int keycode) {
            switch (keycode) {
                case Input.Keys.F1: {
                    stage.setDebugAll(stageDebug = !stageDebug);
                    return true;
                }
            }
            return super.keyDown(event, keycode);
        }
    }
}
