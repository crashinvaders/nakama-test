package com.metaphore.nakamatest.screens.chattest;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.ObjectMap;

public class MatchGameWidget extends Group {
    private static final Vector2 tmpVec = new Vector2();

    private final ObjectMap<String, PlayerActor> playerActors = new ObjectMap<>();

    private final Skin skin;
    private final ShapeRenderer shapeRenderer;

    public MatchGameWidget(Skin skin, ShapeRenderer shapeRenderer) {
        this.skin = skin;
        this.shapeRenderer = shapeRenderer;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
//        batch.end();
//        shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());
//        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
//
//        shapeRenderer.setColor(Color.ROYAL);
//        shapeRenderer.rect(getX(), getY(), getWidth(), getHeight());
//
//        shapeRenderer.end();
//        batch.begin();

        super.draw(batch, parentAlpha);
    }

    public PlayerActor getPlayerActor(String playerId) {
        if (playerId == null) {
            throw new IllegalArgumentException("Player ID cannot be null.");
        }

        PlayerActor playerActor = playerActors.get(playerId);
        if (playerActor == null) {
            playerActor = new PlayerActor(playerId);
            addActor(playerActor);
            playerActors.put(playerId, playerActor);
        }
        return playerActor;
    }

    public class PlayerActor extends Group {

        private final String playerId;
        private final BitmapFont font;
        private final GlyphLayout glyphLayout;

        public PlayerActor(String playerId) {
            if (playerId.length() > 8) {
                playerId = playerId.substring(0, 7) + "...";
            }
            this.playerId = playerId;

            // Color
            {
                float r = MathUtils.random(1f);
                float g = MathUtils.random(1f);
                float b = 2f - r - g;
                setColor(r, g, b, 1f);
            }

            font = skin.getFont("small-font");
            glyphLayout = new GlyphLayout();

            updateContent();
        }

        public void updateContent() {
            glyphLayout.setText(font, playerId, getColor(), 0f, 0, false);
        }

        @Override
        public void draw(Batch batch, float parentAlpha) {
            super.draw(batch, parentAlpha);

            batch.end();
            shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());
            shapeRenderer.setTransformMatrix(batch.getTransformMatrix());
            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
            shapeRenderer.setAutoShapeType(true);

            shapeRenderer.set(ShapeRenderer.ShapeType.Filled);
            shapeRenderer.setColor(getColor());
            shapeRenderer.circle(getX(), getY(), 8f);
            shapeRenderer.set(ShapeRenderer.ShapeType.Line);
            shapeRenderer.setColor(getColor());
            shapeRenderer.circle(getX(), getY(), 10f);

            shapeRenderer.end();
            batch.begin();

            font.draw(batch, glyphLayout, getX() + glyphLayout.width*0.5f, getY() + glyphLayout.height*0.5f - 20f);
        }
    }
}
