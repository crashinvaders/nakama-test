package com.metaphore.nakamatest.screens.chattest;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.*;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.heroiclabs.nakama.Error;
import com.heroiclabs.nakama.*;
import com.heroiclabs.nakama.api.ChannelMessage;
import com.heroiclabs.nakama.api.ChannelMessageList;
import com.heroiclabs.nakama.api.NotificationList;
import com.metaphore.nakamatest.App;
import com.crashinvaders.common.ScreenResetTool;

import java.lang.StringBuilder;
import java.nio.ByteBuffer;
import java.util.concurrent.*;

public class NakamaTestScreen extends ScreenAdapter implements ScreenResetTool.InstanceProvider {
    private static final String TAG = NakamaTestScreen.class.getSimpleName();
//    private static final String NAKAMA_HOST = "192.168.86.246";
    private static final String NAKAMA_HOST = "nakama-dev.crashinvaders.com";
    private static final Array<String> userNames = Array.with("metaphore@crashinvaders.com", "testuser@gmail.com");
    private static final int OP_CODE_COLOR = 0;
    private static final int OP_CODE_POSITION = 1;

    private final Executor gdxThreadExecutor;
    private final ExecutorService asyncThreadExecutor;

    private final Stage stage;
    private final ShapeRenderer shapeRenderer;
    private final Skin skin;

    private final MatchGameWidget gameWidget;

    private final Label lblOutput;
    private final ScrollPane scrollOutput;

    private final Group sectionLogin;
    private final Group sectionChat;
    private final Group sectionMatchConnection;

    private final Client nakamaClient;
    private final SocketClient nakamaSocket;
    private final MySocketListener socketListener;
    private Session nakamaSession;
    private Channel chatChannel;
    private Match matchSession;

    public NakamaTestScreen() {
        gdxThreadExecutor = new Executor() {
            @Override
            public void execute(Runnable command) {
                Gdx.app.postRunnable(command);
            }
        };
        asyncThreadExecutor = Executors.newSingleThreadExecutor();

        nakamaClient = new DefaultClient("defaultkey", NAKAMA_HOST, 7349, false);
        nakamaSocket = nakamaClient.createSocket();
        socketListener = new MySocketListener();

        shapeRenderer = new ShapeRenderer();
        skin = new Skin(Gdx.files.internal("skins/uiskin.json"));
        for (ObjectMap.Entry<String, BitmapFont> entry : skin.getAll(BitmapFont.class)) {
            entry.value.getData().markupEnabled = true;
        }

        stage = new Stage(new ExtendViewport(480f, 800f));

        Stack rootStack = new Stack();
        rootStack.setFillParent(true);
        stage.addActor(rootStack);

        // General game content
        {
            // Game widget
            {
                gameWidget = new MatchGameWidget(skin, shapeRenderer);
                gameWidget.addListener(new InputListener() {
                    @Override
                    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                        updatePlayerPosition(x, y);
                        return true;
                    }

                    @Override
                    public void touchDragged(InputEvent event, float x, float y, int pointer) {
                        updatePlayerPosition(x, y);
                    }

                    private void updatePlayerPosition(float x, float y) {
                        if (nakamaSession == null) return;

                        String playerId = nakamaSession.getUsername();
                        gameWidget.getPlayerActor(playerId).setPosition(x, y);

                        sendPlayerPositionToServer();
                    }
                });
                rootStack.add(new Container<>(gameWidget).fill());
            }

            // Match connection widgets
            {
                final TextField edtMatchId = new TextField("", skin);
                edtMatchId.setMessageText("Match ID");
                TextButton btnJoin = new TextButton("Join", skin);
                btnJoin.addListener(new ChangeListener() {
                    @Override
                    public void changed(ChangeEvent event, Actor actor) {
                        String matchId = edtMatchId.getText().trim();
                        joinMatch(matchId);
                    }
                });
                TextButton btnCreate = new TextButton("Create", skin);
                btnCreate.addListener(new ChangeListener() {
                    @Override
                    public void changed(ChangeEvent event, Actor actor) {
                        createMatch();
                    }
                });

                Table table = new Table();
                table.defaults().growX().padBottom(4f);
                table.align(Align.top);
                table.add(edtMatchId);
                table.row();
                table.add(btnJoin);
                table.row();
                table.add(btnCreate);

                rootStack.add(table);
                sectionMatchConnection = table;
            }

            // Chat button
            {
                TextButton btnChat = new TextButton("Chat", skin);
                btnChat.addListener(new ChangeListener() {
                    @Override
                    public void changed(ChangeEvent event, Actor actor) {
                        sectionChat.setVisible(true);
                    }
                });
                rootStack.add(new Container<>(btnChat).align(Align.bottom).fillX());
            }
        }

        // Login
        {
            Table table = new Table();
            table.align(Align.top);
            table.defaults().growX().padBottom(4f);

            final TextField edtEmail = new TextField(userNames.first(), skin);
            edtEmail.setMessageText("Email");
            edtEmail.addListener(new ClickListener(1) {
                int clickCount = 0;
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    String userName = userNames.get(++clickCount % userNames.size);
                    edtEmail.setText(userName);
                }
            });
            table.add(edtEmail);
            table.row();

            final TextField edtPassword = new TextField("bigblackburger", skin);
            edtPassword.setMessageText("Password");
            edtPassword.setPasswordMode(true);
            table.add(edtPassword);
            table.row();

            TextButton btnLogIn = new TextButton("Log In", skin);
            btnLogIn.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    String email = edtEmail.getText();
                    String password = edtPassword.getText();
                    performLogIn(email, password);
                }
            });
            table.add(btnLogIn);

            rootStack.add(table);
            sectionLogin = table;
        }

        // Chat
        {
            Table table = new Table();
            table.align(Align.top);
            table.defaults().growX().padBottom(4f);

            final TextField edtMessage = new TextField("", skin);
            edtMessage.setMessageText("Enter a message");
            edtMessage.addListener(new InputListener() {
                @Override
                public boolean keyDown(InputEvent event, int keycode) {
                    if (keycode != Input.Keys.ENTER) return super.keyDown(event, keycode);

                    String message = edtMessage.getText().trim();
                    if (message.length() == 0) return true;

                    boolean messageSent = sendChatMessage(message);
                    if (messageSent) {
                        edtMessage.setText(null);
                    }
                    return true;
                }
            });
            table.add(edtMessage);
            table.row();

            TextButton btnHide = new TextButton("Hide", skin);
            btnHide.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    sectionChat.setVisible(false);
                }
            });
            table.add(btnHide);
            table.row();

            // Output text.
            {
                lblOutput = new Label("", skin, "small");
                lblOutput.setAlignment(Align.bottomLeft);
                lblOutput.setWrap(true);

                Container container = new Container<>(lblOutput);
                container.setBackground(skin.getDrawable("outputAreaBg"));
                container.fill();
                container.padLeft(8f).padRight(8f);

                scrollOutput = new ScrollPane(container, skin, "clear");
                scrollOutput.setScrollingDisabled(true, false);

                table.add(scrollOutput).growY().padBottom(0f);
            }

            rootStack.add(table);
            sectionChat = table;
        }

        sectionLogin.setVisible(true);
        sectionChat.setVisible(false);
    }

    @Override
    public void dispose() {
        super.dispose();

        asyncThreadExecutor.submit(new Runnable() {
            @Override
            public void run() {
                if (nakamaSocket != null) {
                    nakamaSocket.disconnect();
                }
                if (nakamaClient != null) {
                    nakamaClient.disconnect();
                }
                nakamaSession = null;
            }
        });
        asyncThreadExecutor.shutdown();

//        try {
//            asyncThreadExecutor.awaitTermination(5, TimeUnit.SECONDS);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        stage.dispose();
        skin.dispose();
    }

    @Override
    public void show() {
        super.show();
        App.inst().getInput().addProcessor(stage);
    }

    @Override
    public void hide() {
        super.hide();
        App.inst().getInput().removeProcessor(stage);
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(delta);
        stage.draw();
    }

    @Override
    public Screen createResetScreenInstance() {
        return new NakamaTestScreen();
    }

    private void performLogIn(final String email, final String password) {
        Gdx.app.debug(TAG, "Log In Initiated");

        if (nakamaSession != null) {
            Gdx.app.error(TAG, "Another nakama session is already in use.");
            return;
        }

        asyncThreadExecutor.execute(new Runnable() {
            @Override
            public void run() {
                // Establish Nakama session.
                try {
                    nakamaSession = nakamaClient.authenticateEmail(email, password).get();
//                    nakamaSession = nakamaClient.authenticateDevice("01234567890123456789", false, "metaphore").get();
                    Gdx.app.log(TAG, "Authentication went successful as \""+ nakamaSession.getUsername() + "\"");
                    Gdx.app.log(TAG, "Access token: " + nakamaSession.getAuthToken());
                } catch (InterruptedException | ExecutionException e) {
                    Gdx.app.error(TAG, "Error authenticating.", e);
                    return;
                }

                // Create socket connection.
                try {
                    nakamaSession = nakamaSocket.connect(nakamaSession, socketListener).get();
                    nakamaSocket.updateStatus("Online");
                } catch (InterruptedException | ExecutionException e) {
                    Gdx.app.error(TAG, "Error creating socket.", e);
                    return;
                }

                // Join chat room.
                try {
                    chatChannel = nakamaSocket.joinChat("TestChatRoom", ChannelType.ROOM, true, false).get();
                    Gdx.app.log(TAG, "Now connected to channel id: " + chatChannel.getId());

                    // Retrieve the last pack of messages.
                    ChannelMessageList messages = nakamaClient.listChannelMessages(nakamaSession, chatChannel.getId(), 10, null, false).get();
                    for (int i = messages.getMessagesCount()-1; i >= 0; i--) {
                        socketListener.onChannelMessage(messages.getMessages(i));
                    }

                } catch (InterruptedException | ExecutionException e) {
                    Gdx.app.error(TAG, "Error joining chat.", e);
                    return;
                }

                gdxThreadExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        sectionLogin.setVisible(false);
//                        sectionChat.setVisible(true);
                    }
                });
            }
        });
    }

    private boolean joinMatch(final String matchId) {
        if (nakamaSession == null) {
            Gdx.app.error(TAG, "Nakama session is not initialized.");
            return false;
        }
        if (matchSession != null) {
            Gdx.app.error(TAG, "A match session already established.");
            return false;
        }
        if (matchId.length() == 0) {
            Gdx.app.error(TAG, "Match ID is empty!");
            return false;
        }

        asyncThreadExecutor.submit(new Runnable() {
            @Override
            public void run() {
                // Start matchmaking.
                try {
                    matchSession = nakamaSocket.joinMatch(matchId).get();
                    Gdx.app.log(TAG, "Joined to the match with ID:  " + matchSession.getMatchId());

                    gdxThreadExecutor.execute(new Runnable() {
                        @Override
                        public void run() {
                            sectionMatchConnection.setVisible(false);
                            sendPlayerColorToServer();
                            sendPlayerPositionToServer();
                        }
                    });
                } catch (InterruptedException | ExecutionException e) {
                    Gdx.app.error(TAG, "Error creating a match.", e);
                }
            }
        });

        return true;
    }

    private boolean createMatch() {
        if (nakamaSession == null) {
            Gdx.app.error(TAG, "Nakama session is not initialized.");
            return false;
        }
        if (matchSession != null) {
            Gdx.app.error(TAG, "A match session already established.");
            return false;
        }

        asyncThreadExecutor.submit(new Runnable() {
            @Override
            public void run() {
                // Start matchmaking.
                try {
                    matchSession = nakamaSocket.createMatch().get();
                    Gdx.app.log(TAG, "Match created with ID:  " + matchSession.getMatchId());

                    gdxThreadExecutor.execute(new Runnable() {
                        @Override
                        public void run() {
                            sectionMatchConnection.setVisible(false);
                            sendPlayerColorToServer();
                            sendPlayerPositionToServer();
                        }
                    });
                } catch (InterruptedException | ExecutionException e) {
                    Gdx.app.error(TAG, "Error creating a match.", e);
                }
            }
        });

        return true;
    }

    private boolean sendChatMessage(final String message) {
        if (nakamaSession == null) {
            Gdx.app.error(TAG, "Nakama session is not initialized.");
            return false;
        }
        if (chatChannel == null) {
            Gdx.app.error(TAG, "Not joined to chat channel!");
            return false;
        }

        asyncThreadExecutor.execute(new Runnable() {
            @Override
            public void run() {
                String composedMessage = "{\"message\":\"" + message + "\"}";
                nakamaSocket.writeChatMessage(chatChannel.getId(), composedMessage);
            }
        });

        return true;
    }

    private void sendPlayerPositionToServer() {
        if (matchSession == null) return;

        String playerId = nakamaSession.getUsername();
        MatchGameWidget.PlayerActor playerActor = gameWidget.getPlayerActor(playerId);

//        String dataString = "{x=\"" + playerActor.getX() + "\",y=\"" + playerActor.getY() + "\"}";
//        byte[] data = dataString.getBytes(Charset.forName("UTF-8"));

        byte[] data = ByteBuffer.allocate(8)
                .putFloat(playerActor.getX())
                .putFloat(playerActor.getY())
                .array();

        nakamaSocket.sendMatchData(matchSession.getMatchId(), OP_CODE_POSITION, data);
    }

    private void sendPlayerColorToServer() {
        if (matchSession == null) return;

        String playerId = nakamaSession.getUsername();
        MatchGameWidget.PlayerActor playerActor = gameWidget.getPlayerActor(playerId);

        int colorEncoded = Color.rgba8888(playerActor.getColor());
        byte[] data = ByteBuffer.allocate(4).putInt(colorEncoded).array();

        nakamaSocket.sendMatchData(matchSession.getMatchId(), OP_CODE_COLOR, data);
    }

    private void appendOutputText(String message) {
        StringBuilder sb = new StringBuilder();
        sb.append(lblOutput.getText());
        if (sb.length() > 0) {
            sb.append('\n');
        }
        sb.append(message);

        lblOutput.setText(sb.toString());
        scrollOutput.setScrollPercentY(1f);
    }

    private class MySocketListener implements SocketListener {
        private final String TAG = MySocketListener.class.getSimpleName();
        private final JsonReader jsonReader = new JsonReader();

        @Override
        public void onDisconnect(Throwable throwable) {
            if (throwable != null) {
                Gdx.app.error(TAG, "MySocketListener.onDisconnect", throwable);
            } else {
                Gdx.app.log(TAG, "MySocketListener.onDisconnect");
            }
        }

        @Override
        public void onError(Error error) {
            Gdx.app.error(TAG, "MySocketListener.onError", error);
        }

        @Override
        public void onChannelMessage(ChannelMessage message) {
            Gdx.app.log(TAG, String.format("Received a message on channel %s", message.getChannelId()));

            final String username = message.getUsername();
            JsonValue jsonContent = jsonReader.parse(message.getContent());
            final String messageText = jsonContent.getString("message");

            gdxThreadExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    appendOutputText("[#ff88ff]" + username + ": [#d0d0d0]" + messageText);
                }
            });
        }

        @Override
        public void onChannelPresence(ChannelPresenceEvent channelPresenceEvent) {
            Gdx.app.log(TAG, "MySocketListener.onChannelPresence");
        }

        @Override
        public void onMatchmakerMatched(MatchmakerMatched matchmakerMatched) {
            Gdx.app.log(TAG, "MySocketListener.onMatchmakerMatched");
        }

        @Override
        public void onMatchData(MatchData matchData) {
            Gdx.app.log(TAG, "MySocketListener.onMatchData");

            //FIXME For some reason matchData.getUserPresence() is always null.
            // Check https://github.com/heroiclabs/nakama-java/issues/25 for solution.
//            final String username = matchData.getUserPresence().getUsername();
            final String username = "enemy";
            final byte[] data = matchData.getData();

            long opCode = matchData.getOpCode();
            if (opCode == OP_CODE_COLOR) {
                gdxThreadExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        int colorEncoded = ByteBuffer.wrap(data).getInt();
                        MatchGameWidget.PlayerActor playerActor = gameWidget.getPlayerActor(username);
                        playerActor.getColor().set(colorEncoded);
                        playerActor.updateContent();
                    }
                });

            } else if (opCode == OP_CODE_POSITION) {
                gdxThreadExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
                        float x = byteBuffer.getFloat();
                        float y = byteBuffer.getFloat();
                        MatchGameWidget.PlayerActor playerActor = gameWidget.getPlayerActor(username);
                        playerActor.setPosition(x, y);
                    }
                });
            }
        }

        @Override
        public void onMatchPresence(MatchPresenceEvent matchPresenceEvent) {
            Gdx.app.log(TAG, "MySocketListener.onMatchPresence");

            gdxThreadExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    sendPlayerColorToServer();
                    sendPlayerPositionToServer();
                }
            });
        }

        @Override
        public void onNotifications(NotificationList notificationList) {
            Gdx.app.log(TAG, "MySocketListener.onNotifications");
        }

        @Override
        public void onStatusPresence(StatusPresenceEvent statusPresenceEvent) {
            Gdx.app.log(TAG, "MySocketListener.onStatusPresence");
        }

        @Override
        public void onStreamPresence(StreamPresenceEvent streamPresenceEvent) {
            Gdx.app.log(TAG, "MySocketListener.onStreamPresence");
        }

        @Override
        public void onStreamData(StreamData streamData) {
            Gdx.app.log(TAG, "MySocketListener.onStreamData");
        }
    }
}
