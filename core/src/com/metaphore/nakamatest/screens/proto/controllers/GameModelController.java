package com.metaphore.nakamatest.screens.proto.controllers;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.crashinvaders.common.viewcontroller.ViewController;
import com.metaphore.nakamatest.App;
import com.metaphore.nakamatest.screens.proto.model.GameModel;

public class GameModelController implements ViewController {

    private final GameModel model = new GameModel();

    @Override
    public void onViewCreated(Group sceneRoot) {
        GameModel.GameData data = new GameModel.GameData();
        data.randomSeed = MathUtils.random(Integer.MAX_VALUE - 1);
        data.heroAmount = 3;
        data.worldWidth = 12;
        data.worldHeight = 12;

        model.initializeGameEntities(data);
        model.setEventManager(App.inst().getEvents());
    }

    @Override
    public void dispose() {
        model.setEventManager(null);
        model.reset();
    }

    @Override
    public void update(float delta) {

    }

    public GameModel getModel() {
        return model;
    }
}
