package com.metaphore.nakamatest.screens.proto.controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.ArrayMap;
import com.crashinvaders.common.lml.CommonLmlParser;
import com.crashinvaders.common.lml.LmlUtils;
import com.crashinvaders.common.lml.SimpleLmlView;
import com.crashinvaders.common.viewcontroller.LmlViewController;
import com.crashinvaders.common.viewcontroller.ViewControllerManager;
import com.github.czyzby.lml.annotation.LmlActor;
import com.metaphore.nakamatest.screens.proto.model.GroundTileModel;
import com.metaphore.nakamatest.screens.proto.model.GameModel;

public class GroundTileViewsController extends LmlViewController {
    private static final int FIELD_SIZE = 12;
    private static final float TILE_SIZE = 64f;

    @LmlActor Group groupGroundTiles;

    private final ArrayMap<GroundTileModel, GroundTileView> groundTiles = new ArrayMap<>();

    public GroundTileViewsController(ViewControllerManager viewControllers, CommonLmlParser lmlParser) {
        super(viewControllers, lmlParser);
    }

    @Override
    public void onViewCreated(Group sceneRoot) {
        super.onViewCreated(sceneRoot);
        processLmlFields(this);

        GameModel gameModel = getController(GameModelController.class).getModel();

        for (GroundTileModel model : gameModel.getGroundTiles()) {
            GroundTileView view = new GroundTileView(lmlParser, model, TILE_SIZE);
            groundTiles.put(model, view);
            groupGroundTiles.addActor(view.viewRoot);
        }
    }

    public float getTileSize() {
        return TILE_SIZE;
    }

    public int getFieldWidth() {
        return FIELD_SIZE;
    }

    public int getFieldHeight() {
        return FIELD_SIZE;
    }

    public static class GroundTileView extends SimpleLmlView {

        final Group viewRoot;
        final GroundTileModel model;

        public GroundTileView(CommonLmlParser lmlParser, GroundTileModel model, float tileSize) {
            this.model = model;

            viewRoot = LmlUtils.parseLmlTemplate(lmlParser, this, Gdx.files.internal("lml/proto/entities/ground-tile.lml"));
            viewRoot.setBounds(model.getX() * tileSize, model.getY() * tileSize, tileSize, tileSize);
        }
    }
}
