package com.metaphore.nakamatest.screens.proto.controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;
import com.crashinvaders.common.eventmanager.EventHandler;
import com.crashinvaders.common.eventmanager.EventInfo;
import com.crashinvaders.common.eventmanager.EventParams;
import com.crashinvaders.common.viewcontroller.ViewController;
import com.crashinvaders.common.viewcontroller.ViewControllerManager;
import com.metaphore.nakamatest.App;
import com.metaphore.nakamatest.screens.proto.events.TurnFinishedEvent;
import com.metaphore.nakamatest.screens.proto.model.GameModel;
import com.metaphore.nakamatest.screens.proto.model.HeroModel;

public class GameLogicController implements ViewController, EventHandler {
    private static final String TAG = GameLogicController.class.getSimpleName();

    private final IntMap<HeroAction> pendingHeroActions = new IntMap<>();
    private final ViewControllerManager controllers;
    private final GameModel gameModel;

    private final Runnable performTurnUpdateRunnable = new Runnable() {
        @Override
        public void run() {
            performTurnUpdate();
        }
    };

    private TurnTimerController turnTimer;
    private boolean turnUpdating = false;

    public GameLogicController(ViewControllerManager controllers) {
        gameModel = controllers.get(GameModelController.class).getModel();
        this.controllers = controllers;
    }

    @Override
    public void onViewCreated(Group sceneRoot) {
        turnTimer = controllers.get(TurnTimerController.class);

        App.inst().getEvents().addHandler(this, TurnFinishedEvent.class);

        // Start first turn on next cycle.
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                turnTimer.startNextTurn();
            }
        });
    }

    @Override
    public void dispose() {
        App.inst().getEvents().removeHandler(this);

        turnTimer = null;
    }

    @Override
    public void update(float delta) {
        // Do nothing.
    }

    @Override
    public void handle(EventInfo event, EventParams params) {
        if (event instanceof TurnFinishedEvent) {
            Gdx.app.postRunnable(performTurnUpdateRunnable);
        }
    }

    public void setPendingHeroAction(int playerId, HeroAction heroAction) {
        if (turnUpdating) {
            throw new IllegalStateException("A turn update is currently in a process.");
        }

        pendingHeroActions.put(playerId, heroAction);
        App.inst().getEvents().dispatchEvent(Pools.obtain(PendingHeroActionChangedEvent.class)
                .init(playerId, heroAction));
    }

    private void performTurnUpdate() {
        if (turnUpdating) {
            throw new IllegalStateException("A turn update is currently in a process.");
        }
        turnUpdating = true;

        //TODO Update bombs.

        Array<HeroModel> heroes = gameModel.getHeroes();
        for (int i = 0; i < heroes.size; i++) {
            int playerId = heroes.get(i).getPlayerId();
            HeroAction heroAction = pendingHeroActions.get(playerId);
            if (heroAction == null) {
                //TODO No action? PUNISH HIM!
            } else {
                performHeroAction(playerId, heroAction);
                App.inst().getEvents().dispatchEvent(Pools.obtain(PendingHeroActionChangedEvent.class)
                        .init(playerId, null));
            }
        }

        pendingHeroActions.clear();

        turnUpdating = false;

        // Start next turn timer.
        turnTimer.startNextTurn();
    }

    private void performHeroAction(int playerId, HeroAction heroAction) {
        if (heroAction == null) {
            throw new IllegalArgumentException("Hero action cannot be null.");
        }

        HeroModel hero = gameModel.getHero(playerId);
        if (hero == null) {
            throw new IllegalArgumentException("There is no hero with player ID: " + playerId);
        }

        int playerPositionX = hero.getX();
        int playerPositionY = hero.getY();

        switch (heroAction) {
            case MOVE_N:
                playerPositionY++;
                break;
            case MOVE_S:
                playerPositionY--;
                break;
            case MOVE_W:
                playerPositionX--;
                break;
            case MOVE_E:
                playerPositionX++;
                break;
            default:
                Gdx.app.error(TAG, "Unexpected user action: " + heroAction);
        }

        //TODO Add action sanity check (collisions etc).
        playerPositionX = MathUtils.clamp(playerPositionX, 0, gameModel.getWorldWidth() - 1);
        playerPositionY = MathUtils.clamp(playerPositionY, 0, gameModel.getWorldHeight() - 1);

        hero.setPosition(playerPositionX, playerPositionY);
    }

    public enum HeroAction {
        MOVE_N,
        MOVE_S,
        MOVE_W,
        MOVE_E,
//        PUT_BOMB_N,
//        PUT_BOMB_S,
//        PUT_BOMB_W,
//        PUT_BOMB_E,
    }

    public static class PendingHeroActionChangedEvent implements EventInfo, Pool.Poolable {

        private int playerId = -1;
        private HeroAction heroAction = null;

        private PendingHeroActionChangedEvent() { }

        PendingHeroActionChangedEvent init(int playerId, HeroAction heroAction) {
            this.playerId = playerId;
            this.heroAction = heroAction;
            return this;
        }

        @Override
        public void reset() {
            playerId = -1;
            heroAction = null;
        }

        public int getPlayerId() {
            return playerId;
        }

        public HeroAction getHeroAction() {
            return heroAction;
        }
    }
}
