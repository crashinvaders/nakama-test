package com.metaphore.nakamatest.screens.proto.controllers;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.Disposable;
import com.crashinvaders.common.CommonUtils;
import com.crashinvaders.common.eventmanager.EventHandler;
import com.crashinvaders.common.eventmanager.EventInfo;
import com.crashinvaders.common.eventmanager.EventParams;
import com.crashinvaders.common.lml.CommonLmlParser;
import com.crashinvaders.common.viewcontroller.LmlViewController;
import com.crashinvaders.common.viewcontroller.ViewControllerManager;
import com.metaphore.nakamatest.App;
import com.metaphore.nakamatest.screens.proto.events.TurnStartedEvent;
import com.metaphore.nakamatest.screens.proto.model.GameModel;
import com.metaphore.nakamatest.screens.proto.model.HeroModel;

import static com.metaphore.nakamatest.screens.proto.controllers.GameLogicController.HeroAction;

public class BotHeroController extends LmlViewController implements EventHandler {
    private static final String TAG = BotHeroController.class.getSimpleName();

    private final ArrayMap<HeroModel, BotHeroBehavior> botBehaviors = new ArrayMap<>();
    private final GameLogicController gameLogicController;
    private final GameModel gameModel;

    public BotHeroController(ViewControllerManager viewControllers, CommonLmlParser lmlParser) {
        super(viewControllers, lmlParser);

        gameLogicController = getController(GameLogicController.class);
        gameModel = getController(GameModelController.class).getModel();
    }

    @Override
    public void onViewCreated(Group sceneRoot) {
        super.onViewCreated(sceneRoot);


        int localPlayerHeroId = -1;
        PlayerHeroInputController playerInputController = getController(PlayerHeroInputController.class);
        // PlayerHeroInputController is a dynamic controller and thus may not be created at the time.
        if (playerInputController != null) {
            localPlayerHeroId = playerInputController.getPlayerId();
        }

        for (HeroModel heroModel : gameModel.getHeroes()) {
            if (heroModel.getPlayerId() != localPlayerHeroId) {
                createBotBehavior(heroModel.getPlayerId());
            }
        }

        App.inst().getEvents().addHandler(this, TurnStartedEvent.class);
    }

    @Override
    public void dispose() {
        super.dispose();

        for (int i = 0; i < botBehaviors.size; i++) {
            BotHeroBehavior botBehavior = botBehaviors.getValueAt(i);
            botBehavior.dispose();
        }
        botBehaviors.clear();

        App.inst().getEvents().removeHandler(this);
    }

    @Override
    public void handle(EventInfo event, EventParams params) {
        if (event instanceof TurnStartedEvent) {
            performBotActions();
        }
    }

    public void createBotBehavior(int playerId) {
        HeroModel heroModel = gameModel.getHero(playerId);

        if (botBehaviors.get(heroModel) != null) return;

        BotHeroBehavior botBehavior = new BotHeroBehavior(gameModel, heroModel);
        botBehaviors.put(heroModel, botBehavior);
    }

    public void removeBotBehavior(int playerId) {
        HeroModel heroModel = gameModel.getHero(playerId);

        BotHeroBehavior botBehavior = botBehaviors.removeKey(heroModel);
        if (botBehavior == null) return;

        botBehavior.dispose();
    }

    private void performBotActions() {
        for (int i = 0; i < botBehaviors.size; i++) {
            BotHeroBehavior botBehavior = botBehaviors.getValueAt(i);
            HeroAction heroAction = botBehavior.getNextUserAction();
            gameLogicController.setPendingHeroAction(botBehavior.heroModel.getPlayerId(), heroAction);
        }
    }

    private static class BotHeroBehavior implements Disposable {

        private final GameModel gameModel;
        private final HeroModel heroModel;

        public BotHeroBehavior(GameModel gameModel, HeroModel heroModel) {
            this.gameModel = gameModel;
            this.heroModel = heroModel;
        }

        public HeroAction getNextUserAction() {
            // Dummy behavior. Picks random action.
            return CommonUtils.random(HeroAction.values());
        }

        @Override
        public void dispose() {

        }
    }
}