package com.metaphore.nakamatest.screens.proto.controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.ArrayMap;
import com.crashinvaders.common.eventmanager.EventHandler;
import com.crashinvaders.common.eventmanager.EventInfo;
import com.crashinvaders.common.eventmanager.EventParams;
import com.crashinvaders.common.lml.CommonLmlParser;
import com.crashinvaders.common.lml.LmlUtils;
import com.crashinvaders.common.lml.SimpleLmlView;
import com.crashinvaders.common.viewcontroller.LmlViewController;
import com.crashinvaders.common.viewcontroller.ViewControllerManager;
import com.github.czyzby.lml.annotation.LmlActor;
import com.github.czyzby.lml.parser.LmlParser;
import com.metaphore.nakamatest.App;
import com.metaphore.nakamatest.screens.proto.model.GameModel;
import com.metaphore.nakamatest.screens.proto.model.HeroModel;

public class HeroViewsController extends LmlViewController implements EventHandler {

    @LmlActor Label lblPendingUserAction;
    @LmlActor Group groupHeroes;

    private final ArrayMap<HeroModel, HeroView> heroes = new ArrayMap<>();

    public HeroViewsController(ViewControllerManager viewControllers, CommonLmlParser lmlParser) {
        super(viewControllers, lmlParser);
    }

    @Override
    public void onViewCreated(Group sceneRoot) {
        super.onViewCreated(sceneRoot);
        processLmlFields(this);

        float tileSize = getController(GroundTileViewsController.class).getTileSize();
        GameModel gameModel = getController(GameModelController.class).getModel();

        for (HeroModel model : gameModel.getHeroes()) {
            HeroView view = new HeroView(lmlParser, model, tileSize);
            heroes.put(model, view);
            groupHeroes.addActor(view.viewRoot);
        }

        App.inst().getEvents().addHandler(this, HeroModel.ChangeEvent.class);
    }

    @Override
    public void dispose() {
        super.dispose();

        App.inst().getEvents().removeHandler(this);
    }

    @Override
    public void handle(EventInfo event, EventParams params) {
        if (event instanceof HeroModel.ChangeEvent) {
            HeroModel.ChangeEvent e = (HeroModel.ChangeEvent) event;
            HeroView view = heroes.get(e.getModel());
            switch (e.getProperty()) {
                case POSITION:
                    view.updatePositionFromModel(true);
                    break;
            }
        }
    }

    public static class HeroView extends SimpleLmlView {

        private final Group viewRoot;
        private final HeroModel heroModel;
        private final float tileSize;
        private int fieldX;
        private int fieldY;

        public HeroView(LmlParser lmlParser, HeroModel heroModel, float tileSize) {
            lmlParser.getData().addArgument("playerId", heroModel.getPlayerId());
            viewRoot = LmlUtils.parseLmlTemplate(lmlParser, this, Gdx.files.internal("lml/proto/entities/hero.lml"));

            this.heroModel = heroModel;
            this.tileSize = tileSize;
            viewRoot.setSize(tileSize, tileSize);
            updatePositionFromModel(false);
        }

        public Group getViewRoot() {
            return viewRoot;
        }

        public void updatePositionFromModel(boolean animate) {
            if (fieldX == heroModel.getX() && fieldY == heroModel.getY()) return;

            fieldX = heroModel.getX();
            fieldY = heroModel.getY();

            float x = fieldX * tileSize;
            float y = fieldY * tileSize;

            if (animate) {
                viewRoot.clearActions();
                viewRoot.addAction(Actions.moveTo(x, y, 0.25f, Interpolation.pow3Out));
            } else {
                viewRoot.setPosition(x, y);
            }
        }

        public int getFieldX() {
            return fieldX;
        }

        public int getFieldY() {
            return fieldY;
        }
    }
}