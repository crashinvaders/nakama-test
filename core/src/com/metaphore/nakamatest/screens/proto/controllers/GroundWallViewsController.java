package com.metaphore.nakamatest.screens.proto.controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.ArrayMap;
import com.crashinvaders.common.lml.CommonLmlParser;
import com.crashinvaders.common.lml.LmlUtils;
import com.crashinvaders.common.lml.SimpleLmlView;
import com.crashinvaders.common.viewcontroller.LmlViewController;
import com.crashinvaders.common.viewcontroller.ViewControllerManager;
import com.github.czyzby.lml.annotation.LmlActor;
import com.metaphore.nakamatest.screens.proto.model.GameModel;
import com.metaphore.nakamatest.screens.proto.model.GroundWallModel;

public class GroundWallViewsController extends LmlViewController {

    @LmlActor Group groupGroundWalls;

    private final ArrayMap<GroundWallModel, GroundWallView> groundWalls = new ArrayMap<>();

    public GroundWallViewsController(ViewControllerManager viewControllers, CommonLmlParser lmlParser) {
        super(viewControllers, lmlParser);
    }

    @Override
    public void onViewCreated(Group sceneRoot) {
        super.onViewCreated(sceneRoot);
        processLmlFields(this);

        GameModel gameModel = getController(GameModelController.class).getModel();
        float tileSize = getController(GroundTileViewsController.class).getTileSize();

        for (GroundWallModel model : gameModel.getGroundWalls()) {
            GroundWallView view = new GroundWallView(lmlParser, model, tileSize);
            groundWalls.put(model, view);
            groupGroundWalls.addActor(view.viewRoot);
        }
    }

    public static class GroundWallView extends SimpleLmlView {

        final Group viewRoot;
        final GroundWallModel model;

        public GroundWallView(CommonLmlParser lmlParser, GroundWallModel model, float tileSize) {
            this.model = model;

            viewRoot = LmlUtils.parseLmlTemplate(lmlParser, this, Gdx.files.internal("lml/proto/entities/ground-wall.lml"));
            viewRoot.setBounds(model.getX() * tileSize, model.getY() * tileSize, tileSize, tileSize);
        }
    }
}
