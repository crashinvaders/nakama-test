package com.metaphore.nakamatest.screens.proto.controllers;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.crashinvaders.common.lml.CommonLmlParser;
import com.crashinvaders.common.viewcontroller.LmlViewController;
import com.crashinvaders.common.viewcontroller.ViewControllerManager;
import com.metaphore.nakamatest.common.scene2d.DoubleClickListener;

import static com.metaphore.nakamatest.screens.proto.controllers.GameLogicController.HeroAction;

public class PlayerHeroInputController extends LmlViewController {
    private static final String TAG = PlayerHeroInputController.class.getSimpleName();
    private static final Vector2 tmpVec = new Vector2();

    private final int playerId;

    private EventListener swipeListener;
    private EventListener doubleClickListener;

    public PlayerHeroInputController(ViewControllerManager viewControllers, CommonLmlParser lmlParser, int playerId) {
        super(viewControllers, lmlParser);
        this.playerId = playerId;
    }

    @Override
    public void onViewCreated(Group sceneRoot) {
        super.onViewCreated(sceneRoot);
        processLmlFields(this);

        stage.addListener(swipeListener = new ActorGestureListener() {
            @Override
            public void fling(InputEvent event, float velocityX, float velocityY, int button) {
                super.fling(event, velocityX, velocityY, button);

                Vector2 velocity = tmpVec.set(velocityX, velocityY);
                if (velocity.len() < 600f) return;

                HeroAction heroAction;

                float angle = velocity.angle();
                if (angle < 45f || angle > 315f) {
                    heroAction = HeroAction.MOVE_E;
                } else if (angle < 135f) {
                    heroAction = HeroAction.MOVE_N;
                } else if (angle < 225f) {
                    heroAction = HeroAction.MOVE_W;
                } else {
                    heroAction = HeroAction.MOVE_S;
                }

                setPendingHeroAction(heroAction);
            }
        });

        stage.addListener(doubleClickListener = new DoubleClickListener() {
            @Override
            protected void onDoubleClick(float x, float y) {
                // Cancel pending hero action.
                setPendingHeroAction(null);
            }
        });

        getController(BotHeroController.class).removeBotBehavior(playerId);
    }

    @Override
    public void dispose() {
        super.dispose();
        stage.removeListener(swipeListener);
        stage.removeListener(doubleClickListener);

//        getController(BotHeroController.class).createBotBehavior(playerId);
    }

    public int getPlayerId() {
        return playerId;
    }

    private void setPendingHeroAction(HeroAction heroAction) {
        getController(GameLogicController.class).setPendingHeroAction(playerId, heroAction);
    }
}