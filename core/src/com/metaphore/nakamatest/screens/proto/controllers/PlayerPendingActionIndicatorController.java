package com.metaphore.nakamatest.screens.proto.controllers;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.crashinvaders.common.eventmanager.EventHandler;
import com.crashinvaders.common.eventmanager.EventInfo;
import com.crashinvaders.common.eventmanager.EventParams;
import com.crashinvaders.common.lml.CommonLmlParser;
import com.crashinvaders.common.viewcontroller.LmlViewController;
import com.crashinvaders.common.viewcontroller.ViewControllerManager;
import com.github.czyzby.lml.annotation.LmlActor;
import com.metaphore.nakamatest.App;
import com.metaphore.nakamatest.screens.proto.controllers.GameLogicController.PendingHeroActionChangedEvent;

import static com.metaphore.nakamatest.screens.proto.controllers.GameLogicController.HeroAction;

public class PlayerPendingActionIndicatorController extends LmlViewController implements EventHandler {

    private final int playerId;

    @LmlActor Label lblPendingUserAction;

    private HeroAction pendingHeroAction = null;

    public PlayerPendingActionIndicatorController(ViewControllerManager viewControllers, CommonLmlParser lmlParser, int playerId) {
        super(viewControllers, lmlParser);
        this.playerId = playerId;
    }

    @Override
    public void onViewCreated(Group sceneRoot) {
        super.onViewCreated(sceneRoot);
        processLmlFields(this);

        lblPendingUserAction.setVisible(true);

        App.inst().getEvents().addHandler(this, PendingHeroActionChangedEvent.class);
    }

    @Override
    public void dispose() {
        super.dispose();

        lblPendingUserAction.setVisible(false);

        App.inst().getEvents().removeHandler(this);
    }

    @Override
    public void handle(EventInfo event, EventParams params) {
        if (event instanceof PendingHeroActionChangedEvent) {
            PendingHeroActionChangedEvent e = (PendingHeroActionChangedEvent) event;
            if (playerId == e.getPlayerId()) {
                setPendingHeroAction(e.getHeroAction());
            }
        }
    }

    public int getPlayerId() {
        return playerId;
    }

    private void setPendingHeroAction(HeroAction userAction) {
        this.pendingHeroAction = userAction;

        lblPendingUserAction.setText(userAction == null ? "" : userAction.toString());
    }
}