package com.metaphore.nakamatest.screens.proto.controllers;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Align;
import com.crashinvaders.common.lml.CommonLmlParser;
import com.crashinvaders.common.scene2d.RadialDrawable;
import com.crashinvaders.common.scene2d.actions.ActionsExt;
import com.crashinvaders.common.viewcontroller.LmlViewController;
import com.crashinvaders.common.viewcontroller.ViewControllerManager;
import com.github.czyzby.lml.annotation.LmlAction;
import com.github.czyzby.lml.annotation.LmlActor;
import com.metaphore.nakamatest.App;
import com.metaphore.nakamatest.screens.proto.common.TurnManager;
import com.metaphore.nakamatest.screens.proto.events.TurnFinishedEvent;
import com.metaphore.nakamatest.screens.proto.events.TurnStartedEvent;

public class TurnTimerController extends LmlViewController implements TurnManager.Listener {
    private static final float TURN_DURATION = 2f;

    @LmlActor Group groupTurnIndicator;
    @LmlActor Image imgTurnIndicatorFull;

    private final com.metaphore.nakamatest.screens.proto.common.TurnManager turnManager;

    private RadialDrawable timerDrawable;

    public TurnTimerController(ViewControllerManager viewControllers, CommonLmlParser lmlParser) {
        super(viewControllers, lmlParser);

        turnManager = new TurnManager(TURN_DURATION);
        turnManager.setListener(this);
    }

    @Override
    public void onViewCreated(Group sceneRoot) {
        super.onViewCreated(sceneRoot);
        processLmlFields(this);

        timerDrawable = (RadialDrawable) imgTurnIndicatorFull.getDrawable();
    }

    @Override
    public void update(float delta) {
        super.update(delta);
        turnManager.update(delta);

        timerDrawable.setAngle(turnManager.getTurnProgress() * 360f);
    }

    @Override
    public void onTurnStarted(int turnNumber) {
        App.inst().getEvents().dispatchEvent(new TurnStartedEvent());

        groupTurnIndicator.addAction(Actions.sequence(
                ActionsExt.transform(true),
                ActionsExt.origin(Align.center),
                Actions.scaleTo(1.3f, 1.3f),
                Actions.scaleTo(1.0f, 1.0f, 0.4f, Interpolation.bounceOut),
                ActionsExt.transform(false)
        ));
    }

    @Override
    public void onTurnFinished(int turnNumber) {
        App.inst().getEvents().dispatchEvent(new TurnFinishedEvent());
    }

    @LmlAction Drawable createTurnIndicatorDrawable() {
        return new RadialDrawable(skin.getRegion("turn-timer-full"));
    }

    public void startNextTurn() {
        turnManager.startNextTurn();
    }
}
