package com.metaphore.nakamatest.screens.proto.model;

import com.crashinvaders.common.eventmanager.EventInfo;
import com.metaphore.nakamatest.common.ModelObject;

public class HeroModel extends ModelObject {

    private final int playerId;
    private int x = 0;
    private int y = 0;

    public HeroModel(int playerId) {
        this.playerId = playerId;
    }

    public HeroModel(int playerId, int x, int y) {
        this.playerId = playerId;
        this.x = x;
        this.y = y;
    }

    public int getPlayerId() {
        return playerId;
    }

    public void setPosition(int x, int y) {
        this.x = x;
        this.y = y;

        if (eventManager != null) {
            eventManager.dispatchEvent(new ChangeEvent(this, ChangeEvent.Property.POSITION));
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public static class ChangeEvent implements EventInfo {

        private final HeroModel model;
        private final Property property;

        ChangeEvent(HeroModel model, Property property) {
            this.model = model;
            this.property = property;
        }

        public HeroModel getModel() {
            return model;
        }

        public Property getProperty() {
            return property;
        }

        public enum Property {
            POSITION,
        }
    }
}
