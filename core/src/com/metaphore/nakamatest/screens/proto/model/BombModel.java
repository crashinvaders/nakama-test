package com.metaphore.nakamatest.screens.proto.model;

import com.crashinvaders.common.eventmanager.EventInfo;
import com.metaphore.nakamatest.common.ModelObject;

public class BombModel extends ModelObject {

    private int x;
    private int y;
    private int ticksTotal;
    private int ticksLeft;

    public BombModel(int x, int y, int ticksTotal) {
        this.x = x;
        this.y = y;
        this.ticksTotal = ticksTotal;
        this.ticksLeft = ticksTotal;
    }

    public void setPosition(int x, int y) {
        this.x = x;
        this.y = y;

        if (eventManager != null) {
            eventManager.dispatchEvent(new ChangeEvent(this, ChangeEvent.Property.POSITION));
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void reduceTicksLeft() {
        if (ticksLeft <= 0) return;

        ticksLeft--;

        if (eventManager != null) {
            eventManager.dispatchEvent(new ChangeEvent(this, ChangeEvent.Property.TICKS_LEFT));
        }
    }

    public static class ChangeEvent implements EventInfo {

        private final BombModel model;
        private final Property property;

        ChangeEvent(BombModel model, Property property) {
            this.model = model;
            this.property = property;
        }

        public BombModel getModel() {
            return model;
        }

        public Property getProperty() {
            return property;
        }

        public enum Property {
            POSITION,
            TICKS_LEFT,
        }
    }
}
