package com.metaphore.nakamatest.screens.proto.model;

import com.metaphore.nakamatest.common.ModelObject;

public class CoinModel extends ModelObject {

    private final int x;
    private final int y;

    public CoinModel(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
