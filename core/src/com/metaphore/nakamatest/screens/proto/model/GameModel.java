package com.metaphore.nakamatest.screens.proto.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.crashinvaders.common.eventmanager.EventManager;
import com.metaphore.nakamatest.common.ModelObject;

import java.util.Random;

public class GameModel extends ModelObject {
    private static final String TAG = GameModel.class.getSimpleName();

    private final Array<GroundTileModel> groundTiles = new Array<>(128);
    private final Array<GroundWallModel> groundWalls = new Array<>(64);
    private final Array<HeroModel> heroes = new Array<>(8);
    private final Array<BombModel> bombs = new Array<>(32);
    private final Array<CoinModel> coins = new Array<>(32);

    private int worldWidth = -1;
    private int worldHeight = -1;
    private Random random = null;

    public GameModel() {

    }

    public void initializeGameEntities(GameData gameData) {
        this.reset();

        this.worldWidth = gameData.worldWidth;
        this.worldHeight = gameData.worldHeight;
        random = new Random(gameData.randomSeed);

        // Ground tile initialization.
        for (int x = 0; x < worldWidth; x++) {
            for (int y = 0; y < worldHeight; y++) {
                GroundTileModel tileModel = new GroundTileModel(x, y);
                groundTiles.add(tileModel);
                tileModel.setEventManager(eventManager);
            }
        }

        // Hero initialization.
        for (int i = 0; i < gameData.heroAmount; i++) {
            // Find random position.
            GroundTileModel groundTileModel = getRandomEmptyTile(random);

            HeroModel heroModel = new HeroModel(i);
            heroes.add(heroModel);
            heroModel.setPosition(groundTileModel.getX(), groundTileModel.getY());
            heroModel.setEventManager(eventManager);
        }

        // Wall initialization.
        int wallAmount = (int)(worldWidth * worldHeight * 0.20f);    // Walls are 20% of total world space.
        for (int i = 0; i < wallAmount; i++) {
            // Find random position.
            GroundTileModel groundTileModel = getRandomEmptyTile(random);

            GroundWallModel groundWallModel = new GroundWallModel(groundTileModel.getX(), groundTileModel.getY());
            groundWalls.add(groundWallModel);
            groundWallModel.setEventManager(eventManager);
        }

        // Coin initialization.
        int coinAmount = (int)(worldWidth * worldHeight * 0.10f);    // Coins are 10% of total world space.
        for (int i = 0; i < coinAmount; i++) {
            // Find random position.
            GroundTileModel groundTileModel = getRandomEmptyTile(random);

            CoinModel coinModel = new CoinModel(groundTileModel.getX(), groundTileModel.getY());
            coins.add(coinModel);
            coinModel.setEventManager(eventManager);
        }
    }

    public void reset() {
        worldWidth = -1;
        worldHeight = -1;
        random = null;

        ModelObject.setEventManager(groundTiles, null);
        groundTiles.clear();
        ModelObject.setEventManager(groundWalls, null);
        groundWalls.clear();
        ModelObject.setEventManager(heroes, null);
        heroes.clear();
        ModelObject.setEventManager(bombs, null);
        bombs.clear();
        ModelObject.setEventManager(coins, null);
        coins.clear();
    }

    @Override
    public void setEventManager(EventManager eventManager) {
        super.setEventManager(eventManager);

        ModelObject.setEventManager(groundTiles, eventManager);
        ModelObject.setEventManager(groundWalls, eventManager);
        ModelObject.setEventManager(heroes, eventManager);
        ModelObject.setEventManager(bombs, eventManager);
        ModelObject.setEventManager(coins, eventManager);
    }

    public Array<GroundTileModel> getGroundTiles() {
        return groundTiles;
    }

    public Array<GroundWallModel> getGroundWalls() {
        return groundWalls;
    }

    public Array<HeroModel> getHeroes() {
        return heroes;
    }

    public Array<BombModel> getBombs() {
        return bombs;
    }

    public int getWorldWidth() {
        return worldWidth;
    }

    public int getWorldHeight() {
        return worldHeight;
    }

    public GroundTileModel getGroundTile(int x, int y) {
        if (x < 0 || x >= worldWidth || y < 0 || y >= worldHeight) {
            Gdx.app.error(TAG, "Illegal tile position.",
                    new IllegalArgumentException("Tile position is outside of world boundaries: " + x + ":" + y));
            return null;
        }

        int tileIndex = y + x * worldWidth; // Depends on the way tiles were created.
        return groundTiles.get(tileIndex);
    }

    public HeroModel getHero(int playerId) {
        for (int i = 0; i < heroes.size; i++) {
            HeroModel heroModel = heroes.get(i);
            if (heroModel.getPlayerId() == playerId) {
                return heroModel;
            }
        }
        Gdx.app.error(TAG, "Cannot find hero with player ID: " + playerId);
        return null;
    }

    public boolean checkPositionOccupied(int x, int y) {
        for (int i = 0; i < heroes.size; i++) {
            HeroModel heroModel = heroes.get(i);
            if (heroModel.getX() == x && heroModel.getY() == y) {
                return true;
            }
        }
        for (int i = 0; i < groundWalls.size; i++) {
            GroundWallModel groundWallModel = groundWalls.get(i);
            if (groundWallModel.getX() == x && groundWallModel.getY() == y) {
                return true;
            }
        }
        return false;
    }

    public GroundTileModel getRandomEmptyTile(Random random) {
        int x, y;
        while (true) {
            x = random.nextInt(worldWidth);
            y = random.nextInt(worldHeight);
            if (!checkPositionOccupied(x, y)) {
                break;
            }
        }
        return getGroundTile(x, y);
    }

    public static class GameData {
        public long randomSeed;
        public int worldWidth;
        public int worldHeight;
        public int heroAmount;
    }
}
