package com.metaphore.nakamatest.screens.proto.common;

import com.badlogic.gdx.math.MathUtils;

public class TurnManager {

    private int turnNumber = 0;
    private float currentTime;
    private float maxTime;
    private boolean turnStated = false;

    private Listener listener;

    public TurnManager(float turnDuration) {
        this.maxTime = turnDuration;
    }

    public void update(float delta) {
        if (!turnStated) return;

        currentTime += delta;
        if (currentTime > maxTime) {
            turnStated = false;

            if (listener != null) {
                listener.onTurnFinished(turnNumber);
            }
        }
    }

    public void startNextTurn() {
        if (turnStated) return;

        turnNumber++;
        currentTime = 0f;
        turnStated = true;

        if (listener != null) {
            listener.onTurnStarted(turnNumber);
        }
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public void setTurnDuration(float turnDuration) {
        this.maxTime = turnDuration;
    }

    public float getTurnProgress() {
        return MathUtils.clamp(currentTime / maxTime, 0f, 1f);
    }

    public boolean isTurnStarted() {
        return turnStated;
    }

    public interface Listener {
        void onTurnStarted(int turnNumber);
        void onTurnFinished(int turnNumber);
    }
}
