package com.metaphore.nakamatest.communication;

import com.badlogic.gdx.utils.Array;

import java.util.Map;

public interface CommData {
    int OPC_START_REQUEST   = 0;
    int OPC_GAME_STARTED    = 1;
    int OPC_PLAYER_ACTION   = 10;
    int OPC_TURN_ACTIONS    = 11;
    int OPC_SERVICE         = 2323;

    int getOpCode();
    
//    static Class<? extends CommData> resolveClass(int opCode) {
//        switch (opCode) {
//            case 0:
//                return StartGameRequest.class;
//            default:
//                Gdx.app.error(CommData.class.getSimpleName(), "There is no data class related to op code: " + opCode);
//                return null;
//        }
//    }

    class StartGameRequest implements CommData {
        @Override public int getOpCode() { return OPC_START_REQUEST; }

        public long random_seed;
    }

    class OnGameStarted implements CommData {
        @Override public int getOpCode() { return OPC_GAME_STARTED; }

        public long random_seed;
        public Array<PlayerHeroId> player_hero_ids;

        public static class PlayerHeroId {
            public int hero_id;
            public String user_id;
        }
    }
}
