package com.metaphore.nakamatest.communication.events;

import com.badlogic.gdx.utils.Array;
import com.crashinvaders.common.eventmanager.EventInfo;
import com.heroiclabs.nakama.UserPresence;

public class MatchPlayersChangedEvent implements EventInfo {

    private final Array<UserPresence> players;

    public MatchPlayersChangedEvent(Array<UserPresence> players) {
        this.players = players;
    }

    public Array<UserPresence> getPlayers() {
        return players;
    }
}
