package com.metaphore.nakamatest.communication.events;

import com.crashinvaders.common.eventmanager.EventInfo;
import com.heroiclabs.nakama.UserPresence;

public class MatchDataEvent implements EventInfo {

    private final String matchId;
    private final long opCode;
    private final UserPresence userPresence;
    private final byte[] data;

    public MatchDataEvent(String matchId, long opCode, UserPresence userPresence, byte[] data) {
        this.matchId = matchId;
        this.opCode = opCode;
        this.userPresence = userPresence;
        this.data = data;
    }

    public String getMatchId() {
        return matchId;
    }

    public long getOpCode() {
        return opCode;
    }

    public UserPresence getUserPresence() {
        return userPresence;
    }

    public byte[] getData() {
        return data;
    }
}
