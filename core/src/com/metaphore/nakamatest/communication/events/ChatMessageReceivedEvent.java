package com.metaphore.nakamatest.communication.events;

import com.crashinvaders.common.eventmanager.EventInfo;

public class ChatMessageReceivedEvent implements EventInfo {

    private String channelId;
    private String username;
    private String messageJson;

    public ChatMessageReceivedEvent(String channelId, String username, String messageJson) {
        this.channelId = channelId;
        this.username = username;
        this.messageJson = messageJson;
    }

    public String getChannelId() {
        return channelId;
    }

    public String getUsername() {
        return username;
    }

    public String getMessageJson() {
        return messageJson;
    }
}
