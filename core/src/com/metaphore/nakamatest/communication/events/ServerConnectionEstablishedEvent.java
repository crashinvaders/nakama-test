package com.metaphore.nakamatest.communication.events;

import com.crashinvaders.common.eventmanager.EventInfo;

public class ServerConnectionEstablishedEvent implements EventInfo {

    private String hostUrl;
    private String deviceUuid;

    public ServerConnectionEstablishedEvent(String hostUrl, String deviceUuid) {
        this.hostUrl = hostUrl;
        this.deviceUuid = deviceUuid;
    }

    public void setHostUrl(String hostUrl) {
        this.hostUrl = hostUrl;
    }

    public void setDeviceUuid(String deviceUuid) {
        this.deviceUuid = deviceUuid;
    }
}
