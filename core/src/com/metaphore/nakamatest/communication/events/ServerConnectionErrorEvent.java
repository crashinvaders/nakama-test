package com.metaphore.nakamatest.communication.events;

import com.crashinvaders.common.eventmanager.EventInfo;

public class ServerConnectionErrorEvent implements EventInfo {

    private final String hostUrl;
    private final String deviceUuid;
    private final Exception exception;

    public ServerConnectionErrorEvent(String hostUrl, String deviceUuid, Exception exception) {
        this.hostUrl = hostUrl;
        this.deviceUuid = deviceUuid;
        this.exception = exception;
    }

    public String getHostUrl() {
        return hostUrl;
    }

    public String getDeviceUuid() {
        return deviceUuid;
    }

    public Exception getException() {
        return exception;
    }
}
