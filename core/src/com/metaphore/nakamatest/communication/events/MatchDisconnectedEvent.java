package com.metaphore.nakamatest.communication.events;

import com.crashinvaders.common.eventmanager.EventInfo;

public class MatchDisconnectedEvent implements EventInfo {

    private final String matchId;

    public MatchDisconnectedEvent(String matchId) {
        this.matchId = matchId;
    }

    public String getMatchId() {
        return matchId;
    }
}
