package com.metaphore.nakamatest.communication.events;

import com.crashinvaders.common.eventmanager.EventInfo;

public class MatchConnectedEvent implements EventInfo {

    private final String matchId;

    public MatchConnectedEvent(String matchId) {
        this.matchId = matchId;
    }

    public String getMatchId() {
        return matchId;
    }
}
