package com.metaphore.nakamatest.communication;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.crashinvaders.common.CommonUtils;
import com.crashinvaders.common.eventmanager.EventInfo;
import com.crashinvaders.common.eventmanager.EventManager;
import com.heroiclabs.nakama.*;
import com.heroiclabs.nakama.Error;
import com.heroiclabs.nakama.api.ChannelMessage;
import com.heroiclabs.nakama.api.NotificationList;
import com.metaphore.nakamatest.communication.events.*;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ServerConnectionManager implements Disposable {
    private static final String TAG = ServerConnectionManager.class.getSimpleName();

    private final EventManager eventManager;

    private final Thread gdxThread;
    private final Executor gdxThreadExecutor;
    private final ExecutorService asyncThreadExecutor;

    private NakamaConnection connection = null;

    public ServerConnectionManager(EventManager eventManager) {
        this.eventManager = eventManager;

        gdxThread = Thread.currentThread();
        gdxThreadExecutor = new Executor() {
            @Override
            public void execute(Runnable command) {
                Gdx.app.postRunnable(command);
            }
        };
        asyncThreadExecutor = Executors.newSingleThreadExecutor();
    }

    @Override
    public void dispose() {
        disconnect();

        asyncThreadExecutor.shutdown();
    }

    public void connect(String hostUrl, String deviceUuid) {
        connect(hostUrl, deviceUuid, null);
    }

    public void connect(String hostUrl, String deviceUuid, ConnectionResultListener listener) {
        disconnect();

        connection = new NakamaConnection(hostUrl);
        connection.connect(deviceUuid, listener);
    }

    public boolean hasConnection() {
        return connection != null && connection.isConnected();
    }

    public void disconnect() {
        if (connection == null) return;

        connection.dispose();
        connection = null;
    }

    public boolean joinMatch(String matchId, ConnectionResultListener listener) {
        return connection.joinMatch(matchId, listener);
    }

    public boolean createMatch(ConnectionResultListener listener) {
        return connection.createMatch(listener);
    }

    public MatchSession getMatch() {
        return connection.matchSession;
    }

    public String getUsername() {
        return connection.nakamaSession.getUsername();
    }

    public void sendMatchData(long optCode, byte[] data) {
        connection.sendMatchData(optCode, data);
    }

    void dispatchEventOnMainThread(final EventInfo eventInfo) {
        if (gdxThread == Thread.currentThread()) {
            eventManager.dispatchEvent(eventInfo);
        } else {
            gdxThreadExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    eventManager.dispatchEvent(eventInfo);
                }
            });
        }
    }

    private class NakamaConnection implements Disposable {
        private static final int PORT_API = 7349;
        private static final int PORT_MATCH = 7350;

        private final String hostUrl;
//        private String username = null;
//        private String deviceUuid = null;

        private final Client nakamaClient;
        private final SocketClient nakamaSocket;
        private final SocketHandler socketListener;
        private Session nakamaSession;
//        private Channel chatChannel;
        private MatchSession matchSession;

        private volatile boolean connecting = false;

        public NakamaConnection(String hostUrl) {
            this.hostUrl = hostUrl;
            nakamaClient = new DefaultClient("defaultkey", hostUrl, PORT_API, false);
            nakamaSocket = nakamaClient.createSocket(PORT_MATCH, 5000);   // Params: port, timeout.
            socketListener = new SocketHandler();
        }

        @Override
        public synchronized void dispose() {
            asyncThreadExecutor.submit(new Runnable() {
                @Override
                public void run() {
                    nakamaSocket.disconnect();
                    nakamaClient.disconnect();
                    nakamaSession = null;
                    matchSession = null;
                }
            });
        }

        public synchronized boolean isConnected() {
            return nakamaSession != null;
        }

        public synchronized boolean isConnectedToMatch() {
            return matchSession != null;
        }

        public synchronized void connect(final String deviceUuid, final ConnectionResultListener listener) {
            if (connecting) {
                IllegalStateException exception = new IllegalStateException("Another connection procedure is in action.");
                Gdx.app.error(TAG, "Another connection procedure is in action.", exception);
                if (listener != null) {
                    listener.onError(exception);
                }
                return;
            }

            Gdx.app.debug(TAG, "Log In Initiated");

//            this.username = username;
//            this.deviceUuid = deviceUuid;

            if (nakamaSession != null) {
                IllegalStateException exception = new IllegalStateException("Another nakama session is already in use.");
                Gdx.app.error(TAG, "Another nakama session is already in use.", exception);
                if (listener != null) {
                    listener.onError(exception);
                }
                return;
            }

            asyncThreadExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    synchronized (NakamaConnection.this) {
                        connecting = true;
                        Session nakamaSession = null;

                        // Establish Nakama session.
                        try {
                            nakamaSession = nakamaClient.authenticateDevice(deviceUuid, true).get();
                            Gdx.app.log(TAG, "Authentication went successful as \"" + nakamaSession.getUsername() + "\"");
                            Gdx.app.debug(TAG, "Access token: " + nakamaSession.getAuthToken());
                        } catch (final InterruptedException | ExecutionException e) {
                            Gdx.app.error(TAG, "Error authenticating.", e);
                            connecting = false;

                            if (listener != null) {
                                Gdx.app.postRunnable(new Runnable() {
                                    @Override
                                    public void run() {
                                        listener.onError(e);
                                    }
                                });
                            }
                            dispatchEventOnMainThread(new ServerConnectionErrorEvent(hostUrl, deviceUuid, e));
                            return;
                        }

                        // Create socket connection.
                        try {
                            nakamaSocket.connect(nakamaSession, socketListener).get();
                            Gdx.app.log(TAG, "Socket connection established.");
                        } catch (InterruptedException | ExecutionException e) {
                            Gdx.app.error(TAG, "Error creating socket.", e);
                            connecting = false;

                            if (listener != null) {
                                Gdx.app.postRunnable(new Runnable() {
                                    @Override
                                    public void run() {
                                        listener.onError(e);
                                    }
                                });
                            }

                            dispatchEventOnMainThread(new ServerConnectionErrorEvent(hostUrl, deviceUuid, e));
                            return;
                        }

//                    // Join chat room.
//                    try {
//                        chatChannel = nakamaSocket.joinChat("TestChatRoom", ChannelType.ROOM, true, false).get();
//                        Gdx.app.log(TAG, "Now connected to channel id: " + chatChannel.getId());
//
//                        // Retrieve the last pack of messages.
//                        ChannelMessageList messages = nakamaClient.listChannelMessages(nakamaSession, chatChannel.getId(), 10, null, false).get();
//                        for (int i = messages.getMessagesCount()-1; i >= 0; i--) {
//                            socketListener.onChannelMessage(messages.getMessages(i));
//                        }
//
//                    } catch (InterruptedException | ExecutionException e) {
//                        Gdx.app.error(TAG, "Error joining chat.", e);
//                        connecting = false;
//
//                        if (listener != null) {
//                            Gdx.app.postRunnable(new Runnable() {
//                                @Override
//                                public void run() {
//                                    listener.onError(e);
//                                }
//                            });
//                        }
//
//                        dispatchEventOnMainThread(new ServerConnectionErrorEvent(hostUrl, username, e));
//                        return;
//                    }

                        nakamaSocket.updateStatus("Online");
                        NakamaConnection.this.nakamaSession = nakamaSession;

                        connecting = false;

                        if (listener != null) {
                            Gdx.app.postRunnable(new Runnable() {
                                @Override
                                public void run() {
                                    listener.onSuccess();
                                }
                            });
                        }

                        dispatchEventOnMainThread(new ServerConnectionEstablishedEvent(hostUrl, deviceUuid));
                    }
                }
            });
        }

        public synchronized boolean joinMatch(final String matchId, final ConnectionResultListener listener) {
            if (nakamaSession == null) {
                Exception exception = new IllegalStateException("Nakama session is not initialized.");
                Gdx.app.error(TAG, "Nakama session is not initialized.", exception);
                if (listener != null) { listener.onError(exception); }
                return false;
            }
            if (matchSession != null) {
                Exception exception = new IllegalStateException("A match session already established.");
                Gdx.app.error(TAG, "A match session already established.", exception);
                if (listener != null) { listener.onError(exception); }
                return false;
            }
            if (matchId.length() == 0) {
                Exception exception = new IllegalStateException("Match ID is empty!");
                Gdx.app.error(TAG, "Match ID is empty!", exception);
                if (listener != null) { listener.onError(exception); }
                return false;
            }

            asyncThreadExecutor.submit(new Runnable() {
                @Override
                public void run() {
                    synchronized (NakamaConnection.this) {
                        try {
                            // Start matchmaking.
                            Match match = nakamaSocket.joinMatch(matchId).get();
                            matchSession = new MatchSession(eventManager, match);
                            Gdx.app.log(TAG, "Joined to the match with ID:  " + match.getMatchId());

                            if (listener != null) {
                                Gdx.app.postRunnable(new Runnable() {
                                    @Override
                                    public void run() {
                                        listener.onSuccess();
                                    }
                                });
                            }

                            dispatchEventOnMainThread(new MatchConnectedEvent(match.getMatchId()));

                        } catch (InterruptedException | ExecutionException e) {
                            Gdx.app.error(TAG, "Error creating a match.", e);
                            matchSession = null;

                            if (listener != null) {
                                Gdx.app.postRunnable(new Runnable() {
                                    @Override
                                    public void run() {
                                        listener.onError(e);
                                    }
                                });
                            }
                        }
                    }
                }
            });

            return true;
        }

        public synchronized boolean createMatch(final ConnectionResultListener listener) {
            if (nakamaSession == null) {
                Exception exception = new IllegalStateException("Nakama session is not initialized.");
                Gdx.app.error(TAG, "Nakama session is not initialized.", exception);
                if (listener != null) { listener.onError(exception); }
                return false;
            }
            if (matchSession != null) {
                Exception exception = new IllegalStateException("A match session already established.");
                Gdx.app.error(TAG, "A match session already established.", exception);
                if (listener != null) { listener.onError(exception); }
                return false;
            }

            asyncThreadExecutor.submit(new Runnable() {
                @Override
                public void run() {
                    synchronized (NakamaConnection.this) {
                        try {
                            // Start matchmaking.
                            Match match = nakamaSocket.createMatch().get();
                            matchSession = new MatchSession(eventManager, match);
                            Gdx.app.log(TAG, "Match created with ID:  " + match.getMatchId());

                            if (listener != null) {
                                Gdx.app.postRunnable(new Runnable() {
                                    @Override
                                    public void run() {
                                        listener.onSuccess();
                                    }
                                });
                            }

                            dispatchEventOnMainThread(new MatchConnectedEvent(match.getMatchId()));

                        } catch (InterruptedException | ExecutionException e) {
                            Gdx.app.error(TAG, "Error creating a match.", e);
                            matchSession = null;

                            if (listener != null) {
                                Gdx.app.postRunnable(new Runnable() {
                                    @Override
                                    public void run() {
                                        listener.onError(e);
                                    }
                                });
                            }
                        }
                    }
                }
            });

            return true;
        }

        public synchronized void sendMatchData(final long optCode, final byte[] data) {
            if (nakamaSession == null) {
                Gdx.app.error(TAG, "Nakama session is not initialized.", new IllegalStateException());
                return;
            }
            if (matchSession == null) {
                Gdx.app.error(TAG, "Match session in not established.", new IllegalStateException());
                return;
            }

            nakamaSocket.sendMatchData(matchSession.getMatchId(), optCode, data, null);

//            asyncThreadExecutor.submit(new Runnable() {
//                @Override
//                public void run() {
//                    synchronized (NakamaConnection.this) {
//                        nakamaSocket.sendMatchData(matchSession.getMatchId(), optCode, data, null);
//                    }
//                }
//            });
        }

        private class SocketHandler implements SocketListener {
            private final String TAG = SocketHandler.class.getSimpleName();

            @Override
            public void onDisconnect(Throwable throwable) {
                synchronized (NakamaConnection.this) {
                    if (throwable != null) {
                        Gdx.app.error(TAG, "Disconnected with error.", throwable);
                    } else {
                        Gdx.app.log(TAG, "Disconnected.");
                    }

                    if (matchSession != null) {
                        String matchId = matchSession.getMatchId();
                        matchSession = null;
                        dispatchEventOnMainThread(new MatchDisconnectedEvent(matchId));
                    }
                }
            }

            @Override
            public void onError(Error error) {
                Gdx.app.error(TAG, "An error occurred.", error);
            }

            @Override
            public void onChannelMessage(ChannelMessage message) {
                Gdx.app.debug(TAG, "Received a message on channel " + message.getChannelId());

                final String channelId = message.getChannelId();
                final String username = message.getUsername();
                final String messageJson = message.getContent();

                dispatchEventOnMainThread(new ChatMessageReceivedEvent(channelId, username, messageJson));
            }

            @Override
            public void onChannelPresence(ChannelPresenceEvent channelPresenceEvent) {
                Gdx.app.debug(TAG, "On channel presence.");
            }

            @Override
            public void onMatchmakerMatched(MatchmakerMatched matchmakerMatched) {
                Gdx.app.debug(TAG, "On matchmaker matched.");
            }

            @Override
            public void onMatchData(MatchData matchData) {
//                Gdx.app.log(TAG, "On match data.");

                final String matchId = matchData.getMatchId();
                final long opCode = matchData.getOpCode();
                final UserPresence userPresence = matchData.getUserPresence();
                //FIXME For some reason matchData.getUserPresence() is always null.
                // Check https://github.com/heroiclabs/nakama-java/issues/25 for solution.
                final byte[] data = matchData.getData();

                dispatchEventOnMainThread(new MatchDataEvent(matchId, opCode, userPresence, data));
            }

            @Override
            public void onMatchPresence(MatchPresenceEvent matchPresenceEvent) {
                Gdx.app.debug(TAG, "On match players changed.");

                synchronized (NakamaConnection.this) {
                    matchSession.updatePlayers(matchPresenceEvent.getJoins(), matchPresenceEvent.getLeaves());
                }
            }

            @Override
            public void onNotifications(NotificationList notificationList) {
                Gdx.app.debug(TAG, "Received notifications. Count: " + notificationList.getNotificationsCount());
            }

            @Override
            public void onStatusPresence(StatusPresenceEvent statusPresenceEvent) {
                Gdx.app.debug(TAG, "On status presence.");
            }

            @Override
            public void onStreamPresence(StreamPresenceEvent streamPresenceEvent) {
                Gdx.app.debug(TAG, "On stream presence.");
            }

            @Override
            public void onStreamData(StreamData streamData) {
                Gdx.app.debug(TAG, "On stream data.");
            }
        }
    }

    public static class MatchSession {
        private static final String TAG = MatchSession.class.getSimpleName();

        private final Array<UserPresence> players = new Array<>(UserPresence.class);
        private final EventManager eventManager;
        private final Match match;

        private boolean programmaticEvents = true;

        MatchSession(EventManager eventManager, Match match) {
            this.eventManager = eventManager;
            this.match = match;

            programmaticEvents = false;
            updatePlayers(match.getPresences(), null);
            programmaticEvents = true;
        }

        public String getMatchId() {
            return match.getMatchId();
        }

        public synchronized Array<UserPresence> getPlayers() {
            return players;
        }

        synchronized void updatePlayers(List<UserPresence> joins, List<UserPresence> leaves) {
            boolean playersModified = false;

            if (leaves != null) for (UserPresence userPresence : leaves) {
                if (players.contains(userPresence, false)) {
                    boolean removed = players.removeValue(userPresence, false);
                    playersModified |= removed;
                }
            }

            if (joins != null) for (UserPresence userPresence : joins) {
                if (!players.contains(userPresence, false)) {
                    players.add(userPresence);
                    playersModified = true;
                }
            }

            if (playersModified) {
                Gdx.app.debug(TAG, "Match players changed. Count: " + players.size);
                if (programmaticEvents) {
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            synchronized (MatchSession.this) {
                                eventManager.dispatchEvent(new MatchPlayersChangedEvent(players));
                            }
                        }
                    });
                }
            }
        }
    }

    public interface ConnectionResultListener {
        void onSuccess();
        void onError(Exception exception);
    }
}
