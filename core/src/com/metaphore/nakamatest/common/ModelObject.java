package com.metaphore.nakamatest.common;

import com.badlogic.gdx.utils.Array;
import com.crashinvaders.common.eventmanager.EventManager;

public abstract class ModelObject {
    protected EventManager eventManager;

    public EventManager getEventManager() {
        return eventManager;
    }

    public void setEventManager(EventManager eventManager) {
        this.eventManager = eventManager;
    }

    public static void setEventManager(Array<? extends ModelObject> array, EventManager eventManager) {
        for (int i = 0; i < array.size; i++) {
            array.get(i).setEventManager(eventManager);
        }
    }
}