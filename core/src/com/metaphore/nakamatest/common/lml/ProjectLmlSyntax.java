package com.metaphore.nakamatest.common.lml;

import com.crashinvaders.common.lml.CommonLmlSyntax;
import com.metaphore.nakamatest.common.lml.attributes.OnDoubleClickLmlAttribute;
import com.metaphore.nakamatest.common.lml.attributes.ScalePressLmlAttribute;
import com.metaphore.nakamatest.common.lml.tags.ButtonLmlTag;
import com.metaphore.nakamatest.common.lml.tags.SizePersistentContainerLmlTag;

public class ProjectLmlSyntax extends CommonLmlSyntax {

    @Override
    protected void registerActorTags() {
        super.registerActorTags();

        addTagProvider(new ButtonLmlTag.Provider(), "button");
        addTagProvider(new SizePersistentContainerLmlTag.TagProvider(), "sizePersistentContainer");
    }

    @Override
    protected void registerCommonAttributes() {
        super.registerCommonAttributes();

        addAttributeProcessor(new ScalePressLmlAttribute(false), "scalePress");
        addAttributeProcessor(new ScalePressLmlAttribute(true), "scalePressTransform");
        addAttributeProcessor(new OnDoubleClickLmlAttribute(), "onDoubleClick", "doubleClick");
    }
}