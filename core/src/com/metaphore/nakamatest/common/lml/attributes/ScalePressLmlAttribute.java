package com.metaphore.nakamatest.common.lml.attributes;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.github.czyzby.lml.parser.LmlParser;
import com.github.czyzby.lml.parser.tag.LmlAttribute;
import com.github.czyzby.lml.parser.tag.LmlTag;
import com.metaphore.nakamatest.common.scene2d.ScalePressInputListener;
import com.metaphore.nakamatest.common.scene2d.TransformScalePressInputListener;

public class ScalePressLmlAttribute implements LmlAttribute<Actor> {

    private final boolean useTransform;

    public ScalePressLmlAttribute() {
        useTransform = false;
    }

    public ScalePressLmlAttribute(boolean useTransform) {
        this.useTransform = useTransform;
    }

    @Override
    public Class<Actor> getHandledType() {
        return Actor.class;
    }

    @Override
    public void process(LmlParser parser, LmlTag tag, Actor actor, String rawAttributeData) {
        float scaleFactor = parser.parseFloat(rawAttributeData, actor);
        ScalePressInputListener listener;
        if (useTransform) {
            if (!(actor instanceof Group)) {
                parser.throwError("Actor should be a derivative of Group class to use transform scaling.");
            }
            listener = new TransformScalePressInputListener();
        } else {
            listener = new ScalePressInputListener();
        }
        listener.setScaleFactor(scaleFactor);
//        listener.setPressDuration(0.4f);
//        listener.setReleaseDuration(0.2f);
//        listener.setPressInterpolation(Interpolation.elasticOut);
//        listener.setReleaseInterpolation(Interpolation.exp10Out);
        actor.addListener(listener);
    }
}
