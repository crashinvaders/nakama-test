package com.metaphore.nakamatest.common.lml.tags;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.github.czyzby.lml.parser.LmlParser;
import com.github.czyzby.lml.parser.impl.tag.actor.TableLmlTag;
import com.github.czyzby.lml.parser.tag.LmlActorBuilder;
import com.github.czyzby.lml.parser.tag.LmlTag;
import com.github.czyzby.lml.parser.tag.LmlTagProvider;

public class ButtonLmlTag extends TableLmlTag {
    private static final String TAG = ButtonLmlTag.class.getSimpleName();

    public ButtonLmlTag(final LmlParser parser, final LmlTag parentTag, final StringBuilder rawTagData) {
        super(parser, parentTag, rawTagData);
    }

    @Override
    protected Actor getNewInstanceOfActor(final LmlActorBuilder builder) {
        Skin skin = getSkin(builder);
        String styleName = builder.getStyleName();

        if (skin.has(styleName, Button.ButtonStyle.class)) {
            Button.ButtonStyle buttonStyle = skin.get(styleName, Button.ButtonStyle.class);
            return new Button(buttonStyle);
        }

        Drawable drawable = skin.getDrawable(styleName);
        if (drawable != null) {
            Gdx.app.debug(TAG, "Creating button style using \""+styleName+"\" drawable on the fly.");
            Button.ButtonStyle buttonStyle = new Button.ButtonStyle();
            buttonStyle.up = drawable;
            // Add new style to the skin.
            skin.add(styleName, buttonStyle);
            return new Button(buttonStyle);
        }

        getParser().throwError("Cannot create button with style: " + styleName);
        return null;
    }

    public static class Provider implements LmlTagProvider {
        @Override
        public LmlTag create(LmlParser parser, LmlTag parentTag, StringBuilder rawTagData) {
            return new ButtonLmlTag(parser, parentTag, rawTagData);
        }
    }
}
