package com.metaphore.nakamatest.common.scene2d;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Align;

public class ScalePressInputListener extends InputListener {

    protected Interpolation pressInterpolation = Interpolation.exp10Out;
    protected Interpolation releaseInterpolation = Interpolation.exp10Out;
    protected float pressDuration = 0.2f;
    protected float releaseDuration = 0.2f;
    protected float scaleFactor = 1.2f;
    protected Action scaleAction = null;

    public ScalePressInputListener() {
    }

    @Override
    public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
        Actor actor = event.getListenerActor();
        if (scaleAction != null) {
            actor.removeAction(scaleAction);
            scaleAction = null;
        }
        actor.setOrigin(Align.center);
        actor.addAction(scaleAction = Actions.scaleTo(scaleFactor, scaleFactor, pressDuration, pressInterpolation));
    }

    @Override
    public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
        Actor actor = event.getListenerActor();
        if (scaleAction != null) {
            actor.removeAction(scaleAction);
            scaleAction = null;
        }
        actor.setOrigin(Align.center);
        actor.addAction(scaleAction = Actions.scaleTo(1.0f, 1.0f, releaseDuration, releaseInterpolation));
    }

    public void setPressInterpolation(Interpolation pressInterpolation) {
        this.pressInterpolation = pressInterpolation;
    }

    public void setReleaseInterpolation(Interpolation releaseInterpolation) {
        this.releaseInterpolation = releaseInterpolation;
    }

    public void setPressDuration(float pressDuration) {
        this.pressDuration = pressDuration;
    }

    public void setReleaseDuration(float releaseDuration) {
        this.releaseDuration = releaseDuration;
    }

    public void setScaleFactor(float scaleFactor) {
        this.scaleFactor = scaleFactor;
    }
}
