package com.metaphore.nakamatest.common.scene2d;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Align;
import com.crashinvaders.common.scene2d.actions.ActionsExt;

/** Same as {@link ScalePressInputListener} but turns on transform for the target actor during press scaling. */
public class TransformScalePressInputListener extends ScalePressInputListener {

    @Override
    public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
        Actor actor = event.getListenerActor();
        if (scaleAction != null) {
            actor.removeAction(scaleAction);
            scaleAction = null;
        }
        actor.setOrigin(Align.center);
        actor.addAction(scaleAction = Actions.sequence(
                ActionsExt.transform(true),
                Actions.scaleTo(scaleFactor, scaleFactor, pressDuration, pressInterpolation)
        ));
    }

    @Override
    public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
        Actor actor = event.getListenerActor();
        if (scaleAction != null) {
            actor.removeAction(scaleAction);
            scaleAction = null;
        }
        actor.setOrigin(Align.center);
        actor.addAction(scaleAction = Actions.sequence(
                Actions.scaleTo(1.0f, 1.0f, releaseDuration, releaseInterpolation),
                ActionsExt.transform(false)
        ));
    }
}
