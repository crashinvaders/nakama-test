package com.metaphore.nakamatest.common.scene2d;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public abstract class DoubleClickListener extends ClickListener {
    private static final long SECOND_CLICK_TIME = 250000000L; // 250ms

    private boolean firstClickCaught = false;
    private long lastClickTime = 0;

    public DoubleClickListener() {
    }

    public DoubleClickListener(int button) {
        super(button);
    }

    @Override
    public void clicked(final InputEvent event, final float x, final float y) {
        long currentEventTime = Gdx.input.getCurrentEventTime();
        long deltaTime = currentEventTime - lastClickTime;
        lastClickTime = currentEventTime;

        if (!firstClickCaught) {
            firstClickCaught = true;
        } else {
            if (deltaTime < SECOND_CLICK_TIME) {
                firstClickCaught = false;
                onDoubleClick(x, y);
            }
        }
    }

    protected abstract void onDoubleClick(float x, float y);
}
