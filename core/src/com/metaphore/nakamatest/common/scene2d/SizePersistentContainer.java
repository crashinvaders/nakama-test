package com.metaphore.nakamatest.common.scene2d;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Container;

/** The container that never shrinks its size and always stays the same or bigger. */
public class SizePersistentContainer<T extends Actor> extends Container<T> {

    private float minWidth = 0f;
    private float minHeight = 0f;

    public SizePersistentContainer() {
    }

    public SizePersistentContainer(T actor) {
        super(actor);
    }

    @Override
    protected void sizeChanged() {
        super.sizeChanged();
        minWidth = getWidth();
        minHeight = getHeight();
    }

    @Override
    public float getPrefWidth() {
        return Math.max(minWidth, super.getPrefWidth());
    }

    @Override
    public float getPrefHeight() {
        return Math.max(minHeight, super.getPrefHeight());
    }
}
